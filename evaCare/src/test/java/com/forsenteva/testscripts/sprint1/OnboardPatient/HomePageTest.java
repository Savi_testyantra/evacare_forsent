package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.Login_Page;

public class HomePageTest extends BaseTest {
	Login_Page page;
	@Test
	public void homePage() throws Throwable {
		
		
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");
		
		 for(int i=4; i<sdata1.length; i++)
		 {
			 String sdata2 = ExcelDataProvider.readcolData(sdata1[i], "FName", GenericLib.testDataPath1, "Sheet1");	 
				
							
			String sdata3 = ExcelDataProvider.readcolData(sdata1[i], "Lname", GenericLib.testDataPath1, "Sheet1");	 
					
			String sdata4 = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.testDataPath1, "Sheet1");	 
						
			 String sdata5 = ExcelDataProvider.readcolData(sdata1[i], "phone", GenericLib.testDataPath1, "Sheet1");	 
							
			String sdata6 = ExcelDataProvider.readcolData(sdata1[i], "Ephone", GenericLib.testDataPath1, "Sheet1");	 
								
			String sdata7 = ExcelDataProvider.readcolData(sdata1[i], "Econtact", GenericLib.testDataPath1, "Sheet1");	 
									
			 String sdata8 = ExcelDataProvider.readcolData(sdata1[i], "DOB", GenericLib.testDataPath1, "Sheet1");	 
										
			HomePage homepage=new HomePage(driver);	
			homepage.addNewPatient(sdata2, sdata3, sdata4, sdata5, sdata6, sdata7);
		 
		 }
	}
}
