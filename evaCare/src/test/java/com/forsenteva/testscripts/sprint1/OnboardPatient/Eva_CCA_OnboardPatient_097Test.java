package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_097Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	private final String PHRASE = "@#$%^";
	private final String PATIENT = "vvv";
	private final String MOOD = "Agitated";
	private final String CONVERSTAION = "Conversation";
	private final String SUBJECT = "Baseball Sport";

	@Test
	public void verifyPhraseIsMandatory() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_097();
	}
}