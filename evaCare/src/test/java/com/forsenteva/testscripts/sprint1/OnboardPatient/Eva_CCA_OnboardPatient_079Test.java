package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;

public class Eva_CCA_OnboardPatient_079Test extends BaseTest{
	//Test data
	private InitializePages initializePages;
	private final String TAG = "#@$%1236";

	/*
	 * Test to validate text fields in personalized media page 
	 * 
	 */
	@Test
	public void Eva_CCA_Onboard_Patient_079() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_079(TAG);
}
}