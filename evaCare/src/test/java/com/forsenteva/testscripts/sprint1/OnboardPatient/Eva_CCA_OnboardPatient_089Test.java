package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_089Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	public final String PATIENT = "vvv";
	public final String PERSONALIZED_MEDIA = "Personalized Media Library";
	
	@Test
  public void verifyMediaRecordSearch() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_089();
		
}
}
