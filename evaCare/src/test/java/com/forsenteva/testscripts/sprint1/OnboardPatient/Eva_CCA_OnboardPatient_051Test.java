package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;

public class Eva_CCA_OnboardPatient_051Test extends BaseTest {
	@Test
	public void submitButton() throws Throwable {
		InitializePages page = new InitializePages(driver);
		
		
		
//		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");

		
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");
		int i=1;
		while(i<sdata1.length)
		{

		String sdata2 = ExcelDataProvider.readcolData(sdata1[i], "FName", GenericLib.testDataPath1, "Sheet1");
		System.out.println(sdata2);

		String sdata3 = ExcelDataProvider.readcolData(sdata1[i], "Lname", GenericLib.testDataPath1,
				"Sheet1");
		System.out.println(sdata3);
		String sdata4 = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.testDataPath1,
				"Sheet1");
		System.out.println(sdata4);
		String sdata5 = ExcelDataProvider.readcolData(sdata1[i], "phone", GenericLib.testDataPath1,
				"Sheet1");
		System.out.println(sdata5);
		String sdata6 = ExcelDataProvider.readcolData(sdata1[i], "Ephone", GenericLib.testDataPath1,
				"Sheet1");
		System.out.println(sdata6);
		String sdata7 = ExcelDataProvider.readcolData(sdata1[i], "Econtact", GenericLib.testDataPath1,
				"Sheet1");
		System.out.println(sdata7);
		String sdata8 = ExcelDataProvider.readcolData(sdata1[i], "DOB", GenericLib.testDataPath1, "Sheet1");
		System.out.println(sdata8);

					
		page.OnboardPatient.onboard_Patient_051(sdata2, sdata3, RandomStringUtils.randomAlphabetic(3)+sdata4, sdata5, sdata6, sdata7);

	}
		
	
    }
	

}


