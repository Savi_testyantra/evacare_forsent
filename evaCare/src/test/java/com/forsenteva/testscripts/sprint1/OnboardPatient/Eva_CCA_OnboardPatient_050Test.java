package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_050Test extends BaseTest {
	
	@Test
	public void cancelButton() throws Throwable {
		InitializePages page = new InitializePages(driver);
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");


			String sdata2 = ExcelDataProvider.readcolData(sdata1[1], "FName", GenericLib.testDataPath1, "Sheet1");
			System.out.println(sdata2);

			String sdata3 = ExcelDataProvider.readcolData(sdata1[1], "Lname", GenericLib.testDataPath1,
					"Sheet1");
			System.out.println(sdata3);
			String sdata4 = ExcelDataProvider.readcolData(sdata1[1], "Email", GenericLib.testDataPath1,
					"Sheet1");
			System.out.println(sdata4);
			String sdata5 = ExcelDataProvider.readcolData(sdata1[1], "phone", GenericLib.testDataPath1,
					"Sheet1");
			System.out.println(sdata5);
			String sdata6 = ExcelDataProvider.readcolData(sdata1[1], "Ephone", GenericLib.testDataPath1,
					"Sheet1");
			System.out.println(sdata6);
			String sdata7 = ExcelDataProvider.readcolData(sdata1[1], "Econtact", GenericLib.testDataPath1,
					"Sheet1");
			System.out.println(sdata7);
			String sdata8 = ExcelDataProvider.readcolData(sdata1[1], "DOB", GenericLib.testDataPath1, "Sheet1");
			System.out.println(sdata8);
			
			page.OnboardPatient.onboard_Patient50(sdata2, sdata3, RandomStringUtils.randomAlphabetic(3)+sdata4, sdata5, sdata6, sdata7);
			

			
		}
	}
