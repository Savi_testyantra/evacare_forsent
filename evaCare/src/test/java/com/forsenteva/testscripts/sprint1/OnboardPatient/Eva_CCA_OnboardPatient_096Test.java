package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_096Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	private final String PHRASE = "@#$%^";
	private final String MOOD = "Agitated";
	private final String SUBJECT = "Baseball Sport";

	@Test
	public void validatePhraseField() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_096( PHRASE,  SUBJECT, MOOD );
		
	}
}