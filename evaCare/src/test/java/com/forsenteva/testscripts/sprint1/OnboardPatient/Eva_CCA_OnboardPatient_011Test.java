package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;


public class Eva_CCA_OnboardPatient_011Test extends BaseTest {
@Test
public void checkingEmailFieldTest() throws Throwable {
	
	
	String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
	
		 String sdata2 = ExcelDataProvider.readcolData(sdata1[28], "FName", GenericLib.testDataPath, "Sheet1");	 
			
						
		String sdata3 = ExcelDataProvider.readcolData(sdata1[28], "Lname", GenericLib.testDataPath, "Sheet1");	 
				
		String sdata4 = ExcelDataProvider.readcolData(sdata1[34], "Email", GenericLib.testDataPath, "Sheet1");	 
					
		 String sdata5 = ExcelDataProvider.readcolData(sdata1[28], "phone", GenericLib.testDataPath, "Sheet1");	 
						
		String sdata6 = ExcelDataProvider.readcolData(sdata1[28], "Ephone", GenericLib.testDataPath, "Sheet1");	 
							
		String sdata7 = ExcelDataProvider.readcolData(sdata1[28], "Econtact", GenericLib.testDataPath, "Sheet1");	 
						
		//InitializePages.OnboardPatientPage.OnboardPatient_011(sdata2, sdata3, sdata4, sdata5, sdata6, sdata7);
		InitializePages.OnboardPatient.OnboardPatient_011(RandomStringUtils.randomAlphabetic(5),
				RandomStringUtils.randomAlphabetic(5), 
				RandomStringUtils.randomAlphabetic(5)+sdata4,
				RandomStringUtils.randomNumeric(10), RandomStringUtils.randomNumeric(10), RandomStringUtils.randomAlphabetic(5));
	 
}
}
