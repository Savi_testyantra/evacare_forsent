package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;

public class Eva_CCA_OnboardPatient_083Test extends BaseTest{
	// Test data 
		private final String PAGE_TITLE = "Onboard Patient - Eva";
		private final String PATIENT_NAME = "P4_aaa";
		private final String COLUMN_NAME = "Music and Movies";
		
   /*
	* Test to verify personalized media library page contains all relevant fields 
    * 
	*/	
	@Test
	public void verifyFieldsDisplay() throws Throwable {
		// Initialization of all page objects 
		InitializePages initializePages = new InitializePages(driver);
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_083();
	}
}