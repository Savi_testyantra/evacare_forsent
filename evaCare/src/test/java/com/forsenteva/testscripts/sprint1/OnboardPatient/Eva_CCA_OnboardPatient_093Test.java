package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_093Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	public final String PATIENT = "vvv";
	public final String CONVERSTAION = "Conversation";

	
	@Test
  public void verifyConversationGrid() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);

		/*
		 * click on board patient in On Board page 
		 */
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_093();

		/*
		 * click add button under conversation column for patient 
		 */
		//initializePages.onboardPatientpage.clickCellDataInOnBoardPage();

		/*
		 * verify Navigation To Tab 
		 */
		//initializePages.onboardPatientpage.verifyNavigationToTab(CONVERSTAION);
		
		
  }
}
