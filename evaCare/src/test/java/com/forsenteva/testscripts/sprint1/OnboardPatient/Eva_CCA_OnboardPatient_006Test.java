package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ConfigDataProvider;
import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;



public class Eva_CCA_OnboardPatient_006Test extends BaseTest {
	@Test
	public void mandatoryFirstNameTest() throws Throwable
	{
		
		
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
		
			 String sdata2 = ExcelDataProvider.readcolData(sdata1[11], "FName", GenericLib.testDataPath, "Sheet1");	 
				
							
			String sdata3 = ExcelDataProvider.readcolData(sdata1[11], "Lname", GenericLib.testDataPath, "Sheet1");	 
					
			String sdata4 = ExcelDataProvider.readcolData(sdata1[34], "Email", GenericLib.testDataPath, "Sheet1");	 
						
			 String sdata5 = ExcelDataProvider.readcolData(sdata1[11], "phone", GenericLib.testDataPath, "Sheet1");	 
							
			String sdata6 = ExcelDataProvider.readcolData(sdata1[11], "Ephone", GenericLib.testDataPath, "Sheet1");	 
								
			String sdata7 = ExcelDataProvider.readcolData(sdata1[11], "Econtact", GenericLib.testDataPath, "Sheet1");	 
							
		
		
			InitializePages.OnboardPatient.OnboardPatient_006(
					RandomStringUtils.randomAlphabetic(5),
					RandomStringUtils.randomAlphabetic(5), 
					RandomStringUtils.randomAlphabetic(5)+sdata4,
					RandomStringUtils.randomNumeric(10), RandomStringUtils.randomNumeric(10), RandomStringUtils.randomAlphabetic(5));
	}

}
