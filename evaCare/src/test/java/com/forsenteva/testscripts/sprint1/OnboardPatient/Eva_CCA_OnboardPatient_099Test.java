package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_099Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	private final String PATIENT = "vvv";
	private final String MOOD1 = "Agitated";
	private final String MOOD2 = "Happy";
	private final String MOOD3 = "Sad";

	private final String CONVERSTAION = "Conversation";

	@Test
  public void verifyMoodField() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.OnboardPatient.Eva_CCA_Onboard_Patient_099(MOOD1, MOOD2, MOOD3);
  }
}
