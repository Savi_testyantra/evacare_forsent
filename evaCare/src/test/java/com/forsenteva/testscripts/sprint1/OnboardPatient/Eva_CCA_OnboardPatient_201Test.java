package com.forsenteva.testscripts.sprint1.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_201Test extends BaseTest{
	@Test
	public void verify_Month() throws Throwable {
		InitializePages page = new InitializePages(driver);
		page.OnboardPatient.onboard_Patient_201();
	}
}
