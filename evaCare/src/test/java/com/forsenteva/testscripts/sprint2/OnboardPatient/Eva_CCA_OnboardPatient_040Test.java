package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;

/*
 * 40 testcase
 * verifying  emergency phone number will accept character and Special characters
 */
public class Eva_CCA_OnboardPatient_040Test extends BaseTest{
@Test
public void OnboardPatient_040Test() throws Throwable
{
	InitializePages.OnboardPatient.onboardPatient40(); 
}
}
