package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

/*
 * verifying user can able to click on pevious and next button or not
 */
public class Eva_CCA_OnboardPatient_022Test extends BaseTest {
	@Test
	public void OnboardPatient_022Test() throws Throwable
	{
		InitializePages.OnboardPatient.OnboardPatient_022();
	}

}
