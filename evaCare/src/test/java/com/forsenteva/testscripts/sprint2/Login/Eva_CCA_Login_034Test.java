package com.forsenteva.testscripts.sprint2.Login;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest_Login;
/*
 *Login_034 test case
 * verify the new password pagehave validation message is there or not.
 */
public class Eva_CCA_Login_034Test extends BaseTest_Login {
	@Test
	public void Login_034Test() throws Throwable
	{
		InitializePages.Login_New_Password.Login_034();
	}

}
