package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
/*
 * 20 testcase method
 * verifying current month calender is displaying or not
 */
public class Eva_CCA_OnboardPatient_020Test extends BaseTest {
	@Test
	public void OnboardPatient_020Test() throws Throwable
	{
		InitializePages.OnboardPatient.OnboardPatient_020();
	}

}
