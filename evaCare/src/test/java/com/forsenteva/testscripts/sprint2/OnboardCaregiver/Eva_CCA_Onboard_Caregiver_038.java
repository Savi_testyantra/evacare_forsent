package com.forsenteva.testscripts.sprint2.OnboardCaregiver;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Caregiver_038 extends BaseTest {
	@Test
	public void addEvent() throws Throwable {
		InitializePages page = new InitializePages(driver);
		page.OnboardCaregiver.eva_Onboard_Caregiver38();
	}
}
