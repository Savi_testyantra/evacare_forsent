package com.forsenteva.testscripts.sprint2.Events;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Events_004Test extends BaseTest{
  private final String MONTH = "September";
	@Test
  public void f() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.OnboardPatient.navigationToEvents();
		initializePages.EventsPage.Eva_CCA_Events_004(MONTH);
  }
}
