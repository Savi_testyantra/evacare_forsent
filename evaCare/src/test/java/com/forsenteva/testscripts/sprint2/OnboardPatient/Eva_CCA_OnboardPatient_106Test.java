package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_106Test extends BaseTest{
	private final String SUBJECT = "Pets";
	
  @Test
  public void validateSubjectField() throws Throwable{
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.OnboardPatientconversation.Eva_CCA_Onboard_Patient_106(SUBJECT);
  }
}