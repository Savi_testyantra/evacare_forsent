package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_036Test extends BaseTest{
	private final String EMERGENCY_CONTACT_DATA = "12345";
	private final String EMERGENCY_CONTACT_ERR_MSG = "Please Enter Valid Contact Name.";
	
  @Test
  public void f() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.OnboardPatient_AddNew.Eva_CCA_Onboard_Patient_036(EMERGENCY_CONTACT_DATA, EMERGENCY_CONTACT_ERR_MSG);
	  
  }
}
