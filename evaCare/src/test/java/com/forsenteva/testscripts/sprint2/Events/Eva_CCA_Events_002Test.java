package com.forsenteva.testscripts.sprint2.Events;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Events_002Test extends BaseTest{
  @Test
  public void verifyEventsPageFields() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.OnboardPatient.navigationToEvents();
	  initializePages.EventsPage.Eva_CCA_Events_002();
  }
}
