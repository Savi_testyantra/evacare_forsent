package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_105Test extends BaseTest{
	private final String SUBJECT1 = "Travel";
	private final String SUBJECT2 = "Food";
  @Test
  public void verifySubjectData() throws Throwable {
		  InitializePages initializePages = new InitializePages(driver);
		  initializePages.OnboardPatientconversation.Eva_CCA_Onboard_Patient_105(SUBJECT1, SUBJECT2);
  }
}
