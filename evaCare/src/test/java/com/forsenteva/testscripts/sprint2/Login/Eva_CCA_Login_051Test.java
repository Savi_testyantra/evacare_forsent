package com.forsenteva.testscripts.sprint2.Login;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest_Login;
/*
 * verifying can able to login with invalid email id
 */
public class Eva_CCA_Login_051Test extends BaseTest_Login{
	@Test
	public void Login_051Test() throws Throwable
	{
		InitializePages.Login_Loginpage.Login_051();
	}

}
