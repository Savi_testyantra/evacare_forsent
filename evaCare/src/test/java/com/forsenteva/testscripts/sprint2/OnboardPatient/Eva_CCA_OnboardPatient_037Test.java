package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;
/*
 * 	
 * 37 testcase
 * verifying emergency phone number is accepting phone number formate or not  
 */
public class Eva_CCA_OnboardPatient_037Test extends BaseTest{
	@Test
	public void OnboardPatient_037Test() throws Throwable
	{
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
		String sdata4 = ExcelDataProvider.readcolData(sdata1[34], "Email", GenericLib.testDataPath, "Sheet1");
		InitializePages.OnboardPatient.OnboardPatient_037(RandomStringUtils.randomAlphabetic(5),
				RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5)+sdata4,
				RandomStringUtils.randomNumeric(10), RandomStringUtils.randomNumeric(10), RandomStringUtils.randomAlphabetic(5));
	}

}
