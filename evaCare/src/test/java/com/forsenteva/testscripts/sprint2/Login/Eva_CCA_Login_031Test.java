package com.forsenteva.testscripts.sprint2.Login;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest_Login;
/*
 *Login_031 test case
 * verify the new password page contains password,enter again,submit button are displays are not
 */
public class Eva_CCA_Login_031Test extends BaseTest_Login{
@Test
public void Login_031Test() throws Throwable
{
	InitializePages.Login_New_Password.Login_031();
}
}
