package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_156Test extends BaseTest{
	@Test
	public void invalidUrlMessage() {
		InitializePages.OnboardPatientconversation.verifyInvalidUrlMessage("331443ghjgjkfhk");
	}

}
