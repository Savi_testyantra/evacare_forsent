package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
/*
 * verifying user can able to select future date or not
 */
public class Eva_CCA_OnboardPatient_023Test extends BaseTest {
	@Test
	public void OnboardPatient_023Test() throws Throwable
	{
		InitializePages.OnboardPatient.OnboardPatient_023();
	}

}
