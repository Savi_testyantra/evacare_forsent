package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
/*144 testcase
 * verifying the url having validation message is there or not in conversation page level 
 * 
 */
public class Eva_CCA_OnboardPatient_144Test extends BaseTest {
	@Test
	public void OnboardPatient_144Test() throws Throwable
	{
		InitializePages.OnboardPatientconversation.OnboardPatient_144();
	}

}
