package com.forsenteva.testscripts.sprint2.Login;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;

import com.forsenteva.web.library.BaseTest_Login;

/*
 * verifying the login the application with valid credentials
 */
public class Eva_CCA_Login_050Test extends BaseTest_Login{
	@Test()
	public void Login_050Test () throws Throwable
	{
		InitializePages.Login_Loginpage.Login_050();
	}

}
