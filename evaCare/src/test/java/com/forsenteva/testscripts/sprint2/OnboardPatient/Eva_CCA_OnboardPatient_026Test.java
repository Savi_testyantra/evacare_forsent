package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;
/*
 * verifying date filed is accepting decimal point instead of '/' symbol
 */
public class Eva_CCA_OnboardPatient_026Test extends BaseTest {
	@Test
	public void OnboardPatient_026Test() throws Throwable
	{
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
		String sdata4 = ExcelDataProvider.readcolData(sdata1[34], "Email", GenericLib.testDataPath, "Sheet1");
		
		InitializePages.OnboardPatient.OnboardPatient_026(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5),
				RandomStringUtils.randomAlphabetic(5)+sdata4, "09"+"."+"09"+"."+"2019",
				RandomStringUtils.randomNumeric(10),
				RandomStringUtils.randomNumeric(10), RandomStringUtils.randomAlphabetic(5));
	}
}
