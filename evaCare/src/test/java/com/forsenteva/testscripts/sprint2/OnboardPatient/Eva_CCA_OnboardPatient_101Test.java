package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_101Test extends BaseTest{
	private final String MOOD="Sad";
  @Test
  public void verifyModdField() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.OnboardPatientconversation.Eva_CCA_Onboard_Patient_101(MOOD);
  }
}