package com.forsenteva.testscripts.sprint2.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_102Test extends BaseTest{
	private final String MOOD1="Sad";
	private final String MOOD2="Happy";
  @Test
  public void verifyMoodFieldNotMultiValue() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.OnboardPatientconversation.Eva_CCA_Onboard_Patient_102(MOOD1,MOOD2);
  }
}
