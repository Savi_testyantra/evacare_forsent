package com.forsenteva.testscripts.sprint3.Events;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
/*
 * 7th test case
 * verifying previous year,day,month,week calender is displaying or not
 */
public class Eva_CCA_Events_008Test extends BaseTest {
	@Test
	public void Events_008Test() throws Throwable
	{
		InitializePages.EventsPage.Events_008();
	}
}
