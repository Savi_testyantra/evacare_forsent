package com.forsenteva.testscripts.sprint3.OnboardCaregiver;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.GenericLib;

public class Eva_CCA_OnboardCaregiver_028Test extends BaseTest {
	@Test
	public void OnboardCaregiver_028Test() throws Throwable
	{
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath, "Sheet1", "TEST_CASE_NO");
		String sdata4 = ExcelDataProvider.readcolData(sdata1[34], "Email", GenericLib.testDataPath, "Sheet1");
		InitializePages.OnboardCaregiver.OnboardCaregiver_028(RandomStringUtils.randomAlphabetic(5), 
				RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphanumeric(5)+sdata4, RandomStringUtils.randomNumeric(10), 
				RandomStringUtils.randomAlphabetic(5));
	}

}
