package com.forsenteva.testscripts.sprint3.Login;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest_Login;

/*
 * 12 testcase
 * Verifying Account for field is allowing multiple select data
 */
public class Eva_CCA_Login_012Test extends BaseTest_Login{
@Test
public void Login_012Test() throws Throwable
{
	InitializePages.Login_CreateAccount.Login_012();
}
}
