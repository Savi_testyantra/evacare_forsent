package com.forsenteva.testscripts.sprint3.OnboardPatient;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_OnboardPatient_224Test extends BaseTest{
	
	@Test
	public void validate224() throws Throwable {
		InitializePages.OnboardPatient_Events.Eva_CCA_Onboard_Patient_224();
	}

}
