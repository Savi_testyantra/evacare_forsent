package com.forsenteva.web.util;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.forsenteva.web.listener.MyExtentListener;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class WebActionUtil 
{
	public static WebDriver driver;
	static WebDriverWait wait;
	public long ETO = 3;
	
	public static JavascriptExecutor jsExecutor;
	public static Actions action;
	public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	public WebActionUtil(WebDriver driver, long ETO)
	{
		this.driver = driver;
		this.ETO = ETO;
		wait = new WebDriverWait(driver, ETO);
	
		jsExecutor = (JavascriptExecutor) driver;
		action = new Actions(driver);
	}
	/* Wait till the page to load */
	@SuppressWarnings("unused")
	public static void waitTillPageLoad(WebDriver driver, int seconds)
	{
		WebDriverWait wait = new WebDriverWait(driver, seconds); 
		jsExecutor = (JavascriptExecutor) driver;
		// Wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = wd -> ((JavascriptExecutor) driver)
				.executeScript("return document.readyState").toString().equals("complete");
		// Get JS is Ready
		boolean jsReady = (Boolean) jsExecutor.executeScript("return document.readyState").toString().equals("complete");
		// Wait Javascript until it is Ready!
		if (!jsReady) 
		{
			System.out.println("JS in NOT Ready!");
			// Wait for Javascript to load
			wait.until(jsLoad);
		} 
	else
	{
		sleep(2);
	}
		}
	public static String capture(WebDriver driver)
	{
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = System.getProperty("user.dir") + "/Reports/ScreensShot/QuadwaveScreenshot" + sDate + ".png";
		try 
		{
			File f = new File(destPath);
			if (!(f.exists()))
			{
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destPath;
	}
	/* 
	 * Verify the Element is Clickable or Not  
	 */
	public static boolean isElementClickable(WebElement element, String elementName) throws Throwable {
		// waitTillPageLoad();
		sleep(5);
		try {
			logger.info("---------Method is Element clickable  ---------");
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/* 
	 * Click on the Element 
	 */
	public static void clickOnElement(WebElement element, String elementName) throws Throwable 
	{
		try
		{
		    if (isElementClickable(element, elementName))
		    {
			   logger.info("---------Verifying element is displayed or not ---------");
			   waitTillPageLoad(driver, 30);
			   element.click();
			   MyExtentListener.logger.pass("Verify user is able to click on " + "\'" + elementName + "\'"
						+ " ||  User is able to click on " + "\'" + elementName + "\'");
		    } 
		    else 
		    {
		    	logger.info("-------Element is not Clickable--------------");
		    }
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}	
	}
	/*
	 * Clicking on submit 
	 */
	public static void submitOnElement(WebElement element, String elementName) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("---------Verifying element is displayed or not ---------");
				waitTillPageLoad(driver, 30);
				element.submit();
				MyExtentListener.logger.pass("Verify user is able to click on " + "\'" + elementName + "\'"
					+ " ||  User is able to click on " + "\'" + elementName + "\'");
			} 
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 * Click on the Element
	 */
	public static void enterOnElement(WebElement element, String elementName , String value) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName)) 
			{
				logger.info("----------- Verfiying user is able to click on the Element----------");
				waitTillPageLoad(driver, 30);
				try 
				{
					waitForElement(element, driver, elementName, 20);
				} 
				catch (Throwable e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				element.sendKeys(Keys.ENTER);
			} 
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}	
	}
	/*
	 * Clicking on the Element using JavaSCript 
	 */
	public static  void clickOnElementUsingJS(WebElement element, String elementName) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("-----------Clicking on the Element Using JS----------");
				jsExecutor.executeScript("arguments[0].click();", element);
			} 
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}	
	}
	/*
	 * Verify the Element is Displayed or Not
	 */
	public static  void isElementDisplayed(WebElement element, String elementName , int minutes) 
	{
		try 
		{
			logger.info("---------Waiting for visibility of element---------" + element);
			waitTillPageLoad(driver, 30);
			long timeout = minutes;
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);			
			logger.info("---------Element is visible---------" + element);
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to view the element " + "\'" + elementName
					+ "\'" + "  || User is unable to view " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to View" + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to view " + "\'" + elementName
					+ "\'" + " || User is unable to view " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	
	
	
	/*
	 * Wait for The element to Load or to Display
	 */
	public static void waitForElement(WebElement element, WebDriver driver, String eleName, int minutes)
			
	{
		try 
		{
			logger.info("---------Waiting for visibility of element---------" + element);
			waitTillPageLoad(driver, 30);
			long timeout = minutes;
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);			
			logger.info("---------Element is visible---------" + element);
		}
		catch (Exception e)
		{
			try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.info("---------Element is not visible---------" + element); 
			throw e;
		} 
		catch (AssertionError e)
		{
			try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
	}
	/*
	 * To Enter the Text to the Text filed
	 */
	public static void typeText(WebElement element, String value, String elementName)
	{
		
		try
		{
			waitTillPageLoad(driver, 20);
			waitForElement(element, driver, elementName, 1);
			logger.info("Enter the value into" + elementName);
			element.sendKeys(value);
			logger.info("User is able to type " + value + " into " + elementName);
		} 
		catch (AssertionError error)
		{
			logger.info(" User is not able to type " + value + " into " + elementName);
			 try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("Unable to type on " + elementName);
		}
		catch (Exception e)
		{
			logger.info(" User is not able to type " + value + "into " + elementName);
			try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Assert.fail("Unable to type in " + elementName);
		}
	}
	/*
	 * Clear the Text field and Enter the Text to the Text filed
	 */
	public static void clearAndTypeText(WebElement element, String value, String elementName)
	{
		try 
		{
			logger.info("Clear the Field and Enter the Text ");
			sleep(3);
			element.clear();
			logger.info(elementName + " is cleared");
			element.sendKeys(value);
			logger.info(value + " is entered in " + elementName);
			logger.info(" User is able to type " + value + " into " + elementName);
		} 
		catch (AssertionError error)
		{
			logger.info( "User is not able to type " + value + " into " + elementName);
			try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("Unable to type on " + "\'" + elementName + "\'");
		} 
		catch (Exception e)
		{
			logger.info("User is not able to type " + value + " into " + elementName);
			try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	/*
	 * Get the Text of the Element
	 */
	public static String getText(WebElement element, String elementName)
	{
		logger.info("Get the text from the element:");
		String eleText = null;
		try
		{
			waitTillPageLoad(driver, 20);
			isElementDisplayed(element, elementName, 1);
			eleText = element.getText();
			if (eleText.equals(null)) 
			{
			logger.info("Unable to fetch text from " + "\'" + elementName);
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
				Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");
			}
		} 
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to get the text of the element " + "\'" + elementName
					+ "\'" + "  || User is unable to get the text of " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to get the text of " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to get the text of " + "\'" + elementName
					+ "\'" + " || User is unable to get the text of " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return eleText;
	}
	/*
	 * Scroll to the End of the page
	 */
	public static void scrollToEndOfThePage() throws Throwable
	{
		sleep(5);
		logger.info("----------Scrolling till End of the Page-------");
		try
		{
			jsExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			logger.info("---------Successfully Scrolled till End of the Page---------");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Scroll to end of the Page " + "\'" + "  || User is unable to scroll to end of the page " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Scroll to end of the page" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll to end of the page " + "\'" 
					+ "\'" + " || User is unable to scroll to end of the page " + "\'" + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 * Scroll to the Element
	 */
	public static void scrollToElement(WebElement element, String elementName) throws Throwable
	{
		waitTillPageLoad(driver, 20);
		logger.info("-------------Scrolling till the Element------------");
		try
		{
			jsExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll to the " + "\'" + elementName
					+ "\'" + "  || User is unable to scroll to the " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to scroll to " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll to " + "\'" + elementName
					+ "\'" + " || User is unable to scroll to " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 * Scroll to the particular pixel
	 */
	public static void scrollByPixel(double xpixels, double ypixels) throws Throwable
	{
		waitTillPageLoad(driver, 20);
		try
		{
		logger.info("Scrolling X-axis=" + xpixels + "and Y-axis=" + ypixels);
		jsExecutor.executeScript("window.scrollBy(" + xpixels + "," + ypixels + ")");
		logger.info("----------Scrolling Completed---------");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll by " + "\'" + xpixels+" "+ypixels
					+ "\'" + "  || User is unable to scroll by " + "\'" + xpixels+" "+ypixels + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Scroll by" + "\'" + xpixels+" "+ypixels + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll by " + "\'" + xpixels+" "+ypixels
					+ "\'" + " || User is unable to scroll by " + "\'" + xpixels+" "+ypixels + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 * Verify the Text
	 */
	public static void verifyElementText(WebElement element, String expectedText)
	{
		sleep(3);
		String actualText = element.getText();
		sleep(3);
		try
		{
		Assert.assertEquals(actualText, expectedText);
		logger.info(actualText + " is matching with " + expectedText);
		}
	catch (AssertionError error) 
	{
		MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify the text of " + "\'" + element
				+ "\'" + "  || User is unable to verify the text of " + "\'" + element + "\'", ExtentColor.RED));
		try 
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.fail("unable to verify the text of" + "\'" + element + "\'");
		try 
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw error;
	} 
	catch (Exception e) 
	{
		MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify the text of " + "\'" + element
				+ "\'" + " || User is unable to verify the text of " + "\'" + element + "\'", ExtentColor.RED));
		try 
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		throw e;
	}
	}
	/*
	 * Verify the page Title
	 */
	public static void verifyTheTitle(String expectedTitle) 
	{
		sleep(3);
		String actualTitle = driver.getTitle();
		logger.info(":" + actualTitle);
		try
		{
		Assert.assertEquals(actualTitle, expectedTitle);
		logger.info("Compare 'Actual title' with the 'Expected Title' ");
		logger.info( actualTitle + " is matching with " + expectedTitle);
		}
		
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify the title " + "\'" 
					+ "\'" + "  || User is unable to view " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to verify the title" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify the title " + "\'" 
					+ "\'" + " || User is unable to verify the title " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 *  Wait in seconds 
	 */
	public static void sleep(long seconds)
	{
		try
		{
			Thread.sleep(seconds * 1000);
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	/*
	 * Get the Web Element 
	 */
	/*
	 *  Send the name Address of the element and name of the locator
	 */
	public static WebElement getWebElement(String locatorType, String locatorValue)
	{
		waitTillPageLoad(driver, 20);
		try
		{
			By by = (By) By.class.getDeclaredMethod(locatorType, String.class).invoke(null, locatorValue);
			WebElement element = driver.findElement(by);
			return element;
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	/* 
	 * Navigate To URL
	 */
	public static void navigateToUrl(String url) 
	{
		waitTillPageLoad(driver, 0);
		logger.info("Navigate to Url");
		driver.navigate().to(url);
	}
	/*
	 *  Refresh the Page
	 */
	public static  void refreshThePage()
	{
		driver.navigate().refresh();
		waitTillPageLoad(driver, 20);
	}
	/*
	 *  navigate To back
	 */
	public static void navigateToback() 
	{
		waitTillPageLoad(driver, 20);
		logger.info("Navigate back ");
		driver.navigate().back();
	}
	/*
	 *  Verifying the Element Displayed Or Not
	 */
	public static boolean isElementDisplayedOrNot(WebElement element) 
	{
		sleep(3);
		boolean actual = element.isDisplayed();
		try
		{
		Assert.assertEquals(actual, true, "Element is Not Displayed");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to view the element " + "\'" + element
					+ "\'" + "  || User is unable to view " + "\'" + element + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to View" + "\'" + element + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to view " + "\'" + element
					+ "\'" + " || User is unable to view " + "\'" + element + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
		return actual;
		
	}
	/*
	 *  Switch To Tab 
	 */
	public static void switchToTab(int tabindex)
	{
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(tabindex));
	}
	/*
	 *  Switch To Window 
	 */
	public static void switchToWindow(String nameOrHandle)
	{
		try 
	    {
	        logger.info("---------Verifying Window is displayed or not ---------"); 
	    ArrayList<String> listOfWindow = new ArrayList<String>(driver.getWindowHandles());
	     driver.switchTo().window(listOfWindow.get(0));
	     MyExtentListener.logger.pass("Verify user is able to switch to window" + "\' "
	                + " ||  User is able to switch to window " + "\'");
	    } 
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to switch to window" + "\'"
					+ "\'" + "  || User is unable to switch to window " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to switch to window" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to switch to window " + "\'" 
					+ "\'" + " || User is unable to switch to window " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 *  Switching into the frame
	 */
	public static  void switchToFrame(WebElement element) 
	{
		try
		{
			
			waitTillPageLoad(driver, 25);
			logger.info("---------Verifying Frame is displayed or not ---------");
			logger.info("Switching into the frame");
			driver.switchTo().frame(element);
			 MyExtentListener.logger.pass("Verify user is able to  Switch to Frame " + "\' "
		                + " ||  User is able to Switch to Frame " + "\'");
	     }
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to switch to frame " + "\'" 
					+ "\'" + "  || User is unable to switch to frame " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to switch to frame" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to switch to frame " + "\'" 
					+ "\'" + " || User is unable to switch to frame " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
		}
	/*
	 *  Switching into the top window/first frame
	 */
	public static void defaultWindowContent()
	{
		waitTillPageLoad(driver, 30);
		logger.info("Switching into the top window/first frame");
		driver.switchTo().defaultContent();
		 MyExtentListener.logger.pass("Verify user is able to  Switch to Default Content " + "\' "
	                + " ||  User is able to Switch to Default Content " + "\'");
	}
	
	/*
	 *  Upload file
	 */
	//File upload by Robot Class
	   public static void uploadFileWithRobot (String imagePath) {
	       StringSelection stringSelection = new StringSelection(imagePath);
	       Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	       clipboard.setContents(stringSelection, null);
	       Robot robot = null;
	       try {
	           robot = new Robot();
	       robot.delay(250);
	       robot.keyPress(KeyEvent.VK_ENTER);
	       robot.keyRelease(KeyEvent.VK_ENTER);
	       robot.keyPress(KeyEvent.VK_CONTROL);
	       robot.keyPress(KeyEvent.VK_V);
	       robot.keyRelease(KeyEvent.VK_V);
	       robot.keyRelease(KeyEvent.VK_CONTROL);
	       robot.keyPress(KeyEvent.VK_ENTER);
	       robot.delay(150);
	       robot.keyRelease(KeyEvent.VK_ENTER);
	       }
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to upload " + "\'" 
					+ "\'" + "  || User is unable to upload " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to upload" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to upload " + "\'" 
					+ "\'" + " || User is unable to upload " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	/* Actions Class methods */

	/* 
	 * Hover the mouse over element 
	 */
	public static void mouseHover(WebElement element, String elementName) 
	{
		try 
		{
			sleep(10);
			// waitTillPageLoad();
			logger.info("Hovering over the " + elementName);
			Actions action = new Actions(driver);
			action.moveToElement(element).build().perform();
			 MyExtentListener.logger.pass("Verify user is able to MouseHover " + "\' "
		                + " ||  User is able to MouseHover " + "\'");
		} 
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to mouseHover on " + "\'" + elementName
					+ "\'" + "  || User is unable to mouseHover on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to mouseHover on" + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to mouseHover on " + "\'" + elementName
					+ "\'" + " || User is unable to mouseHover on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}

	public static void mouseHover2(WebElement element, String elementName) 
	{
		try
		{
			sleep(10);
			logger.info("Hovering over the " + elementName);
			Actions action = new Actions(driver);
			action.sendKeys(element, Keys.ENTER).perform();
			MyExtentListener.logger.pass("Verify user is able to MouseHover " + "\' "
	                + " ||  User is able to MouseHover " + "\'");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to mouseHover on " + "\'" + elementName
					+ "\'" + "  || User is unable to mouseHover on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to mouseHover on" + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to mouseHover on " + "\'" + elementName
					+ "\'" + " || User is unable to mouseHover on " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
/*
 * Getting the value of the Attribute
 */
	public static void getAttributeValue(WebElement element, String elementName, String attributeName) {
        try {
            logger.info("Getting the attribute value of " + elementName);
            String attribute = element.getAttribute(attributeName);
            logger.info("Attribute value of the " + elementName + "is " + attribute);
            MyExtentListener.logger.pass("Verify user is able get the attribute of " + elementName
                    + " ||  User is able to get the attribute of " + elementName );
        } catch (AssertionError error)
        {
            MyExtentListener.logger.fail(MarkupHelper.createLabel("User is not able to get the attribute of " + elementName +
                     "\'", ExtentColor.RED));
            throw error;
        }
        catch (Exception e)
        {
            MyExtentListener.logger.fail(MarkupHelper.createLabel("User is not able to get the attribute of " + elementName , ExtentColor.RED));
            throw e;
        }
    }
	/*
	 *  Drag And Drop
	 */
	public static void dragAndDrop(WebElement source, WebElement target)
	{
		try
		{
			logger.info("Drag the " + source + " and Drop to" + target);
			sleep(1);
			action.dragAndDrop(source, target).perform();
			MyExtentListener.logger.pass("Verify user is able to Drag and Drop " + "\' "
	                + " ||  User is able to Drag and Drop " + "\'");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to drag and drop " + "\'" + "\'" + "  || User is unable to drag and drop " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to drag and drop" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to drag and drop " + "\'" 
					+ "\'" + " || User is unable to drag and drop " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}

	/*
	 *  Double Click On Element 
	 */
	public static void doubleClickOnElement(WebElement element) 
	{
		try
		{
			logger.info("Double click on the " + element);
			action.doubleClick(element).perform();
			MyExtentListener.logger.pass("Verify user is able to Double Click on " + element
	                + " ||  User is able to Double Click on " + element );
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to double click on " + "\'" + element
					+ "\'" + "  || User is unable to double click on " + "\'" + element + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to double click on " + "\'" + element + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to double click on " + "\'" + element
					+ "\'" + " || User is unable to double click " + "\'" + element + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}

	/*
	 *  Arrow Down 
	 */
	public static  void arrowDown()
	{
		try
		{
			logger.info("Press Arrow Key Down");
			action.sendKeys(Keys.ARROW_DOWN).perform();
			MyExtentListener.logger.pass("Verify user is able to Press Arrow key down " + "\' "
	                + " ||  User is able to Press Arrow Key down " + "\'");
		}
	
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press down arrow " + "\'"
					+ "\'" + "  || User is unable to press down arrow " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to press down arrow" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press down arrow " + "\'" 
					+ "\'" + " || User is unable to press down arrow " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}

	/* Arrow Up */
	public static  void arrowUp()
	{
		try
		{
			logger.info( "Press Arrow Key Up");
			action.sendKeys(Keys.ARROW_UP).perform();
			MyExtentListener.logger.pass("Verify user is able to Press Arrow key Up " + "\' "
	                + " ||  User is able to Press Arrow Key Up " + "\'");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press up arrow " + "\'"
					+ "\'" + "  || User is unable to press up arrow " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to press up arrow" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press up arrow " + "\'" 
					+ "\'" + " || User is unable to press up arrow " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}

	/*
	 *  Arrow Up And Enter 
	 */
	public static void arrowUpAndEnter()
	{
		try
		{
			logger.info("Press Arrow Key Up And Press Enter");
			action.sendKeys(Keys.ARROW_UP, Keys.ENTER).perform();
			MyExtentListener.logger.pass("Verify user is able to Press Arrow key Up & Press Enter" + "\' "
	                + " ||  User is able to Press Arrow Key Up & Press Enter " + "\'");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press up arrow and Enter " + "\'"
					+ "\'" + "  || User is unable to press up arrow and Enter " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to press up arrow and Enter " + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press up arrow and Enter " + "\'" 
					+ "\'" + " || User is unable to press up arrow and Enter " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}

	/*
	 *  Arrow Down And Enter
	 */
	public static void arrowDownAndEnter()
	{
		try
		{
			logger.info("Press Arrow Key Down And Press Enter");
			action.sendKeys(Keys.ARROW_DOWN, Keys.ENTER).perform();
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press down arrow and Enter " + "\'"
					+ "\'" + "  || User is unable to press down arrow and Enter" + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to press down arrow and Enter" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press down arrow and Enter" + "\'" 
					+ "\'" + " || User is unable to press down arrow and Enter " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
/*
 * Shift and Delete
 */
	public static  void pressShiftEndDelete()
	{
		logger.info("Press Shift End Delete");
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_SHIFT);
			r.keyPress(KeyEvent.VK_END);
			sleep(1);
			r.keyRelease(KeyEvent.VK_END);
			r.keyRelease(KeyEvent.VK_SHIFT);
			sleep(1);
			r.keyPress(KeyEvent.VK_DELETE);
			sleep(1);
			r.keyRelease(KeyEvent.VK_DELETE);
			logger.info("Delete Completed");

		} catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press Shift and delete " + "\'"
					+ "\'" + "  || User is unable to press shift and delete " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to press shift and delete " + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to press shift and delete " + "\'" 
					+ "\'" + " || User is unable to press shift and delete " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	/*
	 *  Scroll to the Top of the page
	 */
	public static void scrollToTopOfThePage()
	{
		try
		{
			logger.info("Scrolling till the Top of the page");
			waitTillPageLoad(driver, 30);
			logger.info("Scrolling till the Top of the page");
			jsExecutor.executeScript("window.scrollTo(0, -document.body.scrollHeight)");
			logger.info("Scrolling Completed");
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll to top of the page " + "\'"
					+ "\'" + "  || User is unable to scroll to top of the page " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to scroll to top of the page" + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to scroll to top of the page " + "\'" 
					+ "\'" + " || User is unable to scroll to top of the page " + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}	
//	public static void logMessage(LogStatus status, String msg)
//	{
//		System.out.println("Status:" + status + " Msg:" + msg);
//	}
	
	public static void randomGenerateNumber() {
		 //String firstname = RandomStringUtils.randomAlphabetic(10);
		 String lastname = RandomStringUtils.randomAlphabetic(5);
		// String email = RandomStringUtils.randomAlphabetic(10);
		// String phonenumber = RandomStringUtils.randomNumeric(10);
		// ephone = RandomStringUtils.randomNumeric(10);
		// String econtact = RandomStringUtils.randomAlphabetic(10);
		
	}
	
	/* selecting dropdown by value */
	public static void selectbyValue(WebDriver driver, WebElement element, String value) throws IOException {
        try {
            waitTillPageLoad(driver, 30);
            Select sel = new Select(element);
            sel.selectByValue(value);
            MyExtentListener.logger.pass("Verify user is able to select by value for " + "\'" + element + "\'"
                    + " ||  User is able to select by value for " + "\'" + element + "\'");
        } catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to select by value for " + "\'"
				+element	+ "\'" + "  || User is unable to select by value for "+ element + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to select by value for "+ element + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to select by value for "+ element + "\'" 
					+ "\'" + " || User is unable to select by value for " + element + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
    }
	/*
	 * selectiong dropdown byIndex
	 */
	public static void selectbyIndex(WebDriver driver, WebElement element, int index) {
        try {
            waitTillPageLoad(driver, 30);
            Select sel = new Select(element);
            sel.selectByIndex(index);
            MyExtentListener.logger.pass("Verify user is able to select by index  " + "\'" + index + "\'"
                    + " ||  User is able to select by index " + "\'" + index + "\'");
        } 
        catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to select by index for " + "\'"
				+element	+ "\'" + "  || User is unable to select by index for "+ element + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to select by index for "+ element + "\'"  + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to select by index for "+ element + "\'" 
					+ "\'" + " || User is unable to select by index for " + element + "\'"  + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
    }
	/*
	 * selecting dropdown by text
	 */
	public static void verifyContainsText(String actResult, String expResult, String desc)
	{
        if (actResult.contains(expResult)) {
            MyExtentListener.logger.pass("Verify  Expected : " + "\'" + expResult + "\''" + " contains  Actual :  "
                    + actResult + "  || Expected : " + "\'" + expResult + "\''" + "contains  Actual :  " + actResult);
        } else {
            MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify  Expected : " + "\'" + expResult + "\''"
                    + " contains  Actual :  " + actResult + " ||  Expected : " + "\'" + expResult + "\''"
                    + " does not contains  Actual :  " + actResult, ExtentColor.RED));
            try {
				throw new Exception();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
	
	/*
	 * @author: Sindhu N 
	 *
	 * Description: This method returns all options present in dropdown
	 * 
	 */
	public static List<WebElement> getDropDownOptions(WebElement element) {
	    // driver.find
	    logger.info("selecting value from dropdown");
	    Select dropDown = new Select(element);
	    return dropDown.getOptions();
	}
	/*
	 * @author: Sindhu N 
	 *
	 * Description: This method checks whether the element is enabled or not 
	 * 
	 */
	public static void isElementEnabled(WebElement element) {
        try {
logger.info("checking whether element is enabled");
boolean actual = element.isEnabled();
Assert.assertEquals(actual, true, "Element is Not Enabled");
MyExtentListener.logger.pass("Verify user is able to Double Click on " + element
               + " ||  User is able to Double Click on " + element );
}catch (AssertionError error) 
		{
	MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to view the element " + "\'" + element
			+ "\'" + "  || User is unable to view " + "\'" + element + "\'", ExtentColor.RED));
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Assert.fail("unable to View" + "\'" + element+ "\'");
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	throw error;
} 
catch (Exception e) 
{
	MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to view " + "\'" + element
			+ "\'" + " || User is unable to view " + "\'" + element + "\'", ExtentColor.RED));
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e1) 
	{
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	throw e;
}
    }
	
	/* Verify the boolean values */
    public static void verifyTheBoolean(boolean expectedValue,boolean actualValue,String message) {
        logger.info("verifying boolean values");
        Assert.assertEquals(actualValue, expectedValue, message);
    }
    
    /* click ok in alertbox*/
    public static boolean verifyAlertPresent() {
        try 
        { 
            logger.info("checking whether alert is present");
            driver.switchTo().alert().accept(); 
            return true;
        }   
        
        catch(NoAlertPresentException Ex) {
            Ex.printStackTrace();
            return false;
        }
    }
    public static String formatDuration(final long millis) {
    	   long seconds = (millis / 1000) % 60;
    	   long minutes = (millis / (1000 * 60)) % 60;
    	   long hours = millis / (1000 * 60 * 60);

    	   StringBuilder b = new StringBuilder();
    	   b.append(hours == 0 ? "00" : hours < 10 ? String.valueOf("0" + hours) :
    	   String.valueOf(hours));
    	   b.append(":");
    	   b.append(minutes == 0 ? "00" : minutes < 10 ? String.valueOf("0" + minutes) :    
    	   String.valueOf(minutes));
    	   b.append(":");
    	   b.append(seconds == 0 ? "00" : seconds < 10 ? String.valueOf("0" + seconds) :
    	   String.valueOf(seconds));
    	   return b.toString();
    	}
    /* Verify the check box is clicked or not and to click if not clicked */
	public static void clickCheckBox(WebElement element, String elementname) 
	{
		
		try
		{waitTillPageLoad(driver, 30);
		if (element.isSelected())
		{
			logger.info("The check box is clicked");
		} else
		{
			logger.info("Click on check box ");
			element.click();
			logger.info( "The check box is clicked");
		}
		 MyExtentListener.logger.pass("Verify user is able to  ClickCheck Box " + "\' "
	                + " ||  User is able to Click Checkbox " + "\'");
		}
		catch (AssertionError error)
	    {
	        MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Click Checkbox " + "\'" + 
	                 "\'", ExtentColor.RED));
	        throw error;
	    } 
	    catch (Exception e)
	    {
	        MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Click Checkbox" + "\'" + " || User is not able to Click Checkbox" + "\'", ExtentColor.RED));
	        throw e;
	    }
	
	}
	/*
	 * @author:Aatish
	 * 
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */
	public static void clear(WebElement element,  String elementName)
	{
		try 
		{
			logger.info("Clear the Field and Enter the Text ");
			sleep(3);
			element.clear();
			logger.info(elementName + " is cleared");
		} 
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to clear " + "\'" + elementName
					+ "\'" + "  || User is unable to clear " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to clear" + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to clear " + "\'" + elementName
					+ "\'" + " || User is unable to clear " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/*
	 * @author:Aatish
	 * 
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */
	
	public static boolean isSelected(WebElement element, String elementName)
	{
		// waitTillPageLoad();
		sleep(5);
		try {
			logger.info("---------Method is Element Selected  ---------");
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.until(ExpectedConditions.elementToBeSelected(element));
			return true;
		}
		catch (AssertionError error) 
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify element is selected " + "\'" + elementName
					+ "\'" + "  || unable to select " + "\'" + elementName + "\'", ExtentColor.RED));
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to select the " + "\'" + elementName + "\'");
			try 
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		} 
		catch (Exception e)
		{
			MyExtentListener.logger
					.fail(MarkupHelper.createLabel(
							"Verify element is selected " + "\'" + elementName + "\'"
									+ " ||  Element is not selected " + "\'" + elementName + "\'",
	
									ExtentColor.RED));
			try {
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Assert.fail("Unable to select " + elementName);
		}
		return false;
	}
	/*
	 * @author:Aatish
	 * 
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */
	public static boolean isAlertPresent(WebDriver driver, int sec) {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(sec, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MILLISECONDS).ignoring(UnreachableBrowserException.class);
		boolean alerPresent = wait.until(ExpectedConditions.alertIsPresent()) != null;
		if (alerPresent) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public static void isElementNotEnabled(WebElement element) 
	{
        try {
logger.info("checking whether element is enabled");
boolean actual = element.isEnabled();
Assert.assertNotEquals(actual, true, "Element is Enabled");
MyExtentListener.logger.pass("Verify Element is enabled " + element
               + " ||  Element is not enabled " + element );
}catch (AssertionError error) 
		{
	MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify element is enabled " + "\'" + element
			+ "\'" + "  || Element is not enabled " + "\'" + element + "\'", ExtentColor.RED));
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Assert.fail("Element is not enabled " + "\'" + element+ "\'");
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	throw error;
} 
	}


public static void verifyAttributeValue(WebElement element, String attributeName, String attributeValue) {
try {
logger.info("Getting the attribute value of " + element);
String attribute = element.getAttribute(attributeName);
logger.info(attribute);
boolean value = attribute.contains(attributeValue);
Assert.assertEquals(value, true, "Attribute value not matching");
MyExtentListener.logger.pass("Verify user is able to Double Click on " + element
               + " ||  User is able to Double Click on " + element );
} catch (AssertionError error) 
{
	MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify Attribute value is getting verified " + "\'" + element
			+ "\'" + "  || Unable to verify attribute value " + "\'" + element + "\'", ExtentColor.RED));
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Assert.fail("unable to verify attribute value" + "\'" + element + "\'");
	try 
	{
		MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
	} 
	catch (IOException e) 
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	throw error;
} 
}

//generate random string email
public static String generateEmail( String domain, int length)
{
    try {    
                logger.info("Creating a Random String for Email");
                MyExtentListener.logger.pass("Creating a Random Email of" + domain +"|| Able to create Random Email of "+domain);
                return RandomStringUtils.random(length, "abcdefghijklmnopqrstuvwxyz") + "@" + domain;
    }    
    catch (Exception e)
    {
        MyExtentListener.logger.fail(MarkupHelper.createLabel("Creating a Random Email of " + domain + " || Unable to create Random Email of " + domain, ExtentColor.RED));
        throw e;
        
        
    }    
  
}
/*
 * check either Special Character , Character and Numbers are present in a string or not.
 */
public static void verifySpecialCharacter(String name) throws IOException {
    try {
        logger.info("---------Verifying  Special character present are not---------");
        String specialCharacters = " !#$%&'()*+,-./:;<=>?@[]^_`{|}~0123456789[a-zA-Z]";
        String str2[] = name.split(".");
        int count = 0;
        for (int i = 0; i < str2.length; i++) {
            if (specialCharacters.contains(str2[i])) {
                count++;
            }
        }
        if (name != null && count == 0) {
            System.out.println("Special char is present");
        } else {
            System.out.println("Special char is not present");
        }
        MyExtentListener.logger.pass("Verify Special character are present" + "\'" + name + "\'"
                + " ||  User is able to select " + "\'" + name + "\'");
    }
    catch (AssertionError error) 
	{
		MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify Special Characters are present " + "\'" 
				+ "\'" + "  || Unable to find special characters " + "\'"  + "\'", ExtentColor.RED));
		try 
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.fail("unable to find special characters" + "\'"  + "\'");
		try 
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw error;
	} 
	catch (Exception e) 
	{
		MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to find special characters " + "\'" 
			+ "\'" + " || User is unable to find special characters " + "\'"   + "\'", ExtentColor.RED));
		try 
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		throw e;
	}
}
public static void handlingCalender(WebElement clalender, String i,int month,int year) throws InterruptedException
{ 
    String expDate = null;
     // Calendar Month and Year
     String calMonth = null;
     String calYear = null;
     boolean dateNotFound;
     dateNotFound = true;
      List monthList = Arrays.asList("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
      
      WebElement w=driver.findElement(By.xpath("//button[@class='btn btn-default']"));
      Actions a=new Actions(driver);
      a.moveToElement(w);
      a.click().build().perform();
      
     while(dateNotFound)
      { 
       //Retrieve current selected month name from date picker popup.
         String reqMonthandYear = driver.findElement(By.xpath("//button[contains(@id,'datepicker')]//strong")).getText();
       //Retrieve current selected year name from date picker popup.
         
         String reqMonth  = reqMonthandYear.split("\\s")[0];
         String reqYear  = reqMonthandYear.split("\\s")[1];
         
       
       //If current selected month and year are same as expected month and year then go Inside this condition.
       if(monthList.indexOf(reqMonth)+1 == month && (year == Integer.parseInt(reqYear)))
       {
        //Call selectDate function with date to select and set dateNotFound flag to false.
        selectDate(i);
        dateNotFound = false;
       }
       //If current selected month and year are less than expected month and year then go Inside this condition.
       else if(monthList.indexOf(reqMonth)+1 <  month && (year == Integer.parseInt(reqYear)) || year > Integer.parseInt(reqYear))
       {
        //Click on next button of date picker.
           driver.findElement(By.xpath("//span[text()='Next']")).click();
       }
       //If current selected month and year are greater than expected month and year then go Inside this condition.
       else if(monthList.indexOf(reqMonth)+1 > month && (year == Integer.parseInt(reqYear)) || year < Integer.parseInt(reqYear))
       {
        //Click on previous button of date picker.
           driver.findElement(By.xpath("//span[text()='previous']")).click();
       }
      }
      Thread.sleep(3000);
     } 
        
  
  
  
    public static void selectDate(String i)
     {
     
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(1000,5000)");
      List<WebElement>  allDates =  driver.findElements(By.xpath("//*[contains(@id,'datepicker')]//span[@class='ng-binding']"));
      for(WebElement d:allDates)
        {
            String dText = d.getText();
                if(dText.equalsIgnoreCase(i))
            {
                 
                 d.click();
                 break;
                }
        
       }
}
   /*
    * Clicking on Tab button 
    */
    public static void pressTab(WebElement element, String elementName) throws Throwable
    {
        try
        {
            if (isElementClickable(element, elementName))
            {
                logger.info("----------- Verfiying user is able to press tab key from keyboard----------");
                waitTillPageLoad(driver, 30);
                try
                {
                    waitForElement(element, driver, elementName, 20);
                }
                catch (Throwable e)
                {
                    e.printStackTrace();
                }
                element.sendKeys(Keys.TAB);
            }
        }
        catch (AssertionError error)
        {
            MyExtentListener.logger.fail(MarkupHelper.createLabel(
                    "Verify user is able to press tab from keyboard || User is not able to press tab key from keyboard " + elementName + "\'",
                    ExtentColor.RED));
            MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
            Assert.fail("Unable to press tab key  " + elementName);
        }
    }
    

	/**
	 * Waits for JavaScript asynchronous logic in the web page to finish (Angular,
	 * React, etc).
	 *
	 * @throws Exception
	 */
	public static void waitForAsyncCallsToFinish(WebDriver driver) throws Exception {
		try {
			JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
			NgWebDriver ngDriver = new NgWebDriver(jsExecutor);
			ngDriver.waitForAngularRequestsToFinish();
		} catch (Exception ex) {

			throw new Exception("The waitForAsyncCallsToFinish method failed" + ex);

		}
	}

	/**
	 * Waits for page to be ready
	 *
	 * @throws Exception
	 */
	public static void waitToBeReady(JavascriptExecutor javascriptExecutor) {
		new NgWebDriver(javascriptExecutor).waitForAngularRequestsToFinish();
	}

	/**
	 * Select Value from List
	 * 
	 * @throws Throwable
	 */

	public static void selectOption(List<WebElement> optionDrpDwn, String option, WebDriver driver,
			String validationType) throws Throwable {

		for (int i = 0; i < optionDrpDwn.size(); i++) {

			if (validationType.equalsIgnoreCase("Equals")) {
				if (optionDrpDwn.get(i).getText().trim().equals(option)) {

					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("arguments[0].scrollIntoView(true);", optionDrpDwn.get(i));
					WebActionUtil.clickOnElement(optionDrpDwn.get(i), option);
					break;
				}

			} else if (validationType.equalsIgnoreCase("Contains")) {
				if (optionDrpDwn.get(i).getText().trim().contains(option)) {
					WebActionUtil.clickOnElement(optionDrpDwn.get(i), option);
					break;
				}
			}

		}
		waitForAsyncCallsToFinish(driver);
	}
}
