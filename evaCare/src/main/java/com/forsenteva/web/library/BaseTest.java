package com.forsenteva.web.library;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.seleniumhq.jetty9.server.HomeBaseWarning;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.Login_Page;
import com.forsenteva.web.util.WebActionUtil;

import io.github.bonigarcia.wdm.WebDriverManager;


public class BaseTest 
{
	public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	public WebDriver driver;
	public String logPath = "";
	public boolean skipTest = false;
	public long ETO = 10, ITO = 10;
	public WebActionUtil webActionUtil;	
	
	public ExcelDataProvider excelLibrary = new ExcelDataProvider();
	String sdata2;
	public static Properties prop;

	@BeforeClass
	public void _LaunchApp() throws Exception {
		
		try {
			prop=new Properties();
			FileInputStream fis=new FileInputStream(GenericLib.configPath);
			prop.load(fis);
			
		}catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
			String browser=prop.getProperty("Browser");
		
			if (browser.equalsIgnoreCase("firefox"))
			{

				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.addPreference("dom.webnotifications.enabled", false);
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver(firefoxOptions);			
			} 
			else if (browser.equalsIgnoreCase("safari")) 
			{
				driver = new SafariDriver();
				logger.info("Running in the 'Safari' Browser - Local");
				
			} 
			else
			{
				logger.info("Running in the 'Chrome' Browser - Local");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications");
				WebDriverManager.chromedriver().arch64().arch32().setup();
				driver = new ChromeDriver(options);
				logger.info("Running in the 'Chrome' Browser - Local");
			
			}
			driver.manage().timeouts().implicitlyWait(ITO, TimeUnit.SECONDS);
			webActionUtil = new WebActionUtil(driver, ETO);
			
		}

	
	@AfterClass()
	public void _CloseApp() {

		/* Close the browser */
	try {
			if (driver != null) {
				driver.close();
				logger.info("Closing Browser");
				
			} else {
				logger.info("@AfterClass driver instance is null");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
			
		}



	}

	
	
	@BeforeMethod
	public void _SignInToApp() throws Throwable {
		
		HomePage home=new HomePage(driver);

		/* Enter the application URL and login to the application */
		try {
			if (skipTest) {
				logger.info("Skipping BeforeMethod");
			
			} else {
				logger.info("Open the eva Application...");
			
				driver.get(prop.getProperty("url"));
				driver.manage().window().maximize();

				Login_Page loginPage = new Login_Page(driver);
				logger.info("Enter the Credentials");
				
				InitializePages pages=new InitializePages(driver);
				
				int indexOfUserName=ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
				int indexOfpass=ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");
				
				 String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
				 String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
				 loginPage.loginToApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
				  	
				 
				
				 
			}
			
				 
	}
		catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		}
	}	
}
