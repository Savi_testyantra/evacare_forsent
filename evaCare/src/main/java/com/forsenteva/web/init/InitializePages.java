package com.forsenteva.web.init;

import org.openqa.selenium.WebDriver;
import com.forsenteva.web.pages.Eva_CCA_CaregiverPage;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatientPage;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatient_AddNewPage;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatient_ConversationPage;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatient_EventsPage;
import com.forsenteva.web.pages.Eva_CCA_EventsPage;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.Eva_CCA_Login_CreateAccountPage;
import com.forsenteva.web.pages.Eva_CCA_Login_ForgotPasswordPage;
import com.forsenteva.web.pages.Eva_CCA_Login_Homepage;
import com.forsenteva.web.pages.Eva_CCA_Login_Loginpage;
import com.forsenteva.web.pages.Eva_CCA_Login_NewPasswordPage;
import com.forsenteva.web.pages.Login_Page;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatient_PersonalInterestsPage;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatient_PersonalizedMusicAndMoviesPage;
import com.forsenteva.web.pages.Eva_CCA_OnboardPatient_RegisteredDevicesPage;


public class InitializePages
{
	public Login_Page oLogin_Page;
	public HomePage homepage;
	public static Eva_CCA_OnboardPatient_RegisteredDevicesPage OnboardPatient_Registered;
	public static Eva_CCA_OnboardPatient_PersonalInterestsPage OnboardPatient_Personal_Interests;
	public static Eva_CCA_OnboardPatient_PersonalizedMusicAndMoviesPage OnboardPatient_PersonalizedMusicAndMovies;
	public static Eva_CCA_Login_Loginpage Login_Loginpage;
	public static Eva_CCA_Login_Homepage Login_Homepage;
	public static Eva_CCA_Login_ForgotPasswordPage Login_ForgotPassword;
	public static Eva_CCA_Login_NewPasswordPage Login_New_Password;
	public static Eva_CCA_Login_CreateAccountPage  Login_CreateAccount;
	public static Eva_CCA_OnboardPatientPage OnboardPatient;
	public static Eva_CCA_CaregiverPage OnboardCaregiver;	
	public static Eva_CCA_OnboardPatient_ConversationPage  OnboardPatientconversation;
	public static Eva_CCA_EventsPage EventsPage;
	public static Eva_CCA_OnboardPatient_EventsPage OnboardPatient_Events;
	public static Eva_CCA_OnboardPatient_AddNewPage OnboardPatient_AddNew;
	
	
	public InitializePages(WebDriver driver) 
	{	
		oLogin_Page = new Login_Page(driver);
		homepage=new HomePage(driver);
		OnboardPatient_Registered=new Eva_CCA_OnboardPatient_RegisteredDevicesPage(driver);
		OnboardPatient_Personal_Interests=new Eva_CCA_OnboardPatient_PersonalInterestsPage(driver);
		OnboardPatient_PersonalizedMusicAndMovies=new Eva_CCA_OnboardPatient_PersonalizedMusicAndMoviesPage(driver);
		Login_Loginpage=new Eva_CCA_Login_Loginpage(driver);
		Login_Homepage=new Eva_CCA_Login_Homepage(driver);
		Login_ForgotPassword=new Eva_CCA_Login_ForgotPasswordPage(driver);
		Login_New_Password=new Eva_CCA_Login_NewPasswordPage(driver);
		Login_CreateAccount =new Eva_CCA_Login_CreateAccountPage(driver);
		OnboardCaregiver = new Eva_CCA_CaregiverPage(driver);
		OnboardPatient = new Eva_CCA_OnboardPatientPage(driver);
		OnboardPatientconversation = new Eva_CCA_OnboardPatient_ConversationPage(driver);
		EventsPage = new Eva_CCA_EventsPage(driver);
		OnboardPatient_Events =  new Eva_CCA_OnboardPatient_EventsPage(driver);
		OnboardPatient_AddNew = new Eva_CCA_OnboardPatient_AddNewPage(driver);
		
	}
}
