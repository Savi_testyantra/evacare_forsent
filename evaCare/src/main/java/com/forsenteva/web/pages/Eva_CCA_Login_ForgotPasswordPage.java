package com.forsenteva.web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_Login_ForgotPasswordPage extends BasePage{

	public Eva_CCA_Login_ForgotPasswordPage(WebDriver driver) {
		super(driver);
		
	}
	/*
	 * 54 testcase webelements
	 */
@FindBy(xpath="//span[contains(text(),'Forgot Password')]")
private WebElement forgotpassword;
@FindBy(xpath="//span[@class='portlet-title-text']")
private WebElement forgotpasswordpage;

/*
 * 55th testcase webElements
 */
@FindBy(xpath="//div[@class='control-group']/label[contains(text(),'Email Address ')]")
private WebElement forgotemail;
@FindBy(xpath="//div[@class='taglib-captcha']")
private WebElement capcha;
@FindBy(xpath="//div[@class='control-group']/label[contains(text(),'Text Verification ')]")
private WebElement textverification;
@FindBy(xpath="//div[@class='button-holder ']/button[@class='btn btn-primary']")
private WebElement forgotsubmit;
@FindBy(xpath="//ul[@class='taglib-icon-list unstyled']//span[contains(text(),'Sign In')]")
private WebElement forgotsignin;
@FindBy(xpath="//ul[@class='taglib-icon-list unstyled']//span[contains(text(),'Create Account')]")
private WebElement forgotcreateacc;

/*
 * verifying application is navigating to forgot password page or not
 */
public void Login_054() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(forgotpassword, "clicking on forgot password");
	WebActionUtil.sleep(2);
	WebActionUtil.isElementDisplayedOrNot(forgotpasswordpage);
	WebActionUtil.logger.info("application is navigated to forgot password page");
}
public void Login_055() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(forgotpassword, "clicking on forgot password");
	WebActionUtil.sleep(2);
	boolean forgotemail1=WebActionUtil.isElementDisplayedOrNot(forgotemail);
	
	boolean capcha1=WebActionUtil.isElementDisplayedOrNot(capcha);
	
	boolean textverification1=WebActionUtil.isElementDisplayedOrNot(textverification);
	
	
	boolean forgotsubmit1=WebActionUtil.isElementDisplayedOrNot(forgotsubmit);
	
	boolean forgotsignin1=WebActionUtil.isElementDisplayedOrNot(forgotsignin);
	
	boolean forgotcreateacc1=WebActionUtil.isElementDisplayedOrNot(forgotcreateacc);
	
	if(forgotemail1==true && capcha1==true && textverification1==true && forgotsubmit1==true
			&&forgotsignin1==true && forgotcreateacc1==true) {
		WebActionUtil.logger.info("forgotpage contains email addres,text verification,submit"
				+ "signin and sign in account fields");
	}else {
		WebActionUtil.logger.info("forgotpage not contains email addres,text verification,submit"
				+ "signin and sign in account fields");
	}
	
}


}
