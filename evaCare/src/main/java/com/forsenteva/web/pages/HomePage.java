package com.forsenteva.web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class HomePage extends BasePage{
	
	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	@FindBy(xpath = "//span[contains(text(),'Add New')]")
	private WebElement addNew;
	@FindBy(xpath = "//input[@name='FirstName']")
	private WebElement fName;
	@FindBy(xpath = "//input[@name='LastName']")
	private WebElement lName;
	@FindBy(xpath = "//input[@id='Email']")
	private WebElement eMail;
	@FindBy(xpath = "//input[@id='PhoneNumber']")
	private WebElement phoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactNo']")
	private WebElement ePhoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactName']")
	private WebElement eContactName;
	
	
	@FindBy(xpath = "//input[@id='DateOfBirth']")
	private WebElement dobsending;
	
	@FindBy(xpath="//div[@id='Gender']/div[1]/span/i")
	private WebElement gender1;
	@FindBy(xpath = "//div[@id='ui-select-choices-row-1-1']/span/formio-select-item/span")
	private WebElement gender;
	
	
	@FindBy(xpath = "//button[@class='btn btn-default']")
	private WebElement clalender;
	
	@FindBy(xpath="//strong[@class='ng-binding']")
	private WebElement monthclick;
	
	@FindBy(xpath="//strong[@class='ng-binding']")
	private WebElement yearclick;
	
	@FindBy(xpath="//span[@class='ng-binding'][0]")
	private WebElement moving;
	
	@FindBy(xpath="//span[@class='ng-binding'][18]")
	private WebElement moving1;
	
	
	@FindBy(xpath="//span[contains(text(),'next')]")
	private WebElement next;
	
	@FindBy(xpath="//input[@name='CareGiver-115']")
	private WebElement caregiver;
	
	@FindBy(xpath="//button[@id='PatientProfile.Submit']")
	private WebElement submit;
		
		public void addNewPatient(String firstname,String lastname,String email,String pnonenum,String ephonenum,String econtact) throws Throwable {
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElementUsingJS(addNew, "add");
			WebActionUtil.sleep(3);
			WebActionUtil.typeText(fName, firstname, "Firstname");
			WebActionUtil.typeText(lName, lastname, "Lastname");
			WebActionUtil.typeText(eMail, email, "Email");
			//Select select=new Select(driver.findElement(By.xpath("//i[@class='caret pull-right']")));
			//select.deselectByVisibleText("Male");
			
			
			//String date=Excell.getCellData(GenericLib.testdata, "Addpatient", 6, 1);
			/*System.out.println("Date "+date);
			String datarry[]= date.split("\\.");
			
			String day=datarry[0];
			String month=datarry[1];
			String year=datarry[2];
								
			WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
			WebActionUtil.clickOnElementUsingJS(monthclick, "clicking on month");
			WebActionUtil.clickOnElementUsingJS(yearclick, "clicking on year");
			
			String startyear=moving.getText();
			String endyear=moving1.getText();
			
			
				int startyear1 = Integer.parseInt(startyear);
				int endyear1 = Integer.parseInt(endyear);
				int year1 = Integer.parseInt(year);
				int index=endyear1-startyear1;
			
			
			if(startyear1>=year1&&year1<=endyear1)
			{
				String clickin_element="//span[@class='ng-binding']["+index+"]";
				driver.findElement(By.xpath(clickin_element)).click();
			}*/
			String date="August 2019";
			String expdate="25";
			WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
			while(true)
			{	Thread.sleep(100);
				 String monthdata= monthclick.getText();
				 
				if(monthdata.equals(date))
				{
					break;
				}else
				{
					WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
				}
			}
			driver.findElement(By.xpath("(//span[@class='ng-binding'])[25]")).click();
			WebActionUtil.clickOnElementUsingJS(gender1, "gender");
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElementUsingJS(gender, "gender");
			//WebActionUtil.typeText(dobsending, dob, "Phonenumber");
			
			WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
			WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
			WebActionUtil.typeText(eContactName, econtact, "Econtact");
			WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
			WebActionUtil.sleep(5);
//			WebActionUtil.clickOnElement(submit, "submit");
			WebActionUtil.scrollByPixel(20, 0);
			WebActionUtil.clickOnElementUsingJS(submit, "submit");
			WebActionUtil.sleep(500);
			
		}		
			
			
		}
		


