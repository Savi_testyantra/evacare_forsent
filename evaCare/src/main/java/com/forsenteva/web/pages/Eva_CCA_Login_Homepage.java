package com.forsenteva.web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_Login_Homepage extends BasePage {

	public Eva_CCA_Login_Homepage(WebDriver driver) {
		super(driver);
	}
	/*
	 * 44 testcases webelemens
	 */
	@FindBy(xpath = "//a[@class='logo custom-logo']")
	private  WebElement logodisplay;
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	@FindBy(xpath = "//span[contains(text(),' Onboard Caregiver')]")
	private WebElement onboardcaregiver;
	@FindBy(xpath = "//span[contains(text(),' Events ')]")
	private WebElement events;
	@FindBy(xpath = "//a[@id='eva-theme-help-icon']")
	private WebElement help;
	@FindBy(xpath = "//div[@class='avatar pull-right']")
	private WebElement userprofile;
	/*
	 * 45 testcase webelements
	 */
	@FindBy(xpath = "//a[contains(text(),'Onboard Patient')]")
	private WebElement onboard_patient_pagetitle;
	@FindBy(xpath = "//a[contains(text(),'Onboard Caregiver')]")
	private WebElement onboardcare_giver_pagetitle;
	@FindBy(xpath = "//a[contains(text(),'Events')]")
	private WebElement events_pagetitle;
	
	/*
	 * 48 testcase webelements
	 */
	@FindBy(xpath = "//div[@id='user-block']/ul//li[@class='signout']")
	private WebElement signoutbtn;
	
	/*
	 * 46 testcase webelements
	 */
	@FindBy(xpath = "//a[@class='btn btn-navbar']/i[@class='icon-navbar-controller']")
	private WebElement colapsefirst;
	@FindBy(xpath = "//a[@class='btn btn-navbar close']/i[@class='icon-navbar-controller']")
	private WebElement colapsesecond;
	
	
	/*
	 * verifying Eva logo is displaying or not DashBoard Page
	 */
	public void Login_044() throws Throwable
	{
		int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
		int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

		String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
		String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
		
		Eva_CCA_Login_Loginpage.LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
		WebActionUtil.sleep(1);
		boolean imagedisplay=WebActionUtil.isElementDisplayedOrNot(logodisplay);
		Assert.assertEquals(imagedisplay, true);
		WebActionUtil.logger.info("Eva image is displayed in the application Dashboard");
		boolean onboardpt=WebActionUtil.isElementDisplayedOrNot(onboardPatient);
		
		boolean onboardcaregiver1=WebActionUtil.isElementDisplayedOrNot(onboardcaregiver);
		
		boolean events1=WebActionUtil.isElementDisplayedOrNot(events);
		
		
		boolean help1=WebActionUtil.isElementDisplayedOrNot(help);
		
		boolean userprofile1=WebActionUtil.isElementDisplayedOrNot(userprofile);
		
		
		if(onboardpt==true &&onboardcaregiver1==true&& events1==true && help1==true && userprofile1==true) {
			WebActionUtil.logger.info("All accessible modules is displayed for the user");
		}
		else
		{
			WebActionUtil.logger.info("All accessible modules is not displayed for the user");
		}
		
	}
/*
 * 	verifying the onboardpatient,onboardcaregiver,events are accessing or not
 */
public void Login_045() throws Throwable
{
	int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

	String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	
	Eva_CCA_Login_Loginpage.LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
	WebActionUtil.sleep(1);
	
	WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onboard pt");
	WebActionUtil.sleep(2);
	String onboardPatienttitle1=WebActionUtil.getText(onboard_patient_pagetitle, "onboard page title");
	
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElementUsingJS(onboardcaregiver, "clicking on  caregivwer");
	WebActionUtil.sleep(1);
	String onboardcaregivertitle1=WebActionUtil.getText(onboardcare_giver_pagetitle, "onboard caregiver title");
	
	WebActionUtil.clickOnElementUsingJS(events, "clicking on events page");
	String eventstitle1=WebActionUtil.getText(events_pagetitle, "events title");
	
	
	if(onboardPatienttitle1.contains("Onboard Patient")&&onboardcaregivertitle1.contains("Onboard Caregiver")
			&&eventstitle1.contains("Events")) {
		WebActionUtil.logger.info("Care Center Admin login should be able to access the following modules\r\n" + 
				"Onboard Patient\r\n" + 
				"Onboard Caregiver\r\n" + 
				"Events");
	}
}

/*
 * verifying sign out button is displaying or not
 */
public void Login_048() throws Throwable
{
	int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

	String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	
	Eva_CCA_Login_Loginpage.LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(userprofile, "clicking on user profile");
	WebActionUtil.sleep(1);
	boolean signoutdisplay=WebActionUtil.isElementDisplayedOrNot(signoutbtn);
	Assert.assertEquals(signoutdisplay, true);
	WebActionUtil.logger.info("Signout btn is displaying");
}
/*
 * verifying sign out button is working or not
 */
public void Login_049() throws Throwable
{
	int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

	String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	
	Eva_CCA_Login_Loginpage.LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(userprofile, "clicking on user profile");
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(signoutbtn, "clicking on singoutbtn");
	WebActionUtil.sleep(2);
	WebActionUtil.verifyTheTitle("Dashboard - Eva");
	WebActionUtil.logger.info("user can signout the application successfully and navigated to the signinpage");
}
/*
 * verifying page is collapsed or not
 */
public void Login_046() throws Throwable
{
	int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

	String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	
	Eva_CCA_Login_Loginpage.LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
	WebActionUtil.sleep(2);
	
	WebActionUtil.clickOnElementUsingJS(colapsefirst, "clicking on colapse button");
	WebActionUtil.sleep(1);
	try {
		boolean colapseonbrd=WebActionUtil.isElementDisplayedOrNot(onboardPatient);
		WebActionUtil.logger.info("page is not collapsed");
	}catch(AssertionError e)
	{
		WebActionUtil.logger.info("page is  collapsed");
	}
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(colapsesecond, "clicking on colapse button");
	WebActionUtil.sleep(2);
}
/*
 * verify page is expanded or not after doing collapse the page
 */
public void Login_047() throws Throwable {
	int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

	String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	
	Eva_CCA_Login_Loginpage.LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
	WebActionUtil.sleep(2);
	
	WebActionUtil.clickOnElementUsingJS(colapsefirst, "clicking on colapse button");
	WebActionUtil.sleep(1);
	try {
		WebActionUtil.isElementDisplayedOrNot(onboardPatient);
		WebActionUtil.logger.info("page is not collapsed");
	}catch(AssertionError e)
	{
		WebActionUtil.logger.info("page is  collapsed");
	}
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(colapsesecond, "clicking on colapse button");
	WebActionUtil.sleep(1);
	try {
		WebActionUtil.isElementDisplayedOrNot(onboardPatient);
		WebActionUtil.logger.info("page is expanded");
	}catch(AssertionError e) {
		WebActionUtil.logger.info("page is not expanded");
	}
}

}
