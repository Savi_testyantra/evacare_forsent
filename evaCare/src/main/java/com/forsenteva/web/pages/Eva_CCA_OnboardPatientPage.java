package com.forsenteva.web.pages;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.assertthat.selenium_shutterbug.core.Snapshot;
import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatientPage extends BasePage {

	public Eva_CCA_OnboardPatientPage(WebDriver driver) {
		super(driver);
	}

	// path for Onboard patient tab
	@FindBy(xpath = "//span[text()=' Onboard Patient ']")
	private WebElement onboardPatientTab;
	// path for add conversation "+"
	@FindBy(xpath = "//button[@id='PatientsList.Conversation-0-9']")
	private WebElement addConversation;

	// Path for add new button in onboard patient page
	@FindBy(xpath = "//span[text()='Add New']")
	private WebElement addNewButton;
	// Path for pagination
	@FindBy(xpath = "//div[@class='page-content ng-scope']/descendant::div[@id='form-group-PatientsList']/descendant::li/span[text()=' Showing 1 to  5 of 5 rows']")
	private WebElement pagination;
	// Path for patient edit button
	@FindBy(xpath = "//div[@id='form-group-PatientsList.Profile-4-5']/descendant::button[@id='PatientsList.Profile-4-5']")
	private WebElement patientEdit;
	// Path for patient one edit button
	@FindBy(xpath = "//div[@id='form-group-PatientsList.Profile-0-5']/descendant::button[@id='PatientsList.Profile-0-5']")
	private WebElement patientOneEdit;
	// Path for patient two edit button
	@FindBy(xpath = "//div[@id='form-group-PatientsList.Profile-1-5']/descendant::button[@id='PatientsList.Profile-1-5']")
	private WebElement patientTwoEdit;
	// Path for caregiver check boxes
	@FindBy(xpath = "//input[@type='checkbox']")
	private WebElement allCheckboxes;
	@FindBy(xpath = "//input[@type='checkbox']")
	private WebElement allCheckboxes1;
	// Path for Active radio button
	@FindBy(xpath = "//div[@class='ng-scope radio-inline']/descendant::input[@id='Status-0']")
	private WebElement activeRadioBtn;
	// Path for Inactive radio button
	@FindBy(xpath = "//div[@class='ng-scope radio-inline']/descendant::input[@id='Status-5']")
	private WebElement inactiveRadioBtn;
	/*
	 * 
	 * OnBoard Patient page elements
	 */
	@FindBy(xpath = "//div[@id='mCSB_1_container']//span[@class='nav-title'][text()=' Events ']")
	private static WebElement eventsButton;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.Profile-0-5']//button[@id='PatientsList.Profile-0-5']")
	private static WebElement addProfileButton;

//	@FindBy(xpath = "//nav[@id='navigation']//span[text()=' Onboard Caregiver ']")
//	private static WebElement caregiver;

	@FindBy(xpath = "//input[@id='PatientsList.FirstName-0-2']")
	private WebElement firstPatientName;

	@FindBy(xpath = "//button[@id='PatientsList.Profile-0-5']")
	private WebElement profileButton;

	@FindBy(xpath = "//button[@id='PatientsList.PersonalInterests-0-6']")
	private WebElement personalInterestAddButton;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.PersonalizedMusicandMovies-0-7']//button[@id='PatientsList.PersonalizedMusicandMovies-0-7']")
	private static WebElement addMusicMoviesButton;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.Events-0-10']//button[@id='PatientsList.Events-0-10']")
	private static WebElement patientEventPlusButton;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.PersonalizedMediaLibrary-0-8']//button[@id='PatientsList.PersonalizedMediaLibrary-0-8']")
	private WebElement personalizedLibraryAddButton;

	@FindBy(xpath = "//button[@id='PatientsList.Conversation-0-9']")
	private static WebElement conversationAddButton;

	@FindBy(xpath = "//button[@id='PatientsList.Events-0-10']")
	private WebElement eventsAdButton;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.RegisteredDevices-0-11']//button[@id='PatientsList.RegisteredDevices-0-11']")
	private static WebElement registeredDeviceButton;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.RegisteredDevices-1-11']//button[@id='PatientsList.RegisteredDevices-1-11']")
	private static WebElement secondRegisteredDeviceButton;

	@FindBy(xpath = "//input[@id='Filter']")
	private WebElement filterTextbox;

	@FindBy(xpath = "//button[@id='Search']")
	private WebElement searchButton;

	@FindBy(xpath = "//button[@id='PatientProfile.Refresh']")
	private WebElement refreshButton;

	@FindBy(xpath = "//button[@id='CreatePatient.AddNew']")
	private static WebElement onBoardAddNewButton;

	/*
	 * Profile page elements
	 * 
	 * 
	 */
	@FindBy(xpath = "//input[@id='FirstName']")
	private WebElement firstNameTextBox;

	@FindBy(xpath = "//input[@id='LastName']")
	private WebElement lastNameTextBox;

	@FindBy(xpath = "//input[@id='Email']")
	private WebElement emailTextBox;

	@FindBy(xpath = "//div[@id='Gender']/descendant::div[contains(@class,'select-match')]/descendant::span[contains(@class,'btn-default')]")
	private WebElement genderDropdown;

	@FindBy(xpath = "//div[@id='form-group-PhoneNumber']//input[@id='PhoneNumber']")
	private WebElement phoneNumberTextbox;

	@FindBy(xpath = "//input[@id='EmergencyContactNo']")
	private WebElement emergencyContactTextbox;

	@FindBy(xpath = "//input[@id='Status-0']")
	private WebElement activeRadioButton;

	@FindBy(xpath = "//input[@id='Status-5']")
	private WebElement inactiveRadioButton;

	@FindBy(xpath = "//input[@id='CareGiver-198']")
	private WebElement careGiverCheckbox;

	@FindBy(xpath = "//button[@id='PatientProfile.Submit']")
	private WebElement profileSubmitButton;

	@FindBy(xpath = "//button[@id='PatientProfile.cancel']")
	private WebElement profileCancelButton;

	/*
	 * Personal interests page elements
	 * 
	 * 
	 */
	@FindBy(xpath = "//input[@id='PresentPast-Present']")
	private WebElement presentRadioButton;

	@FindBy(xpath = "//input[@id='PresentPast-Past']")
	private WebElement pastRadioButton;

	@FindBy(xpath = "//input[@id='AddActivities']")
	private WebElement activitiesTextbox;

	@FindBy(xpath = "//button[@id='AddActivitiesEvent']")
	private WebElement activitiesaddButton;

	@FindBy(xpath = "//input[@id='AddHobbies']")
	private WebElement hobbiesAddTextbox;

	@FindBy(xpath = "//button[@id='AddHobbiesEvent']")
	private WebElement hobbiesAddButton;

	@FindBy(xpath = "//input[@id='AddInterests']")
	private WebElement interestsTextbox;

	@FindBy(xpath = "//input[@id='AddInterests']")
	private WebElement interestsAddButton;

	/*
	 * Personalized Media page elements
	 * 
	 * 
	 */
	@FindBy(xpath = "//button[contains(@id,'Delete')]")
	private List<WebElement> deleteButtons;

	@FindBy(xpath = "//input[@id='MediaTags']")
	private WebElement tagsTextbox;

	@FindBy(xpath = "//div[@id='form-group-MediaMusic']//input[@id='MediaMusic']")
	private WebElement musicTextbox;

	@FindBy(xpath = "//div[@id='form-group-MediaMovie']//input[@id='MediaMovie']")
	private WebElement movieTextbox;

	@FindBy(xpath = "//div[@id='form-group-MediaArtist']//input[@id='MediaArtist']")
	private WebElement artistTextbox;

	@FindBy(xpath = "//div[@id='form-group-PersonalizedMediaMusic.add']//button[@id='PersonalizedMediaMusic.add']")
	private WebElement musicAddButton;

	@FindBy(xpath = "//div[@id='form-group-PersonalizedMedia.columns2']//button[@id='PersonalizedMediaTags.Add']")
	private WebElement tagsAddButton;

	@FindBy(xpath = "//div[@id='form-group-PersonalizedMediaMovies.add']//button[@id='PersonalizedMediaMovies.add']")
	private WebElement moviesAddButton;

	@FindBy(xpath = "//div[@id='form-group-PersonalizedMediaArtist.add']//button[@id='PersonalizedMediaArtist.add']")
	private WebElement artistAddButton;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaText-')]")
	private List<WebElement> tagsTextButton;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaText2')]")
	private List<WebElement> musicTextButton;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaText3')]")
	private List<WebElement> movieTextButton;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaText4')]")
	private List<WebElement> artistTextButton;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaDelete-')]//button[starts-with(@id,'PersonalizedMediaDelete-')]")
	private List<WebElement> tagsRemoveButtons;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaDelete2')]//button[starts-with(@id,'PersonalizedMediaDelete2')]")
	private List<WebElement> musicRemoveButtons;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaDelete3')]//button[starts-with(@id,'PersonalizedMediaDelete3')]")
	private List<WebElement> movieRemoveButtons;

	@FindBy(xpath = "//div[contains(@id,'form-group-PersonalizedMediaDelete4')]//button[starts-with(@id,'PersonalizedMediaDelete4')]")
	private List<WebElement> artistRemoveButtons;

	@FindBy(xpath = "//button[text()='Ok']")
	private WebElement confirmOkButton;

	/*
	 * 
	 * personalized media library page elements
	 */
	@FindBy(xpath = "//input[@id='PersonalizedSearch']")
	private WebElement personalizedSearchTextbox;

	@FindBy(xpath = "//button[@id='searchButton2']")
	private WebElement personalizedSearchButton;

	@FindBy(xpath = "//table[contains(@class,'datagrid-table')]")
	private WebElement table;

	@FindBy(xpath = "//span[text()='All']")
	private WebElement allDropdown;

	@FindBy(xpath = "//button[contains(@id,'Refresh2')]")
	private WebElement Refreshbutton;

	@FindBy(xpath = "//button[contains(@id,'Delete-1')]")
	private WebElement deleteButton;

	@FindBy(xpath = "//input[@id='PersonalizedSearch']")
	private WebElement searchTextbox;

	@FindBy(xpath = "//button[@id='searchButton2']")
	private WebElement personalizedLibSearchButton;

	@FindBy(xpath = "//button[contains(@id,'Delete-')]")
	private List<WebElement> personalizedLibDeleteButtons;

	/*
	 * 
	 * conversation page elements
	 */
	@FindBy(xpath = "//button[contains(@id,'AddNew')]")
	private WebElement conversationaAddNewButton;

	@FindBy(xpath = "//input[@id='PatientName']")
	private WebElement patientName;

	@FindBy(xpath = "//input[starts-with(@id,'Question-')]")
	private List<WebElement> gridList;

	@FindBy(xpath = "//input[@id='PatientName']")
	private WebElement selectedPatient;

	@FindBy(xpath = "//button[@id='CommunicationForm.AddNew']")
	private static WebElement conversationAddNewButton;

	/*
	 * 
	 * Add New conversation page elements
	 */

	@FindBy(xpath = "//input[@id='AlertInfo']")
	private WebElement alertCheckbox;

	@FindBy(xpath = "//div[@id='Mood']//span[text()='Please select']")
	private WebElement moodDropdown;

	@FindBy(xpath = "//div[@id='Mood']/descendant::span[text()='Agitated']")
	private List<WebElement> moodDropdownValues;

	@FindBy(xpath = "//div[@id='Subjects']//input")
	private WebElement subjectTextbox;

	@FindBy(xpath = "//input[@id='AnswerType-Answer']")
	private WebElement answerRadioButton;

	@FindBy(xpath = "//input[@id='AnswerType-Media']")
	private WebElement mediaRadiButton;

	@FindBy(xpath = "//textarea[@id='Answer']")
	private WebElement answerMediatextArea;

	@FindBy(xpath = "//label[@for='CommunicationForm.file']")
	private WebElement uploadImagelabel;

	@FindBy(xpath = "//label[@for='CommunicationForm.Record']")
	private WebElement recordlabel;

	@FindBy(xpath = "//input[@id='RepeatedMoreThanTimes']")
	private WebElement repeatedMoreThanTextBox;

	@FindBy(xpath = "//div[@id='Within']/descendant::div[contains(@class,'ui-select-match')]")
	private WebElement withinDropdown;

	@FindBy(xpath = "//div[@id='Trigger']/descendant::div[contains(@class,'ui-select-match')]/descendant::span[contains(@class,'btn-default')]")
	private WebElement triggerDropdown;

	@FindBy(xpath = "//div[@id='form-group-Submit']/descendant::button[@id='Submit']")
	private WebElement submitButton;

	@FindBy(xpath = "//button[@id='CommunicationForm.Cancel']")
	private WebElement cancelButton;

	@FindBy(xpath = "//input[@id='CommunicationForm.QuestionPhrase']")
	private WebElement questiontextBox;

	@FindBy(xpath = "//span[text()='No Results Found']")
	private WebElement errorMessage;

	@FindBy(xpath = "//input[@id='Filter']")
	private WebElement filterTextBox;

	@FindBy(xpath = "//label[contains(@class,'field-required')][@for='CommunicationForm.QuestionPhrase']")
	private WebElement questionLabel;

	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private static WebElement onboardPatient;

	@FindBy(xpath = "//span[contains(text(),'Add New')]")
	private WebElement addNew;

	@FindBy(xpath = "//input[@name='FirstName']")
	private WebElement first_Name;

	@FindBy(xpath = "//input[@name='LastName']")
	private WebElement last_Name;

	@FindBy(xpath = "//input[@id='PhoneNumber']")
	private WebElement phone_Num;

	@FindBy(xpath = "//input[@id='EmergencyContactNo']")
	private WebElement ePhone_Num;

	@FindBy(xpath = "//input[@id='EmergencyContactName']")
	private WebElement eContact_Name;

	@FindBy(xpath = "//input[@id='DateOfBirth']")
	private WebElement date_Of_Birth;

	@FindBy(xpath = "//div[@id='Gender']/div[1]/span/i")
	private WebElement gender_1;

	@FindBy(xpath = "//button[@class='btn btn-default']")
	private WebElement clalender;

	@FindBy(xpath = "//strong[@class='ng-binding']")
	private WebElement month_Click;

	@FindBy(xpath = "//strong[@class='ng-binding']")
	private WebElement year_Click;

	@FindBy(xpath = "//span[@class='ng-binding'][0]")
	private WebElement moving;

	@FindBy(xpath = "//span[@class='ng-binding'][18]")
	private WebElement moving1;

	@FindBy(xpath = "//p[.='Please Enter Valid Contact Name.']")
	private WebElement verifyContact;

	@FindBy(xpath = "//div[@class='row ng-scope']/descendant::div[@id='form-group-selectcaregiver']/..")
	private WebElement mendotryField;

	@FindBy(xpath = "//div[@id='form-group-CareGiver']/descendant::input[@type='checkbox'][1]")
	private WebElement careGiver;

	@FindBy(xpath = "//div[@id='form-group-CareGiver']/descendant::input[@type='checkbox'][1]")
	private WebElement careGiver_1;

	@FindBy(xpath = "//div[@id='form-group-CareGiver']/descendant::input[@type='checkbox'][2]")
	private WebElement careGiver_2;

	@FindBy(xpath = "//div[@id='form-group-CareGiver']/descendant::input[@type='checkbox'][3]")
	private WebElement careGiver_3;

	@FindBy(xpath = "//div[@class='ng-scope radio-inline']/descendant::input[@id='Status-0']")
	private WebElement radioButton;

	@FindBy(xpath = "//div[@class='ng-scope radio-inline']/descendant::input[@id='Status-5']")
	private WebElement clickRadioButton;

	@FindBy(xpath = "//button[@id='PatientProfile.cancel']")
	private WebElement cancel;

	@FindBy(xpath = "//input[@id='PatientsList.FirstName-0-2']")
	private WebElement verifyText;

	@FindBy(xpath = "//button[@id='PatientsList.Events-0-10']")
	private WebElement click_Event;

	@FindBy(xpath = "//button[@id='PatientsList.Conversation-0-9']")
	private WebElement click_Conversation;

	@FindBy(xpath = "//button[@class='btn btn-sm btn-primary pull-right theme-color']/descendant::span[.='Add Event']")
	private WebElement click_AddEvent;

	@FindBy(xpath = "//div[@class='addEventDialogcontrols']/descendant::input[@type='text']")
	private WebElement enter_Event;

	@FindBy(xpath = "//div[@class='addEventDialogcontrols']/descendant::textarea[@class='form-control ng-pristine ng-untouched ng-valid ng-empty']")
	private WebElement enter_Description;

	@FindBy(xpath = "//button[.='Save']")
	private WebElement click_Savebutton;

	@FindBy(xpath = "//div[@class='col-md-5 text-center']/descendant::button[.='Year']")
	private WebElement click_Year;

	@FindBy(xpath = "//button[@class='btn btn-sm btn-primary pull-right theme-color']/descendant::span[.='Schedule Media']")
	private WebElement schedule_Media;

	@FindBy(xpath = "//div[@class='col-md-3']/descendant::h5[.='2019']")
	private WebElement verify_Year;

	@FindBy(xpath = "//div[@class='col-md-5 text-center']/descendant::button[.='Month']")
	private WebElement click_Month;

	@FindBy(xpath = "//div[@class='col-md-3']/descendant::h5[.='September 2019']")
	private WebElement click_VerifyMonth;

	@FindBy(xpath = "//div[@class='col-md-5 text-center']/descendant::button[.='Week']")
	private WebElement click_Week;

	@FindBy(xpath = "//div[@class='col-md-5 text-center']/descendant::button[.='Day']")
	private WebElement click_Day;

	@FindBy(xpath = "//li[@class='breadcrumb-truncate']/descendant::a[.='Add Events']")
	private WebElement verify_AddEventPage;

	@FindBy(xpath = "//button[contains(text(),'Next')]")
	private WebElement click_NextButton;

	@FindBy(xpath = "//button[contains(text(),'Previous')]")
	private WebElement click_PreviousButton;

	@FindBy(xpath = "//div[@class='col-md-3']/descendant::h5[@class='text-center ng-binding']")
	private WebElement click_DayText;

	@FindBy(xpath = "//div[@class='cal-cell1 ng-scope cal-day-weekend cal-day-past']/descendant::span[.='Sunday']")
	private WebElement click_WeekDay;

	@FindBy(xpath = "//button[@id='CommunicationForm.AddNew']/descendant::span[.='Add New']")
	private WebElement conversation_AddNewButton;

	@FindBy(xpath = "//input[@id='CommunicationForm.QuestionPhrase']")
	private WebElement conversation_QuestionPhrase;

	@FindBy(xpath = "//i[@class='caret pull-right']")
	private WebElement conversation_PleaseSelect;

	@FindBy(xpath = "//span[.='Agitated']")
	private WebElement conversation_PleaseSelect_Mood;

	@FindBy(xpath = "//div[@id='Subjects']/descendant::input[@type='search']")
	private WebElement conversation_Subject;

	@FindBy(xpath = "//div[@id='form-group-CommunicationForm.Event2-0-29']/descendant::span[@class='fa fa-trash-o']")
	private WebElement conversation_DeleteButton;

	@FindBy(xpath = "//button[@id='Submit']")
	private WebElement conversation_SubmitButton;

	@FindBy(xpath = "//button[@class='btn btn-danger pull-right ng-binding ng-scope']")
	private WebElement conversation_DeleteCancelButton;

	@FindBy(xpath = "//input[@id='Question-0-4']")
	private WebElement verify_CancelButton;

	@FindBy(xpath = "//div[@class='input-group leftLayoutControl']/descendant::input[@id='Filter']")
	private WebElement click_SerchField;

	@FindBy(xpath = "//span[@class='fa fa-search']")
	private WebElement click_SerchFieldButton;

	@FindBy(xpath = "//div[@title='aabio']")
	private WebElement verify_SerchFieldGrid;

	@FindBy(xpath = "//button[@id='PatientsList.Profile-0-5']")
	private WebElement click_ProfileColoum;

	@FindBy(xpath = "//input[@id='Status-5']")
	private WebElement click_InactiveStatus;

	@FindBy(xpath = "//a[contains(text(),'Onboard Patient')]") // getting text from on board patient
	private WebElement gettext;

	/*
	 * 2 testcase webelements
	 */
	@FindBy(xpath = "//input[@name='Filter']")
	private WebElement serachfield;
	@FindBy(xpath = "//button[@id='Search']")
	private WebElement searchbtn;
	@FindBy(xpath = "//div[@id='form-group-PatientsList']/descendant::table/descendant::th[@class='ng-binding ng-scope']")
	private List<WebElement> list;

	/*
	 * 3 testcase webelements
	 */
	@FindBy(xpath = "//input[@name='FirstName']")
	private WebElement fName;

	@FindBy(xpath = "//input[@name='LastName']")
	private WebElement lName;

	/*
	 * 4 testcase webelements
	 */
	@FindBy(xpath = "//label[contains(text(),'First Name : ')]")
	WebElement firstname;
	@FindBy(xpath = "//label[contains(text(),'Last Name : ')]")
	WebElement lastname;
	@FindBy(xpath = "//label[contains(text(),'Email : ')]")
	WebElement email;
	@FindBy(xpath = "//label[contains(text(),'Gender :')]")
	WebElement gender;
	@FindBy(xpath = "//label[contains(text(),'Date of Birth : ')]")
	WebElement dob;

	@FindBy(xpath = "//label[contains(text(),'Phone Number : ')]")
	WebElement phonenum;
	@FindBy(xpath = "//label[contains(text(),'Emergency Phone No. : ')]")
	WebElement ephonenum;
	@FindBy(xpath = "//label[contains(text(),'Emergency Contact : ')]")
	WebElement econtact;
	@FindBy(xpath = "//label[contains(text(),'Status : ')]")
	WebElement status;

	@FindBy(xpath = "//span[@id='selectcaregiver']")
	WebElement selectcaregiver;

	@FindBy(xpath = "//button[@id='PatientProfile.Submit']")
	WebElement selectbtn;
	@FindBy(xpath = "//span[contains(text(),'Cancel')]")
	WebElement cancelbtn;

	/*
	 * 5 testcase webelements
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::input[@id='Email']")
	private WebElement eMail;
	@FindBy(xpath = "//div[@id='Gender']/div/span/i")
	private WebElement gender1;
	@FindBy(xpath = "//button[@class='btn btn-default']")
	private WebElement calender;

	@FindBy(xpath = "//strong[@class='ng-binding']")
	private WebElement monthclick;
	@FindBy(xpath = "//input[@id='PhoneNumber']")
	private WebElement phoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactNo']")
	private WebElement ePhoneNum;
	@FindBy(xpath = "//input[@id='EmergencyContactName']")
	private WebElement eContactName;
	@FindBy(xpath = "//span[contains(text(),'next')]")
	private WebElement next;

	@FindBy(xpath = "//div[@id='form-group-CareGiver']/descendant::input[@type='checkbox'][1]")
	private static WebElement caregiver;

	@FindBy(xpath = "//button[@id='PatientProfile.Submit']")
	private WebElement submit;

	/*
	 * 6 testcase webelements
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::div[@id='form-group-FirstName']/descendant::label[@ng-class=\"{'field-required': isRequired(component)}\"]")
	private WebElement fnameasstric;

	/*
	 * 9th testcase webelement
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::div[@id='form-group-LastName']/descendant::label[@ng-class=\"{'field-required': isRequired(component)}\"]")
	private WebElement lastnamestrick;

	/*
	 * 12th testcase webelement
	 */
	@FindBy(xpath = "//div[@id='form-group-PatientProfile']/descendant::div[@id='form-group-Email']/descendant::label[@ng-class=\"{'field-required': isRequired(component)}\"]")
	private WebElement emailastrick;

	/*
	 * 13 th test case web elements
	 */
	@FindBy(xpath = "//p[contains(text(),'Please Enter Valid Email Address')]")
	private WebElement eMailtext;

	/*
	 * 14 th test case webelements
	 */
	@FindBy(xpath = "//p[contains(text(),'Please Enter Valid Email Address')]")
	private WebElement emailvaldationmsg;

	/*
	 * 15 th test case webelements
	 */
	@FindBy(xpath = "//div[@id='ui-select-choices-row-1-1']/span/formio-select-item/span")
	private WebElement gendermale;
	@FindBy(xpath = "//div[@id='ui-select-choices-row-1-0']/span/formio-select-item/span")
	private WebElement genderfemale;

	/*
	 * 17 th test case web elements
	 */
	@FindBy(xpath = "//span[contains(text(),'Female')]")
	private WebElement femalevalue;

	/*
	 * 18 th testcase web elements
	 */
	@FindBy(xpath = "//span[contains(text(),'Male')]")
	private WebElement malevalue;

	/*
	 * 19 th test case WebElements
	 */
	@FindBy(xpath = "//i[@class='glyphicon glyphicon-calendar ng-scope']")
	private WebElement dateicon;
	@FindBy(xpath = "//button[@class='btn btn-default btn-sm uib-title']")
	private WebElement opencalender;

	/*
	 * 24 th test case WebElements
	 */
	@FindBy(xpath = "//input[@id='DateOfBirth']")
	private WebElement datefield;

	/*
	 * 22th test case webelements
	 */
	@FindBy(xpath = "//button[@class='btn btn-default btn-sm pull-left uib-left']/descendant::span[contains(text(),'previous')]")
	private WebElement calenderprevious;
	/*
	 * 23th test case webelements
	 */
	@FindBy(xpath = "//div[@class='uib-daypicker ng-scope']/descendant::tr[@class='uib-weeks ng-scope']/descendant::td[@class='uib-day text-center ng-scope']/descendant::button[@class='btn btn-default btn-sm active']")
	private WebElement calenderdate;

	/*
	 * 52th test case webelements
	 */
	@FindBy(xpath = "//div[@class='input-group leftLayoutControl']/input[@name='Filter']")
	private WebElement serachtheelemt;
	@FindBy(xpath = "//div[@id='form-group-Search']/button[@id='Search']")
	private WebElement serachtheelemtbtn;
	@FindBy(xpath = "//div[@id='form-group-PatientsList.FirstName-0-2']")
	private WebElement getserchelement;
	@FindBy(xpath = "//div[@id='form-group-PatientProfile.Refresh']/button[@name='PatientProfile.Refresh']")
	private WebElement refresh;

	/*
	 * 55 th test cases
	 */
	@FindBy(xpath = "//button[@id='PatientsList.Profile-0-5']")
	private WebElement profileedit;
	@FindBy(xpath = "//div[@id='form-group-CareGiver']/descendant::input[@type='checkbox'][1]")
	private WebElement careviveredit;
	// Path for Events '+' button in onboard patient page
	@FindBy(xpath = "//div[@id='form-group-PatientsList.Events-0-10']/descendant::button[@id='PatientsList.Events-0-10']")
	private WebElement eventButton;
	// Path for patient one status field
	@FindBy(xpath = "//div[@id='form-group-PatientsList.Status-0-4']")
	private WebElement statusField;

	// Method to verify CCA will uncheck caregiver from one patient, it will reflect
	// for other patients also

	public void navigateToAddEvents() {
		try {
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElement(onboardPatientTab, "Clicking on onboard patient tab");
			WebActionUtil.clickOnElement(eventButton, "Clicking on add events icon");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * 21 testcase webelements
	 */
	@FindBy(xpath = "//strong[@class='ng-binding']")
	private WebElement year;

	/*
	 * 
	 * verifying On board page is displaying or not first Testcase
	 */
	public void OnboardPatient_001() throws Throwable {

		WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboardtab");
		String exptitle = "Onboard Patient - Eva";
		WebActionUtil.verifyTheTitle(exptitle);
		String data = WebActionUtil.getText(gettext, "getttext");
		WebActionUtil.sleep(2);
		if (data.equals("Onboard Patient")) {
			WebActionUtil.logger.info("stay in Onboard Patient page");

		} else {
			WebActionUtil.logger.info(" not in Onboard Patient page");

		}
	}

	/*
	 * Verifying On board page fields i.e Addnew button ,search field and patient
	 * grid 2nd testcase method
	 */

	/*
	 * Verifying On board page fields i.e Addnew button ,search field and patient
	 * grid 2nd testcase method
	 */

	public void OnboardPatient_002() throws Throwable {
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboardtab");
		WebActionUtil.sleep(3);
		String str = null;

		/*
		 * fetching all table header data and adding in to the list
		 */

		for (int i = 2; i <= 12; i++) {
			str = list.get(i).getText();

		}

		boolean add = WebActionUtil.isElementDisplayedOrNot(addNew);

		boolean serachfield1 = WebActionUtil.isElementDisplayedOrNot(serachfield);

		boolean searchbtn1 = WebActionUtil.isElementDisplayedOrNot(searchbtn);

		/*
		 * validating the result
		 */

		if (add == true && serachfield1 == true && searchbtn1 == true && str != null) {

			WebActionUtil.logger.info("all fields are displayed");

		} else {
			WebActionUtil.logger.info("all fields are not  displayed");
		}
	}

	/*
	 * Verifying the addnew page navigating third testcase
	 * 
	 */
	public void OnboardPatient_003() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboard element");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "addnew");
		WebActionUtil.sleep(3);
		boolean Fname = WebActionUtil.isElementDisplayedOrNot(fName);
		WebActionUtil.sleep(3);
		boolean lastname = WebActionUtil.isElementDisplayedOrNot(lName);

		if (Fname == true && lastname == true) {
			WebActionUtil.logger.info("Entering in to the Add new page");
		} else {
			WebActionUtil.logger.info("not entering  in to the Add new page");
		}
	}

	/*
	 * Verifying the all the fields are displayed in addnew patient page or not 4th
	 * testcase
	 */
	public void OnboardPatient_004() throws Throwable {

		WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboard");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "addnew");
		WebActionUtil.sleep(3);
		boolean First = WebActionUtil.isElementDisplayedOrNot(firstname);
		WebActionUtil.sleep(3);
		boolean Last = WebActionUtil.isElementDisplayedOrNot(lastname);
		WebActionUtil.sleep(3);
		boolean Email = WebActionUtil.isElementDisplayedOrNot(email);
		WebActionUtil.sleep(3);
		boolean Gender = WebActionUtil.isElementDisplayedOrNot(gender);
		WebActionUtil.sleep(3);
		boolean Dob = WebActionUtil.isElementDisplayedOrNot(dob);
		WebActionUtil.sleep(3);
		boolean Phone = WebActionUtil.isElementDisplayedOrNot(phonenum);
		WebActionUtil.sleep(3);
		boolean Ephone = WebActionUtil.isElementDisplayedOrNot(ephonenum);
		WebActionUtil.sleep(3);
		boolean Econtact = WebActionUtil.isElementDisplayedOrNot(econtact);
		WebActionUtil.sleep(3);
		boolean SelectCare = WebActionUtil.isElementDisplayedOrNot(selectcaregiver);

		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(cancelbtn, "cancelbtn");

		// String data11=WebActionUtil.getText(cancelbtn, "cancelbtn");

		boolean cancel = WebActionUtil.isElementDisplayedOrNot(cancelbtn);
		WebActionUtil.sleep(3);

		boolean submit = WebActionUtil.isElementDisplayedOrNot(cancelbtn);
		WebActionUtil.sleep(3);

		if (First == true && Last == true && Email == true && Gender == true && Dob == true && Phone == true
				&& Ephone == true && Econtact == true && SelectCare == true && cancel == true && submit == true) {
			WebActionUtil.logger.info("Add patient page cotains Firstname,Lastname,Email,Gender,"
					+ "Date of Birth,Phonenumber,Emergency Phone number,Emegency contact number,"
					+ "Status,select caregiver check box,submit and cancel button are displayed.");
		}

		else {
			WebActionUtil.logger.info("Some problem is displayed the elements");
		}

	}

	/*
	 * Verifying first name field accept special characters,alphabets and numeric
	 * fisth testcase
	 */

	public void OnboardPatient_005(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {

		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(3);

		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		String alertdata = driver.switchTo().alert().getText();
		WebActionUtil.sleep(3);
		driver.switchTo().alert().accept();
		if (alertdata.contains("Patient Created Successfully")) {
			WebActionUtil.logger.info("First Name field should accept alphabets, Numbers, Special characters");

		} else {
			WebActionUtil.logger.info("First Name field should not accept alphabets, Numbers, Special characters");
		}
		WebActionUtil.sleep(2);

	}

	/*
	 * Verifying first name field is manditary or not. 6th testcase
	 */
	public void OnboardPatient_006(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		/*
		 * Shutterbug.shootElement(driver,
		 * firstname1).withName("star_beforeClicking").save(); Snapshot image1 =
		 * Shutterbug.shootElement(driver, firstname1).withName("star_beforeClicking");
		 */
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");

		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(fName, "element");
		WebActionUtil.sleep(3);

		/*
		 * Shutterbug.shootElement(driver,
		 * firstname1).withName("star_afterClicking").save(); WebActionUtil.sleep(5);
		 * Snapshot image2 = Shutterbug.shootElement(driver,
		 * firstname1).withName("star_afterClicking"); assertEquals(image2,image1);
		 */

		String firstnamemandary = fnameasstric.getAttribute("ng-class");

		WebActionUtil.sleep(3);
		if (firstnamemandary.contains("{'field-required': isRequired(component)}")) {
			WebActionUtil.logger.info("mandatory symbol (*) is displaying to the user");
		} else {
			WebActionUtil.logger.info("mandatory symbol (*) is not displaying to the user");
		}
	}

	/*
	 * 7th testcase verifying submitton is enabled or not
	 */
	public void OnboardPatient_007(String lastname, String email, String pnonenum, String ephonenum, String econtact)
			throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		// WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);
		boolean subvalue = submit.isEnabled();
		WebActionUtil.sleep(3);
		Assert.assertEquals(subvalue, false, "submit not displayed");
		WebActionUtil.logger.info("submit button is not prasent");
		// WebActionUtil.clickOnElementUsingJS(submit, "submit");
	}

	/*
	 * Verifying last name field accept special characters,alphabets and numeric 8th
	 * testcase
	 */
	public void OnboardPatient_008(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		WebActionUtil.sleep(3);
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		String alertdata = driver.switchTo().alert().getText();
		WebActionUtil.sleep(3);
		driver.switchTo().alert().accept();

		if (alertdata.contains("Patient Created Successfully")) {
			WebActionUtil.logger.info("Last Name field should accept alphabets, Numbers, Special characters");

		} else {
			WebActionUtil.logger.info("Last Name field should not accept alphabets, Numbers, Special characters");
		}
		WebActionUtil.sleep(2);

	}

	/*
	 * Verifying last name field is mandatory or not 9th testcase
	 */
	public void OnboardPatient_009(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		/*
		 * Shutterbug.shootElement(driver, email1).withName("BeforeEmail").save();
		 * WebActionUtil.sleep(3); Snapshot image1 = Shutterbug.shootElement(driver,
		 * email1).withName("BeforEmail");
		 */
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(lName, "lastname");
		WebActionUtil.sleep(3);

		/*
		 * Shutterbug.shootElement(driver, email1).withName("afterEmail").save();
		 * WebActionUtil.sleep(3); Snapshot image2 = Shutterbug.shootElement(driver,
		 * email1).withName("afterEmail"); assertEquals(image1, image2);
		 */

		String lastnamestrick1 = lastnamestrick.getAttribute("ng-class");

		if (lastnamestrick1.contains("{'field-required': isRequired(component)}")) {
			WebActionUtil.logger.info("mandatory symbol of lastname (*) is displaying to the user");
		} else {
			WebActionUtil.logger.info("mandatory symbol of lastname (*) is not displaying to the user");
		}

	}

	/*
	 * Validating Last name is mandatory or not. 10th testcase
	 */
	public void OnboardPatient_010(String firstname, String email, String pnonenum, String ephonenum, String econtact)
			throws Throwable {

		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		String date = "September 2019";
		String expdate = "02";

		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);
		boolean subvalue = submit.isEnabled();
		WebActionUtil.sleep(3);
		Assert.assertEquals(subvalue, false, "submit not displayed");

		WebActionUtil.logger.info("Lastname is mandatory");

	}

	/*
	 * Verifying email is format e.g abc@c.com 11 th testcase
	 */
	public void OnboardPatient_011(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		WebActionUtil.sleep(3);
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		String alertdata = driver.switchTo().alert().getText();
		WebActionUtil.sleep(3);
		driver.switchTo().alert().accept();

		if (alertdata.contains("Patient Created Successfully")) {
			WebActionUtil.logger.info("Email field should accept abc@c.com");

		} else {
			WebActionUtil.logger.info("Email field not accept abc@c.com");
		}
		WebActionUtil.sleep(2);

	}

	/*
	 * Verifying email field is mandatory or not 12th test case webelement
	 */
	public void OnboardPatient_012(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		/*
		 * Shutterbug.shootElement(driver, email1).withName("BeforeEmail").save();
		 * WebActionUtil.sleep(3); Snapshot image1 = Shutterbug.shootElement(driver,
		 * email1).withName("BeforEmail");
		 */
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		WebActionUtil.sleep(3);
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}
		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(eMail, "element");
		WebActionUtil.sleep(3);

		/*
		 * Shutterbug.shootElement(driver, email1).withName("afterEmail").save();
		 * WebActionUtil.sleep(3); Snapshot image2 = Shutterbug.shootElement(driver,
		 * email1).withName("afterEmail"); assertEquals(image1, image2);
		 */

		String emailastrick1 = emailastrick.getAttribute("ng-class");

		if (emailastrick1.contains("{'field-required': isRequired(component)}")) {
			WebActionUtil.logger.info("mandatory symbol of Email (*) is displaying to the user");
		} else {
			WebActionUtil.logger.info("mandatory symbol of Email (*) is not displaying to the user");
		}

	}

	/*
	 * Email validation 13 th test case
	 */
	public void OnboardPatient_013() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);

		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.emaildatapath, "Sheet1", "TEST_CASE_NO");
		int i = 1;
		while (i <= 4) {
			String sdata2 = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.emaildatapath, "Sheet1");
			webActionUtil.sleep(3);
			WebActionUtil.clearAndTypeText(eMail, sdata2, "Email");
			// driver.switchTo().activeElement().sendKeys(Keys.TAB);
			WebActionUtil.sleep(2);
			try {
				boolean data = WebActionUtil.isElementDisplayedOrNot(eMailtext);

				if (data == true) {
					webActionUtil.sleep(5);
					webActionUtil.clearAndTypeText(eMail, sdata2, "Email");
					webActionUtil.sleep(5);
					i++;
				} else {
					WebActionUtil.logger.info("Email suceesfully verified all conditions");
				}
			} catch (AssertionError e) {
				WebActionUtil.logger.info("Email suceesfully verified all conditions");
			}
		}
	}
	/*
	 * Verifying invalid email format e.g abc@) 14 th testcase
	 *
	 */

	public void OnboardPatient_014(String email) throws Throwable {
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		String emailvaidation = WebActionUtil.getText(emailvaldationmsg, "emial validation message will diaplay");

		WebActionUtil.logger.info(emailvaidation);

		if (emailvaidation.contains("Please Enter Valid Email Address")) {
			WebActionUtil.logger.info("Invalid Email id formate is not allowing to the Email formate");

		} else {
			WebActionUtil.logger.info("Invalid Email id formate is  allowing to the Email formate");
		}
		WebActionUtil.sleep(1);

	}
	/*
	 * 
	 * Validating all Gender fields are displaying or not 15 th test case
	 */

	public void OnboardPatient_015() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		boolean malefield = WebActionUtil.isElementDisplayedOrNot(gendermale);
		WebActionUtil.sleep(3);
		boolean femalefield = WebActionUtil.isElementDisplayedOrNot(genderfemale);

		WebActionUtil.sleep(3);
		if (malefield == true && femalefield == true) {
			WebActionUtil.logger.info("All Gender fields are displaying");

		} else {
			WebActionUtil.logger.info("All Gender fields are  not displaying");
		}

	}
	/*
	 * verifying user can able to select the gender fields in drop down 16 th test
	 * case
	 */

	public void OnboardPatient_016() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(gendermale, "click on Male value");
		WebActionUtil.sleep(3);

		WebActionUtil.logger.info("user can able to select the Gender in drop down");
		try {
			WebActionUtil.clearAndTypeText(gender1, "Trans", "user can not select invalid Gender");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("user can't select the invalid gender data");
		}

		WebActionUtil.sleep(1);

	}

	/*
	 * verifying user can able to see either selected data is displayed or not in
	 * the gender field 17 th test case
	 */
	public void OnboardPatient_017() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(genderfemale, "click on feMale value");
		WebActionUtil.sleep(3);

		try {
			WebActionUtil.verifyElementText(femalevalue, "Female");
			WebActionUtil.logger.info("Entered text is  displayed");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Entered text is not displayed");
		}

	}

	/*
	 * checking multiple data in gender field 18 th test case
	 */
	public void OnboardPatient_018() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(genderfemale, "click on feMale value");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "click on gender");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(gendermale, "clicking on male value");
		WebActionUtil.sleep(3);
		try {
			WebActionUtil.verifyElementText(malevalue, "Male");
			WebActionUtil.logger.info("Female value is replaced with male hence user can not enter multiple value");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Gender field is  allowing entering multipledata");
		}
	}

	/*
	 * date filed is displayed or not 19 th testcase
	 */
	public void OnboardPatient_019() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on bordpatient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "click on addnew pt");
		WebActionUtil.sleep(3);
		boolean date = WebActionUtil.isElementDisplayedOrNot(dateicon);

		if (date = true) {
			WebActionUtil.logger.info("Date field is Present");
			WebActionUtil.clickOnElementUsingJS(dateicon, "clicking on date");
			WebActionUtil.sleep(3);
			boolean calender = WebActionUtil.isElementDisplayedOrNot(opencalender);
			if (calender = true) {

				String calender1 = WebActionUtil.getText(opencalender, "callendr is present");
				WebActionUtil.logger.info(calender1 + "calender is opened");
			} else {
				WebActionUtil.logger.info("date is not prasent");
			}
		}
	}

	/*
	 * Eva_CCA_Onboard Patient_039 test cases verifying Emergency contact number
	 * mandatory or not.
	 */

	public void OnboardPatient_039(String firstname, String lastname, String email, String pnonenum, String econtact)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);

		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);

		boolean subvalue = submit.isEnabled();

		Assert.assertEquals(subvalue, false, "submit not displayed");

		WebActionUtil.logger.info("Emergency field is not containing any data");
	}

	/*
	 * Eva_CCA_Onboard Patient_030 testcase verify phone number field have any
	 * validation message is or not
	 * 
	 */
	public void OnboardPatient_030(String firstname, String lastname, String email, String ephonenum, String econtact)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		String alertdata = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		WebActionUtil.sleep(3);
		if (alertdata.contains("Patient Created Successfully")) {
			WebActionUtil.logger.info("phone number doesn't have any validation and phone number is not a mandatory");

		} else {
			WebActionUtil.logger.info("phone number  have any validation and phone number is  a mandatory");
		}
		WebActionUtil.sleep(2);
	}

	/*
	 * Eva_CCA_Onboard Patient_029 verify phone number is mandatary or not
	 */
	public void OnboardPatient_029(String firstname, String lastname, String email, String ephonenum, String econtact)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");

		WebActionUtil.sleep(3);
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);

		boolean subvalue = submit.isEnabled();

		Assert.assertEquals(subvalue, true, "submit not displayed");

		WebActionUtil.logger.info("phone number field is not mandatary");
	}

	/*
	 * OnboardPatient_027 verifying date field is mandatary or not
	 * 
	 */
	public void OnboardPatient_027(String firstname, String lastname, String email, String phonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");

		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, phonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(caregiver, "caregiver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);

		boolean subvalue = submit.isEnabled();

		Assert.assertEquals(subvalue, false, "submit is displayed");

		WebActionUtil.logger.info("submit button is not displayed,hence Date field is manditary");
	}

	/*
	 * OnboardPatient_028 test case verifying phone number field is accepting phone
	 * number format are not
	 */
	public void OnboardPatient_028(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}
		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		String alertdata = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		if (alertdata.contains("Patient Created Successfully")) {
			WebActionUtil.logger.info("phone number is acceptiong phone number formate");

		} else {
			WebActionUtil.logger.info("phone number is not acceptiong phone number formate");
		}
		WebActionUtil.sleep(2);
	}

	/*
	 * OnboardPatient_024 verifying user can able to enter date is
	 */

	public void OnboardPatient_024(String firstname, String lastname, String email, String dob, String pnonenum,
			String ephonenum, String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(5);
		WebActionUtil.typeText(datefield, dob, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);

		boolean subvalue = submit.isEnabled();
		try {
			Assert.assertEquals(subvalue, false, "submit is displayed");

			WebActionUtil.logger.info("submit button is not displayed,hence entered date is not a date formate");
		} catch (AssertionError e) {

		}
		WebActionUtil.sleep(2);
	}

	/*
	 * Onboard Patient_022 verifying user can able to click previous and next
	 * buttons in calendar
	 */
	public void OnboardPatient_022() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(calender, "clicking on calander");
		WebActionUtil.sleep(3);
		String firstmonthvalue = WebActionUtil.getText(monthclick, "gettingmonthdata");

		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(next, "clicking on next button");
		WebActionUtil.sleep(3);
		String secondmonthvalue = WebActionUtil.getText(monthclick, "gettingmonthdata");

		if (firstmonthvalue.equalsIgnoreCase(secondmonthvalue)) {
			WebActionUtil.logger.info("user can't click to the next button");

		} else {
			WebActionUtil.logger.info("usercan can click to the next button");
		}
		WebActionUtil.clickOnElementUsingJS(calenderprevious, "click on previous in calender");
		WebActionUtil.sleep(3);
		String thirdmonthvalue = WebActionUtil.getText(monthclick, "gettingmonthdata");

		if (firstmonthvalue.equalsIgnoreCase(thirdmonthvalue)) {
			WebActionUtil.logger.info("usercan click to the previous button");
		} else {
			WebActionUtil.logger.info("usercan can't click to the previous button");
		}
	}

	/*
	 * Onboard Patient_023 testcase verify user can't selecting future date or not.
	 */
	public void OnboardPatient_023() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clear(datefield, "clearing the date field");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(calender, "clicking on calander");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(next, "clicking on next button");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(calenderdate, "selecting date");
		WebActionUtil.sleep(3);
		String datealertdata = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		try {
			Assert.assertEquals(datealertdata, "Please Select A valid Date");
			WebActionUtil.logger.info("user can not select the future date");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("user can  select the future date");
		}
	}
	/*
	 * OnboardPatient_026 verifying date filed is accepting decimal point instead of
	 * '/' symbol
	 */

	public void OnboardPatient_026(String firstname, String lastname, String email, String dob, String pnonenum,
			String ephonenum, String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(datefield, "datefield");
		WebActionUtil.sleep(5);
		WebActionUtil.typeText(datefield, dob, "dob");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		boolean subvalue = submit.isEnabled();
		try {
			Assert.assertEquals(subvalue, true, "submit is not displayed");

			WebActionUtil.logger.info("submit button is  displayed,hence entered date is  accepting deciman formate");
		} catch (AssertionError e) {
			WebActionUtil.logger
					.info("submit button is not displayed,hence entered date is not accepting decimanl formate");
		}
		WebActionUtil.sleep(2);
	}
	/*
	 * OnboardPatient_047 verifying caregiver field is mandatory or not
	 */

	public void OnboardPatient_047(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(3);
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.clearAndTypeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		boolean subvalue = submit.isEnabled();
		try {
			Assert.assertEquals(subvalue, true, "submit is not displayed");

			WebActionUtil.logger.info("submit button is  displayed,hence entered date is  accepting deciman formate");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("submit button is not displayed,hence caregiver field is mandatory");
		}
		WebActionUtil.sleep(2);
	}

	/*
	 * OnboardPatient_052 test case verifying phone number field is accepting phone
	 * number format are not
	 */
	public void OnboardPatient_052(String lastname, String email, String pnonenum, String ephonenum, String econtact)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicing on borard pts");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);

		String firstnamefield = RandomStringUtils.randomAlphabetic(5);

		WebActionUtil.clearAndTypeText(fName, firstnamefield, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eMail, email, "Email");
		WebActionUtil.sleep(3);

		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(3);

		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");

		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		// String monthdata= monthclick.getText();

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.clearAndTypeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		driver.switchTo().alert().accept();
		WebActionUtil.sleep(15);
		WebActionUtil.clickOnElementUsingJS(refresh, "clicking on refresh");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(serachtheelemt, firstnamefield, "firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(serachtheelemtbtn, "clicking on search btn");
		WebActionUtil.sleep(5);
		String search_elrment_attribute = getserchelement.getAttribute("title");

		try {
			Assert.assertEquals(search_elrment_attribute, firstnamefield);
			WebActionUtil.logger.info("created patient is appered in grid");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("created patient is not appered in grid");
		}

	}

	/*
	 * OnboardPatient_055 test case verifying editprofile is navigating or not
	 */
	public void OnboardPatient_055() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicing on borard pts");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(profileedit, "clicking on profileedit");
		WebActionUtil.sleep(3);
		try {
			WebActionUtil.isSelected(careviveredit, "caregiver selecting");
			WebActionUtil.logger.info("aplication is  navigated to the edit profile");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("aplication is not navigated to the edit profile");
		}
	}

	/*
	 * OnboardPatient_056 test case verifying Email is enable or not in editprofile
	 */
	public void OnboardPatient_056() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicing on borard pts");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(profileedit, "clicking on profileedit");
		WebActionUtil.sleep(3);
		String Emailenabled = eMail.getAttribute("ng-disabled");
		if (Emailenabled.contains("readOnly ||")) {
			WebActionUtil.logger.info("Email is enable in Profile edit page");
		} else {
			WebActionUtil.logger.info("Email is not enable in Profile edit page");
		}
	}

	/*
	 * 20 testcase method verifying current month calender is displaying or not
	 */
	public void OnboardPatient_020() throws Throwable {

		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicing on borard pts");
		WebActionUtil.sleep(3);

		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		String monthdata = monthclick.getText();

		if (monthdata.contains(month)) {
			WebActionUtil.logger.info("current month calender is displaying");
		} else {
			WebActionUtil.logger.info("current month calender is not displaying");
		}
	}

	public void OnboardPatient_021() throws Throwable {

		/*
		 * List<String> monthList = Arrays.asList("January", "February", "March",
		 * "April", "May", "June", "July", "August", "September", "October", "November",
		 * "December"); // Expected Date, Month and Year int expMonth; int expYear;
		 * String expDate = null; // Calendar Month and Year String calMonth = null;
		 * String calYear = null; boolean dateNotFound;
		 * WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicing on borard pts");
		 * WebActionUtil.sleep(3);
		 * 
		 * WebActionUtil.clickOnElementUsingJS(addNew, "add"); WebActionUtil.sleep(3);
		 * WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		 * WebActionUtil.sleep(2); dateNotFound = true;
		 * 
		 * //Set your expected date, month and year. expDate = "18"; expMonth= 8;
		 * expYear = 2013;
		 * 
		 * //This loop will be executed continuously till dateNotFound Is true.
		 * while(dateNotFound) { //Retrieve current selected month name from date picker
		 * popup. calMonth = monthclick.getText(); String[]
		 * calMonth1=calMonth.split(" ");
		 * WebActionUtil.clickOnElementUsingJS(monthclick, "calenderclicking");
		 * //Retrieve current selected year name from date picker popup. calYear =
		 * driver.findElement(By.xpath("//strong[@class='ng-binding']")).getText();
		 * System.out.println(calYear); System.out.println(calMonth1[0]); //If current
		 * selected month and year are same as expected month and year then go Inside
		 * this condition. if(monthList.indexOf(calMonth1[0])+1 == expMonth && (expYear
		 * == Integer.parseInt(calYear))) { //Call selectDate function with date to
		 * select and set dateNotFound flag to false. selectDate(expDate); dateNotFound
		 * = false; } //If current selected month and year are less than expected month
		 * and year then go Inside this condition. else
		 * if(monthList.indexOf(calMonth1[0])+1 < expMonth && (expYear ==
		 * Integer.parseInt(calYear)) || expYear > Integer.parseInt(calYear)) { //Click
		 * on next button of date picker. next.click(); } //If current selected month
		 * and year are greater than expected month and year then go Inside this
		 * condition. else if(monthList.indexOf(calMonth1[0])+1 > expMonth && (expYear
		 * == Integer.parseInt(calYear)) || expYear < Integer.parseInt(calYear)) {
		 * //Click on previous button of date picker.
		 * driver.findElement(By.xpath("//span[contains(text(),'previous')]")).click();
		 * 
		 * } WebActionUtil.sleep(3); } }
		 * 
		 * public void selectDate(String date) { WebElement datePicker =
		 * driver.findElement(By.id("ui-datepicker-div")); List<WebElement>
		 * noOfColumns=datePicker.findElements(By.tagName("td"));
		 * 
		 * //Loop will rotate till expected date not found. for (WebElement cell:
		 * noOfColumns){ //Select the date from date picker when condition match. if
		 * (cell.getText().equals(date)){ cell.findElement(By.linkText(date)).click();
		 * break; } }
		 */

		String expcurrmonth = "August";
		String expcurrentyear = "2019";
		String expcurrdate = "25";
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicing on borard pts");
		WebActionUtil.sleep(3);

		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		WebActionUtil.sleep(2);
		String currentmonth = monthclick.getText();

		/*
		 * exp month is current month
		 */
		if (currentmonth.contains(expcurrmonth)) {
			List<WebElement> datepicker = driver.findElements(
					By.xpath("//div[@class='uib-daypicker ng-scope']//td/button[@class='btn btn-default btn-sm']"));
			for (WebElement dates : datepicker) {

				String gridsdates = dates.getText();
				if (gridsdates.contains(expcurrdate)) {
					dates.click();
					break;
				}
			}
		}

		/*
		 * exp month and date is current year
		 */
		else {
			WebActionUtil.clickOnElementUsingJS(monthclick, "clicking on month");
			WebActionUtil.sleep(1);
			String curryear = year.getText();
			WebActionUtil.sleep(1);
			if (curryear.contains(expcurrentyear)) {
				List<WebElement> datemonth = driver
						.findElements(By.xpath("//div[@class='uib-monthpicker ng-scope']//td/button[@type='button']"));
				for (WebElement month : datemonth) {
					WebActionUtil.sleep(1);
					String gridsmonth = month.getText();
					if (gridsmonth.contains(expcurrmonth)) {
						WebActionUtil.sleep(1);
						month.click();
						WebActionUtil.sleep(2);
						List<WebElement> datepicker = driver.findElements(By.xpath(
								"//div[@class='uib-daypicker ng-scope']//td/button[@class='btn btn-default btn-sm']"));
						for (WebElement dates : datepicker) {

							String gridsdates = dates.getText();
							if (gridsdates.contains(expcurrdate)) {
								dates.click();
								break;
							}
						}
					}
				}
			}
		}
		WebActionUtil.sleep(3);
	}

	/*
	 * 35 testcase verifying emergency contact number is mandatory or not
	 */
	public void OnboardPatient_035(String firstname, String lastname, String email, String pnonenum, String ephonenum)
			throws Throwable {

		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(eMail, email, "Email");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		WebActionUtil.sleep(3);
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.clearAndTypeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clearAndTypeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		boolean subvalue = submit.isEnabled();
		try {
			Assert.assertEquals(subvalue, true, "submit is not displayed");

			WebActionUtil.logger.info("submit button is  displayed,hence emegency contact name is not required");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("submit button is not displayed,hence emegency contact name is  required");
		}
		WebActionUtil.sleep(2);
	}
	/*
	 * 
	 * 37 testcase verifying emergency phone number is accepting phone number
	 * formate or not
	 */

	public void OnboardPatient_037(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(fName, firstname, "Firstname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(lName, lastname, "Lastname");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gendermale, "gender");
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
		String[] monthName = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		Calendar cal = Calendar.getInstance();
		String month = monthName[cal.get(Calendar.MONTH)];
		while (true) {
			Thread.sleep(100);
			String monthdata = monthclick.getText();

			if (monthdata.contains(month)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}

		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.typeText(phoneNum, pnonenum, "Phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(ePhoneNum, ephonenum, "Ephonenum");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(eContactName, econtact, "Econtact");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		WebActionUtil.sleep(7);
		String alertdata = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		if (alertdata.contains("Patient Created Successfully")) {
			WebActionUtil.logger.info("emegency phone number is acceptiong phone number formate");

		} else {
			WebActionUtil.logger.info("emegency  phone number is not acceptiong phone number formate");
		}
		WebActionUtil.sleep(2);
	}

	/*
	 * 40 testcase verifying emergency phone number will accept character and
	 * Special characters
	 */
	/*
	 * Submit button should enable after all mandatory fields will filled with data
	 */
	public void onboardPatient40() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on addNew");
		WebActionUtil.sleep(6);
		WebActionUtil.typeText(first_Name, "Rahul", "Entering first name");
		WebActionUtil.typeText(last_Name, "Singham", "Lastname");
		WebActionUtil.typeText(eMail, WebActionUtil.generateEmail("gmail.com", 6), "Email");

		WebActionUtil.sleep(4);
		WebActionUtil.handlingCalender(clalender, "05", 10, 2019);

		WebActionUtil.clickOnElementUsingJS(gender_1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender, "gender");
		WebActionUtil.typeText(phone_Num, "3216549872", "Phonenumber");

		WebActionUtil.typeText(eContact_Name, RandomStringUtils.randomAlphabetic(5), "Econtact");
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);
		WebActionUtil.scrollByPixel(20, 0);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		boolean submitdata = WebActionUtil.isElementDisplayedOrNot(submitButton);
		try {
			Assert.assertEquals(submitdata, "true");
			WebActionUtil.logger.info("Button is Enable");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Button is not Enable");
		}
		WebActionUtil.sleep(5);
	}

	public void onboard_Patient41() throws Throwable {
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard_Paitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on Add_New_Button");
		WebActionUtil.typeText(eContact_Name, "123,4567890", "Entering the text in Emergency_Contact");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(3);
		try {
			WebActionUtil.verifyElementText(verifyContact, "Please Enter Valid Contact Name.");
			WebActionUtil.logger.info("Entered text is  displayed");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Entered text is not displayed");
		}

	}

	public void onboard_Patient42() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Paitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on AddNew");
		Shutterbug.shootElement(driver, mendotryField).withName("star").save();
		Snapshot s = Shutterbug.shootElement(driver, mendotryField).withName("star");
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElement(careGiver, "Clicking on the checkBox");
		Shutterbug.shootElement(driver, mendotryField).withName("star_AfterClicking").save();
		Snapshot p = Shutterbug.shootElement(driver, mendotryField).withName("star_AfterClicking");
		assertEquals(s, p);
		System.out.println("is it working ");
	}

	public void onboard_Patient44() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "Clicking on Onboard Paitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on AddNew");
		WebActionUtil.clickOnElement(careGiver, "Clicking on the checkBox");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(careGiver, "Clicking on the checkBox");
		WebActionUtil.sleep(3);
	}

	public void onboard_Patient45() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Paitent");
		WebActionUtil.clickOnElement(addNew, "Clicking on AddNew");
		WebActionUtil.clickOnElement(careGiver_1, "Clicking on the checkBox One");
		webActionUtil.sleep(3);
		WebActionUtil.clickOnElement(careGiver_2, "Clicking on the checkBox Two");
		webActionUtil.sleep(3);
		WebActionUtil.scrollToElement(careGiver_3, "Clicking on the checkBox Three");
		WebActionUtil.sleep(3);

	}

	public void onboard_Patient46() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Paitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on AddNew");
		WebActionUtil.typeText(first_Name, "Rahul", "Entering first name");
		WebActionUtil.typeText(last_Name, "Slathia", "Entering Last name");
		WebActionUtil.typeText(eMail, "aabi@hajs.com", "Entering Email");
		WebActionUtil.sleep(3);
		WebActionUtil.handlingCalender(clalender, "10", 10, 2021);
		WebActionUtil.clickOnElementUsingJS(gender_1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender, "gender");
		WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
		WebActionUtil.typeText(phone_Num, "1456328970", "Phonenumber");
		WebActionUtil.typeText(ePhone_Num, "3697412580", "Ephonenum");
		WebActionUtil.typeText(eContact_Name, "Lila", "Econtact");
		WebActionUtil.sleep(5);
		WebActionUtil.scrollByPixel(20, 0);
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(5);
		WebActionUtil.isElementDisplayed(submit, "Submit button", 1);
	}

	public void onboard_Patient48() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on OnBoard Paitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on Addnew button");
		WebActionUtil.isSelected(radioButton, "Checking the Radio Button");
		try {

			WebActionUtil.logger.info("Radio Button  Active is  displayed");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Radio Button  Active is  not displayed");
		}
	}

	public void onboard_Patient49() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on OnBoard Paitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on Addnew button");
		try {

			WebActionUtil.verifyElementText(clickRadioButton, "Status field is Disable");
			WebActionUtil.logger.info("Status field is enble");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Status field is Disable");
		}

		WebActionUtil.sleep(7);
	}

	public void onboard_Patient50(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(first_Name, firstname, "Firstname");
		WebActionUtil.typeText(last_Name, lastname, "Lastname");
		WebActionUtil.typeText(eMail, email, "Email");
		WebActionUtil.clickOnElementUsingJS(gender_1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender, "gender");
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
		while (true) {
			Thread.sleep(100);
			String monthdata = month_Click.getText();

			if (monthdata.equals(date)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}
		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.typeText(phone_Num, pnonenum, "Phonenumber");
		WebActionUtil.typeText(ePhone_Num, ephonenum, "Ephonenum");
		WebActionUtil.typeText(eContact_Name, econtact, "Econtact");
		WebActionUtil.clickOnElement(caregiver, "gecareviver selecting");
		WebActionUtil.sleep(5);

		WebActionUtil.scrollByPixel(20, 0);
		WebActionUtil.sleep(5);
		WebActionUtil.sleep(5);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);

		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(cancel, "Clicking on Cancel Button");

		try {
			WebActionUtil.verifyElementText(verifyText, "Savita");
			WebActionUtil.logger.info("paitent is Created");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("paitent is not Created");
		}
	}

	public void onboard_Patient_208() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_AddEvent, "Clicking on the Add Event button");
		WebActionUtil.sleep(5);
		WebActionUtil.clear(enter_Event, "Clearing the text");
		WebActionUtil.typeText(enter_Event, RandomStringUtils.randomAlphabetic(6), "Entering the Tittle");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(enter_Description,
				"Those who thinking Chanderyaan 2 is unsucessfull then click under the link and check the achivement :)",
				"Entering the description");
		WebActionUtil.clickOnElementUsingJS(click_Savebutton, "Clicking on Save button");

	}
	/*
	 * User should allow to view next Year/Month/Week / Day
	 */

	public void onboard_Patient_207() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_Year, "Clicking on the year");
		WebActionUtil.clickOnElementUsingJS(click_Month, "Clicking on the Month");
		WebActionUtil.clickOnElementUsingJS(click_Week, "Clicking on the Week");
		WebActionUtil.clickOnElementUsingJS(click_Day, "Clicking on the particular Week");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(click_NextButton, "clicking on the next Button");
		WebActionUtil.sleep(2);

	}

	/*
	 * User should allow to view previous Year/Month/Week / Day
	 */
	public void onboard_Patient_206() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_Year, "Clicking on the year");
		WebActionUtil.clickOnElementUsingJS(click_PreviousButton, "Clcking on the Previous Button");
		WebActionUtil.clickOnElementUsingJS(click_Month, "Clickin on the Month");
		WebActionUtil.clickOnElementUsingJS(click_PreviousButton, "Clcking on the Previous Button");
		WebActionUtil.clickOnElementUsingJS(click_Day, "Clicking on the Day");
		WebActionUtil.clickOnElementUsingJS(click_PreviousButton, "Clcking on the Previous Button");
		WebActionUtil.sleep(3);
	}

	/*
	 * Verify user can select Day based calendar
	 */
	public void onboard_Patient_205() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_Day, "Clicking on the Day");
		WebActionUtil.sleep(3);
		try {

			WebActionUtil.verifyElementText(click_DayText, "Wednesday 11 September, 2019");
			WebActionUtil.logger.info("Date is same");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Date is Not same");
		}
	}

	/*
	 * Verify user can select Week based calendar
	 */

	public void onboard_Patient_204() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_Week, "Clicking on the Week");
		WebActionUtil.sleep(3);
		try {

			WebActionUtil.verifyElementText(click_WeekDay, "Sunday");
			WebActionUtil.logger.info("Day is same");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Day is Not same");
		}

	}
	/*
	 * Verify user can select Month based calendar
	 */

	public void onboard_Patient_203() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_Month, "Clicking on the Month");

		WebActionUtil.sleep(3);
		try {
			WebActionUtil.verifyElementText(click_VerifyMonth, "September 2019");
			WebActionUtil.logger.info("Month is same");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Month is Not same");
		}
	}

	/*
	 * Verify user can select Year based calendar
	 */
	public void onboard_Patient_202() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_Year, "Clicking on the Year");
		WebActionUtil.sleep(3);
		try {
			WebActionUtil.verifyElementText(verify_Year, "2019");
			WebActionUtil.logger.info("Year is same");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Year is Not same");
		}
	}

	public void onboard_Patient_201() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.isElementDisplayed(click_Year, "year is present or not", 2);
		WebActionUtil.isElementDisplayed(click_Month, "month is present or not", 2);
		WebActionUtil.isElementDisplayed(click_Day, "day is present or not", 2);
		WebActionUtil.isElementDisplayed(click_NextButton, "next button  is present or not", 2);
		WebActionUtil.isElementDisplayed(click_PreviousButton, "previous button is present or not", 2);
		WebActionUtil.isElementDisplayed(schedule_Media, "Schedule media is dispalyed or not", 2);
		WebActionUtil.isElementDisplayed(click_AddEvent, "Add Event is displayed or not ", 2);
	}

	public void onboard_Patient_200() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_Event, "Clicking on Event");
		WebActionUtil.sleep(3);
		WebActionUtil.getText(verify_AddEventPage, "Getting the text of the Event Page");
		try {
			WebActionUtil.verifyElementText(verify_AddEventPage, "AddEventPage");
			WebActionUtil.logger.info("AddEventPage text verified");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("AddEventPage text not verified");
		}
	}

	public void onboard_Patient_190() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_Conversation, "Clicking on Event");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(conversation_AddNewButton, "Clicking on the Add New Button");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(conversation_QuestionPhrase, "what is the Full form of ISRO",
				"Entering the Text in the Box");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(conversation_PleaseSelect, "Clicking on the Mood");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(conversation_PleaseSelect_Mood, "Clicking on the Mood");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(conversation_Subject, "Entering the Subject");
		WebActionUtil.typeText(conversation_Subject, "Baseball sport", "Entering the subject");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.scrollByPixel(0, 300);
		WebActionUtil.scrollToElement(conversation_SubmitButton, "clicking on Submit button");
		WebActionUtil.sleep(3);

		WebActionUtil.clickOnElementUsingJS(conversation_SubmitButton, "Clicking on submit button");

	}

	public void onboard_Patient_198() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on Onboard Patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_Conversation, "Clicking on Event");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(conversation_DeleteButton, "Clciking on the Delete button");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(conversation_DeleteCancelButton, "Clicking on Cancel button");
		WebActionUtil.sleep(5);
		try {
			WebActionUtil.verifyElementText(verify_CancelButton, "aserf");
			WebActionUtil.logger.info("Text Present");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Text not Present");
		}

	}

	public void onboard_Patient_051(String firstname, String lastname, String email, String pnonenum, String ephonenum,
			String econtact) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(addNew, "add");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(first_Name, firstname, "Firstname");
		WebActionUtil.typeText(last_Name, lastname, "Lastname");

		boolean status = false;

		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.testDataPath1, "Sheet1", "TEST_CASE_NO");
		int i = 1;
		while (i < sdata1.length) {
			String spdata2 = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.testDataPath1, "Sheet1");
			WebActionUtil.clickOnElementUsingJS(eMail, "Email input box");
			WebActionUtil.typeText(eMail, email, "Email");
			driver.switchTo().activeElement().sendKeys(Keys.TAB);
			try {
				status = WebActionUtil.isAlertPresent(driver, 20);
			} catch (Exception e) {
				WebActionUtil.logger.info("Alert is not present");
			}
			System.out.println(status);
			if (status) {
				driver.switchTo().alert().accept();
				i++;
			} else {
				WebActionUtil.logger.info("Email suceesfully verified all conditions");
				break;
			}
		}
		WebActionUtil.clickOnElementUsingJS(gender_1, "gender");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(gender, "gender");
		String date = "September 2019";
		String expdate = "02";
		WebActionUtil.clickOnElementUsingJS(clalender, "calenderclicking");
		while (true) {
			Thread.sleep(100);
			String monthdata = month_Click.getText();

			if (monthdata.equals(date)) {
				break;
			} else {
				WebActionUtil.clickOnElementUsingJS(next, "Addnewbutton");
			}
		}
		driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();

		WebActionUtil.typeText(phone_Num, pnonenum, "Phonenumber");
		WebActionUtil.typeText(ePhone_Num, ephonenum, "Ephonenum");
		WebActionUtil.typeText(eContact_Name, econtact, "Econtact");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(caregiver, "gecareviver selecting");
		WebActionUtil.scrollByPixel(20, 0);
		WebActionUtil.sleep(2);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.clickOnElementUsingJS(submit, "submit");
		String alertdata = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();

		try {
			WebActionUtil.verifyElementText(verifyText, "Paitent ");
			WebActionUtil.logger.info("paitent is Created");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("paitent is not Created");
		}
	}

	public void onboard_Patient_052() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on OnBoardPaitent");
		WebActionUtil.typeText(click_SerchField, "anil", "Entering the data into Serch field");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(click_SerchFieldButton, "Clciking on the Serch Button");
		WebActionUtil.sleep(5);
		// String text = driver.findElement(By.xpath("//div[@class='input-group
		// leftLayoutControl']/descendant::input[@id='PatientsList.FirstName-0-2']")).getText();
		// System.out.println(text);
		try {
			// WebActionUtil.verifyElementText(verify_SerchFieldGrid, "anil");
			String tabledata = verify_SerchFieldGrid.getAttribute("title");
			System.out.println(tabledata);
			Assert.assertEquals(tabledata, "aabio");

			WebActionUtil.logger.info("paitent is present");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("paitent is not present");
		}

	}

	public void onboard_Patient_056() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clicking on OnBoardPaitent");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(click_ProfileColoum, "Clicking on the Profile Coloum");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(click_InactiveStatus, "Clicking on the Inactive Status");

		WebActionUtil.sleep(5);
		try {

			WebActionUtil.isSelected(click_InactiveStatus, "Checking the status");
			WebActionUtil.logger.info("User can able to Change the Status");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("User can not able to Change the Status");
		}
	}

	/*
	 * 
	 * verify user can create a duplicate data in tags field
	 */
	public void Eva_CCA_Onboard_Patient_079(String data) throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		webActionUtil.sleep(7);
		WebActionUtil.clickOnElement(addMusicMoviesButton, "clicking on add button");
		webActionUtil.sleep(5);
		if (!tagsRemoveButtons.isEmpty()) {
			for (WebElement element : tagsRemoveButtons) {
				boolean isDisplayed = element.isDisplayed();
				if (isDisplayed == true) {
					WebActionUtil.clickOnElement(element, "click on delete button");
					WebActionUtil.clickOnElement(confirmOkButton, "click on ok button in confirmation pop-up");
				}
			}
		}

		WebActionUtil.typeText(tagsTextbox, data, "entering data into tags textbox");
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElement(tagsAddButton, "clicking on add button of tags");
		webActionUtil.sleep(2);

		try {
			if (tagsTextButton.size() != 0) {
				Assert.assertEquals(tagsTextButton.size(), 1);
			}
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("Tags did not accept special characters");
		}
	}

	/*
	 * 
	 * verify the text field for data validation
	 */
	public void Eva_CCA_Onboard_Patient_081(String data) throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElement(addMusicMoviesButton, "clicking on add button");
		webActionUtil.sleep(3);
		if (!tagsRemoveButtons.isEmpty()) {
			for (WebElement element : tagsRemoveButtons) {
				boolean isDisplayed = element.isDisplayed();
				if (isDisplayed == true) {
					WebActionUtil.clickOnElement(element, "tags remove button");
					WebActionUtil.clickOnElement(confirmOkButton, "ok button in confirmation box");
				}
			}
		}

		WebActionUtil.typeText(tagsTextbox, data, "entering data into tag textbox");
		WebActionUtil.clickOnElement(tagsAddButton, "add button of tags");
		webActionUtil.sleep(2);
		try {
			if (tagsTextButton.size() != 0) {
				Assert.assertEquals(tagsTextButton.size(), 0);
			}
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("Tags accepted  more than 45 characters");
		}
	}

	static String getAlphaNumericString(int n) {
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int j = 0; j < n; j++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index = (int) (AlphaNumericString.length() * Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString.charAt(index));
		}
		return sb.toString();
	}

	/*
	 * 
	 * verify user can add more than 10 interests in each portlet
	 */
	public void Eva_CCA_Onboard_Patient_082(String tags, String music, String movies, String artist, int n)
			throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		webActionUtil.sleep(6);
		WebActionUtil.clickOnElement(addMusicMoviesButton, "clicking on add button");
		webActionUtil.sleep(3);
		if (!tagsRemoveButtons.isEmpty()) {
			for (WebElement element : tagsRemoveButtons) {
				boolean isDisplayed = element.isDisplayed();
				if (isDisplayed == true) {
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(element, "click on delete button");
					WebActionUtil.sleep(2);
					WebActionUtil.clickOnElement(confirmOkButton, "click on ok button in confirmation pop-up");
				}
			}
		}

		if (!musicRemoveButtons.isEmpty()) {
			for (WebElement element : musicRemoveButtons) {
				boolean isDisplayed = element.isDisplayed();
				if (isDisplayed == true) {
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(element, "click on delete button");
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(confirmOkButton, "click on ok button in confirmation pop-up");
				}
			}
		}

		if (!movieRemoveButtons.isEmpty()) {
			for (WebElement element : movieRemoveButtons) {
				boolean isDisplayed = element.isDisplayed();
				if (isDisplayed == true) {
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(element, "click on delete button");
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(confirmOkButton, "click on ok button in confirmation pop-up");
				}
			}
		}

		if (!artistRemoveButtons.isEmpty()) {
			for (WebElement element : artistRemoveButtons) {
				boolean isDisplayed = element.isDisplayed();
				if (isDisplayed == true) {
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(element, "click on delete button");
					WebActionUtil.sleep(1);
					WebActionUtil.clickOnElement(confirmOkButton, "click on ok button in confirmation pop-up");
				}
			}
		}

		for (int i = 0; i <= 14; i++) {
			WebActionUtil.typeText(tagsTextbox, getAlphaNumericString(n), "enter data into tags");
			webActionUtil.sleep(3);

			WebActionUtil.clickOnElement(tagsAddButton, "add button of tags field");
			webActionUtil.sleep(3);

			WebActionUtil.typeText(musicTextbox, getAlphaNumericString(n), "enter data into music text box");
			webActionUtil.sleep(3);

			WebActionUtil.clickOnElement(musicAddButton, "clicking on add button of music field");
			webActionUtil.sleep(3);

			WebActionUtil.typeText(movieTextbox, getAlphaNumericString(n), "entering data into movies text field");
			webActionUtil.sleep(3);

			WebActionUtil.clickOnElement(moviesAddButton, "clicking movies add button");
			webActionUtil.sleep(3);

			WebActionUtil.typeText(artistTextbox, getAlphaNumericString(n), "entering data into artist text field");
			webActionUtil.sleep(3);

			WebActionUtil.clickOnElement(artistAddButton, "clicking on artist add button");
			webActionUtil.sleep(3);

			int tagsCount = tagsTextButton.size();
			if (tagsCount > 10) {
				WebActionUtil.logger.info("tags field accepts more than 10 interests ");
			}

			if (musicTextButton.size() > 10) {
				WebActionUtil.logger.info("music field accepts more than 10 interests ");
			}

			if (movieTextButton.size() > 10) {
				WebActionUtil.logger.info("movie field accepts more than 10 interests ");
			}

			if (artistTextButton.size() > 10) {
				WebActionUtil.logger.info("artist field accepts more than 10 interests ");
			}
		}
	}

	/*
	 * 
	 * verify whether page is navigating to personalized media library page
	 */
	public void Eva_CCA_Onboard_Patient_083() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElement(personalizedLibraryAddButton, "clicking on add button");
		webActionUtil.sleep(2);

		try {
			WebActionUtil.isElementDisplayedOrNot(personalizedSearchTextbox);
			WebActionUtil.isElementDisplayedOrNot(personalizedSearchButton);
			WebActionUtil.logger.info("Navigation successful to personalized media library page");
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("Navigation was not successful to personalized media library page");
		}
	}

	/*
	 * 
	 * verify fields in persnalized media library page
	 */
	public void Eva_CCA_Onboard_Patient_084() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElement(personalizedLibraryAddButton, "clicking on add button");
		webActionUtil.sleep(2);

		try {
			WebActionUtil.isElementDisplayedOrNot(personalizedSearchTextbox);
			WebActionUtil.isElementDisplayedOrNot(personalizedSearchButton);
			WebActionUtil.isElementDisplayedOrNot(table);
			WebActionUtil.isElementDisplayedOrNot(allDropdown);
			WebActionUtil.isElementDisplayedOrNot(Refreshbutton);
			WebActionUtil.logger.info("All fields in personalized media library page is verified");
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("All fields in personalized media library page did not get verified");
		}
	}

	/*
	 * 
	 * verify persnalized media library page without giving personal interests and
	 * music, movies values
	 */
	public void Eva_CCA_Onboard_Patient_085() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElement(personalInterestAddButton, "clicking on add button");
		webActionUtil.sleep(4);
		if (!deleteButtons.isEmpty()) {
			for (WebElement element : deleteButtons) {
				WebActionUtil.clickOnElement(element, "click on delete element");
				WebActionUtil.clickOnElement(confirmOkButton, "click ok in confirmation pop-up");
			}
		}
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		webActionUtil.sleep(4);
		WebActionUtil.clickOnElement(addMusicMoviesButton, "clicking onboard patient");
		webActionUtil.sleep(2);
		if (!deleteButtons.isEmpty()) {
			for (WebElement element : deleteButtons) {
				WebActionUtil.clickOnElement(element, "click on delete element");
				WebActionUtil.clickOnElement(confirmOkButton, "click ok in confirmation pop-up");
			}
		}
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElement(personalizedLibraryAddButton, "clicking onboard patient");
		webActionUtil.sleep(2);
		try {
			WebActionUtil.isElementDisplayedOrNot(errorMessage);
			WebActionUtil.logger.info("No records error message is displayed");
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("No records error message is not displayed");
		}
	}

	/*
	 * 
	 * verify user can update the personalized media library or not
	 */
	public void Eva_CCA_Onboard_Patient_086() throws Throwable {

	}

	/*
	 * 
	 * verify user can update the personalized media library or not
	 */
	public void Eva_CCA_Onboard_Patient_089() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);

		WebActionUtil.clickOnElement(personalizedLibraryAddButton, "clicking onboard patient");
		webActionUtil.sleep(2);

		try {
			if (WebActionUtil.isElementClickable(deleteButton, "is alert clickable")) {
				WebActionUtil.logger.info("user able to delete records");
			}
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("user not able to delete records");
		}
	}

	/*
	 * 
	 * verify search in personalized media library page
	 */
	public void Eva_CCA_Onboard_Patient_090() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);

	}

	/*
	 * 
	 * verify search in personalized media library
	 */
	public void Eva_CCA_Onboard_Patient_091() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		try {
			if (WebActionUtil.isElementDisplayedOrNot(patientName)) {
				WebActionUtil.logger.info("page navigated to conversation page");
			}
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("page did not navigated to conversation page");
		}
	}

	/*
	 * 
	 * verify conversation page fields
	 */
	public void Eva_CCA_Onboard_Patient_092() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		try {
			String selectedpatient = selectedPatient.getText();
			Assert.assertEquals(selectedpatient, patientName);
			WebActionUtil.verifyElementText(patientName, selectedpatient);
			WebActionUtil.logger.info("selected patient is disaplyed");
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("selected patient is not disaplyed");
		}
	}

	// /*
	// *
	// * verify navigation to conversation page
	// */
	// public void Eva_CCA_Onboard_Patient_091() throws Throwable {
	// WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
	// WebActionUtil.sleep(2);
	//
	// WebActionUtil.clickOnElement(conversationAddButton, "click on add under
	// converstaion column");
	// webActionUtil.sleep(2);
	//
	// try {
	// WebActionUtil.isElementDisplayed(patientName, "", 10);
	// WebActionUtil.isElementDisplayed(conversationAddNewButton, "", 10);
	// for(WebElement element:gridList) {
	// WebActionUtil.isElementDisplayed(element, "", 10);
	// }
	// WebActionUtil.logger.info("verification of fields in convrsation page is
	// successful");
	// }
	// catch( AssertionError a) {
	// WebActionUtil.logger.info("verification of fields in convrsation page is not
	// successful");
	// }
	//
	// }

	/*
	 * 
	 * verify patient name field in add new conversation page
	 */
	public void Eva_CCA_Onboard_Patient_093() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		try {
			WebActionUtil.isElementDisplayed(patientName, "", 10);
			WebActionUtil.isElementDisplayed(conversationAddNewButton, "", 10);
			for (WebElement element : gridList) {
				WebActionUtil.isElementDisplayed(element, "", 10);
			}
			WebActionUtil.logger.info("verification in convrsation page is successful");
		} catch (AssertionError a) {
			WebActionUtil.logger.info("verification in convrsation page is not successful");
		}

	}

	/*
	 * 
	 * verify conversation grid
	 */
	public void Eva_CCA_Onboard_Patient_094() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add under converstaion column");
		webActionUtil.sleep(4);

		try {
			WebActionUtil.isElementDisplayed(alertCheckbox, "", 10);
			WebActionUtil.isElementDisplayed(moodDropdown, "", 10);
			WebActionUtil.logger.info("navigation to add conversation page is successful");
		} catch (AssertionError a) {
			WebActionUtil.logger.info("navigation to add conversation page is not successful");
		}
	}

	/*
	 * 
	 * verify navigation to add conversation page
	 */
	public void Eva_CCA_Onboard_Patient_095() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		try {
			WebActionUtil.isElementDisplayed(questiontextBox, "", 10);
			WebActionUtil.isElementDisplayed(moodDropdown, "", 10);
			WebActionUtil.isElementDisplayed(subjectTextbox, "", 10);
			WebActionUtil.isElementDisplayed(answerRadioButton, "", 10);
			WebActionUtil.isElementDisplayed(mediaRadiButton, "", 10);
			WebActionUtil.isElementDisplayed(answerMediatextArea, "", 10);
			WebActionUtil.isElementDisplayed(uploadImagelabel, "", 10);
			WebActionUtil.isElementDisplayed(alertCheckbox, "", 10);
			WebActionUtil.isElementDisplayed(recordlabel, "", 10);
			WebActionUtil.isElementDisplayed(withinDropdown, "", 10);
			WebActionUtil.isElementDisplayed(repeatedMoreThanTextBox, "", 10);
			WebActionUtil.isElementDisplayed(triggerDropdown, "", 10);
			WebActionUtil.isElementDisplayed(emailTextBox, "", 10);
			WebActionUtil.scrollToElement(submitButton, "");
			WebActionUtil.isElementDisplayed(submitButton, "", 10);
			WebActionUtil.isElementDisplayed(cancelButton, "", 10);
			WebActionUtil.logger.info("navigation to add conversation page is successful");

		} catch (AssertionError a) {
			WebActionUtil.logger.info("navigation to add conversation page is not successful");
		}
	}

	/*
	 * 
	 * verify fields in add conversation page
	 */
	public void Eva_CCA_Onboard_Patient_096(String question, String subject, String mood) throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(3);

		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add under converstaion column");
		webActionUtil.sleep(2);
		WebActionUtil.typeText(questiontextBox, question, "");
		WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + subject + "']"))
				.click();

		try {
			WebActionUtil.isElementEnabled(submitButton);
			WebActionUtil.logger.info("phrase accepts special characters");
		} catch (AssertionError a) {
			WebActionUtil.logger.info("phrase does not accepts special characters");
		}
	}

	/*
	 * 
	 * verify phrase field with valid data
	 */
	public void Eva_CCA_Onboard_Patient_097() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		try {
			WebActionUtil.isElementDisplayedOrNot(questionLabel);
			WebActionUtil.logger.info("phrase field is mandatory");
		}

		catch (AssertionError a) {
			WebActionUtil.logger.info("phrase field is not  mandatory");
		}
	}

	/*
	 * 
	 * verify phrase field is mandatory or not
	 */
	public void Eva_CCA_Onboard_Patient_098(String mood, String subject) throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(2);

		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add under converstaion column");
		webActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mood + "']"))
				.click();

		WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + subject + "']"))
				.click();

		try {
			WebActionUtil.isElementEnabled(submitButton);
			WebActionUtil.logger.info("submit is enabled after filling all mamndatory fields");
		} catch (AssertionError a) {
			WebActionUtil.logger.info("submit is not enabled after filling all mamndatory fields");
		}
	}

	/*
	 * 
	 * verify phrase field by not filling any data
	 */
	public void Eva_CCA_Onboard_Patient_099(String value1, String value2, String value3) throws Throwable {
		ArrayList<String> moodValues = new ArrayList<>();
		moodValues.add(value1);
		moodValues.add(value2);
		moodValues.add(value3);

		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddButton, "click on add under converstaion column");
		webActionUtil.sleep(3);

		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add under converstaion column");
		webActionUtil.sleep(3);

		try {
			WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
			WebActionUtil.typeText(moodDropdown, value1, "");
			WebElement element = WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + value1 + "']"));
			Assert.assertEquals(element.isDisplayed(), true);

			WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
			WebActionUtil.typeText(moodDropdown, value2, "");
			WebElement element1 = WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + value2 + "']"));
			Assert.assertEquals(element1.isDisplayed(), true);

			WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
			WebActionUtil.typeText(moodDropdown, value3, "Mood");
			WebElement element2 = WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + value3 + "']"));
			Assert.assertEquals(element2.isDisplayed(), true);
			WebActionUtil.logger.info("mood dropdown contains value");

		} catch (AssertionError a) {
			WebActionUtil.logger.info("mood dropdown does notcontains value");
		}
	}

	/*
	 * 
	 * verify user can enter special characters in phone number field
	 */
	public void Eva_CCA_Onboard_Patient_031(String data) throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(3);

		try {
			WebActionUtil.clickOnElement(onBoardAddNewButton, "clicking onboard patient");
			WebActionUtil.sleep(3);
			WebActionUtil.typeText(phoneNumberTextbox, data, "Phone No.");
			String text = WebActionUtil.getText(phoneNumberTextbox, "Phone No.");
			if (!text.equalsIgnoreCase(data)) {
				WebActionUtil.logger.info("succesfully phone number does not accept special characters");
			}
		} catch (AssertionError error) {
			WebActionUtil.logger.info("Failed to accept a valid phone number");
		}
	}

	/*
	 * 
	 * navigation to conversation page
	 */
	public static void navigationToconversationPage() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);

		WebActionUtil.clickOnElement(conversationAddButton, "clicking onboard patient");
		WebActionUtil.sleep(3);
	}

	/*
	 * 
	 * navigation to caregiver page
	 */
	public static void clickCaregiver() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(caregiver, "Caregiver");
	}

	/*
	 * 
	 * navigation to onboard add new page
	 */
	public static void navigationToOnBoardAddNew() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(onBoardAddNewButton, "AddNew");
		WebActionUtil.sleep(5);
	}

	/*
	 * 
	 * navigation to events page
	 */
	public static void navigationToEvents() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(eventsButton, "events");
	}

	/*
	 * 
	 * navigation to profile page
	 */
	public static void navigationToProfile() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);
		clickAddprofile();
	}

	public static void clickAddprofile() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(addProfileButton, "profile");
		WebActionUtil.sleep(3);
	}

	/*
	 * 
	 * navigation to music and movies page
	 */
	public static void navigationToMusicAndMovies() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(addMusicMoviesButton, "music and movies");
		WebActionUtil.sleep(3);
	}

	public static void navigationToDeviceRegistrationPage() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(registeredDeviceButton, "click on device registration button");
		WebActionUtil.sleep(3);
	}

	public static void navigationToDevicePageForSecondPatient() throws Throwable {
		WebActionUtil.clickOnElement(onboardPatient, "clicking onboard patient");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(secondRegisteredDeviceButton,
				"+ button under registered device column for second patient");
		WebActionUtil.sleep(3);
	}

	// Method to navigating to add conversation page
	public void navigateToAddConversation() {
		try {
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElement(onboardPatientTab, "Clicking on onboard patient tab");
			WebActionUtil.clickOnElement(addConversation, "Clicking on add conversation icon");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method to verify the Emergency Phone No. field by not filling any data
	public void Eva_CCA_Onboard_Patient_050(String firstname, String lastname, String email, String genderName,
			String phoneNumumber, String emergencyName, String emergencyNum) {
		try {
			WebActionUtil.clickOnElement(onboardPatientTab, "OnboardPatientTab");
//			Getting text of the pagination before adding the patient for count 
			String beforeCancel = pagination.getText();
			System.out.println(beforeCancel);
			WebActionUtil.clickOnElement(addNewButton, "Add new button");
			WebActionUtil.typeText(fName, firstname, "FirstName");
			WebActionUtil.typeText(lName, lastname, "LastName");
			WebActionUtil.typeText(eMail, email, "Email");
			WebActionUtil.clickOnElement(gender, "Gender");
			WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + genderName + "']")).click();
			String date = "September 2023";
			String expdate = "02";
			WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
			while (true) {
				Thread.sleep(100);
				String monthdata = monthclick.getText();

				if (monthdata.equals(date)) {
					break;
				} else {
					WebActionUtil.logger.info("It's not select the next month date");
				}
			}
			driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
			WebActionUtil.typeText(phoneNum, phoneNumumber, "phoneNum");
			WebActionUtil.typeText(eContactName, emergencyName, "emergencyName");
			WebActionUtil.typeText(ePhoneNum, emergencyNum, "emergencyNum");
			WebActionUtil.clickOnElement(caregiver, "Caregiver");
			WebActionUtil.clickOnElement(cancel, "Cancel");
			// Getting text of the pagination after clicking on cancel button to verify the
			// count of the patients in not increased
			String afterCancel = pagination.getText();
			System.out.println(afterCancel);
			Assert.assertEquals(beforeCancel, afterCancel);

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method to verify the status change is reflecting in patient grid or not
	public void Eva_CCA_Onboard_Patient_058() {
		try {
			WebActionUtil.clickOnElement(onboardPatientTab, "OnboardPatientTab");
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElement(patientOneEdit, "patientOneEdit");
			if (activeRadioBtn.isSelected()) {
				WebActionUtil.clickOnElement(inactiveRadioBtn, "inactiveRadioBtn");
				WebActionUtil.scrollToElement(submit, "Submit button");
				WebActionUtil.clickOnElement(submit, "Submit");
				WebActionUtil.sleep(15);
				WebActionUtil.driver.navigate().refresh();
				WebActionUtil.waitTillPageLoad(driver, 10);
				WebActionUtil.verifyAttributeValue(statusField, "Title", "Inactive");

			} else {
				WebActionUtil.clickOnElement(activeRadioBtn, "activeRadioBtn");
				WebActionUtil.scrollToElement(submit, "Submit button");
				WebActionUtil.clickOnElement(submit, "Submit");
				WebActionUtil.sleep(15);
				WebActionUtil.driver.navigate().refresh();
				WebActionUtil.waitTillPageLoad(driver, 10);
				WebActionUtil.verifyAttributeValue(statusField, "Title", "Active");
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method to verify CCA will uncheck caregiver from one patient, it will reflect
	// for other patients also
	public void Eva_CCA_Onboard_Patient_059() {
		try {
			WebActionUtil.clickOnElement(onboardPatientTab, "OnboardPatientTab");
			WebActionUtil.clickOnElement(patientOneEdit, "patientOneEdit");
			WebActionUtil.scrollToElement(caregiver, "Caregiver");
			java.util.List<WebElement> elements = allCheckboxes.findElements(By.xpath("//input[@type='checkbox']"));
			System.out.println(elements);
			// java.util.List<WebElement> allCheckboxes =
			// WebActionUtil.driver.findElements(By.xpath("//input[@type='checkbox']"));
			for (WebElement checkbox : elements) {
				if (checkbox.isSelected()) {
					continue;
				} else {
					checkbox.click();
				}
			}
			WebActionUtil.clickOnElement(submit, "Submit");
			WebActionUtil.driver.navigate().refresh();
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.clickOnElement(patientTwoEdit, "patientTwoEdit");
			WebActionUtil.scrollToElement(caregiver, "Caregiver");
			java.util.List<WebElement> elements1 = allCheckboxes1.findElements(By.xpath("//input[@type='checkbox']"));
			System.out.println(elements);
			for (WebElement checkbox : elements1) {
				if (checkbox.isSelected()) {
					continue;
				} else {
					checkbox.click();
				}
			}
			WebActionUtil.clickOnElement(submit, "Submit");
			WebActionUtil.driver.navigate().refresh();
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.clickOnElement(patientOneEdit, "patientOneEdit");
			WebActionUtil.scrollToElement(caregiver, "Caregiver");
			for (WebElement checkbox : elements) {
				if (!checkbox.isSelected()) {
					continue;
				} else {
					checkbox.click();
				}
			}
			WebActionUtil.driver.navigate().refresh();
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.clickOnElement(patientTwoEdit, "patientTwoEdit");
			WebActionUtil.scrollToElement(caregiver, "Caregiver");
			for (WebElement checkbox : elements1) {
				if (!checkbox.isSelected()) {
					Assert.fail("unchecked caregiver from one patient is reflecting for other patients also");
				} else {
					WebActionUtil.logger
							.info("unchecked caregiver from one patient is not reflecting for other patients also");
				}
			}

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method to verify the status change is reflecting in patient grid or not
	public void Eva_CCA_Onboard_Patient_061(String firstname, String lastname, String genderName, String phoneNumumber,
			String emergencyName, String emergencyNum, String expected) {
		try {
			WebActionUtil.clickOnElement(onboardPatientTab, "OnboardPatientTab");
			WebActionUtil.clickOnElement(addNewButton, "Add new button");
			WebActionUtil.typeText(fName, firstname, "FirstName");
			WebActionUtil.typeText(lName, lastname, "LastName");
			String email = WebActionUtil.generateEmail("gmail.com", 5);
			WebActionUtil.typeText(eMail, email, "Email");
			WebActionUtil.clickOnElement(gender, "Gender");
			WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + genderName + "']")).click();
			String date = "September 2019";
			String expdate = "02";
			WebActionUtil.clickOnElementUsingJS(calender, "calenderclicking");
			while (true) {
				Thread.sleep(100);
				String monthdata = monthclick.getText();

				if (monthdata.equals(date)) {
					break;
				} else {
					WebActionUtil.logger.info("It's not select the next month date");
				}
			}
			driver.findElement(By.xpath("(//span[@class='ng-binding'])[02]")).click();
			WebActionUtil.typeText(phoneNum, phoneNumumber, "phoneNum");
			WebActionUtil.typeText(eContactName, emergencyName, "emergencyName");
			WebActionUtil.typeText(ePhoneNum, emergencyNum, "emergencyNum");
			WebActionUtil.clickOnElement(caregiver, "Caregiver");
			WebActionUtil.clickOnElement(submit, "Submit");
			WebActionUtil.sleep(4);
			driver.switchTo().alert().accept();
			WebActionUtil.driver.navigate().refresh();
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.clickOnElement(addNewButton, "Add new button");
			WebActionUtil.sleep(3);
			WebActionUtil.typeText(eMail, email, "Email");
			WebActionUtil.clickOnElement(gender, "Gender");
			String actual = WebActionUtil.driver.switchTo().alert().getText();
			Assert.assertEquals(actual, expected);
			WebActionUtil.driver.switchTo().alert().accept();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
