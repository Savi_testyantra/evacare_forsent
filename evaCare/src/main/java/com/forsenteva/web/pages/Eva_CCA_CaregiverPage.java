package com.forsenteva.web.pages;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_CaregiverPage extends BasePage {

	public Eva_CCA_CaregiverPage(WebDriver driver) {
		super(driver);
	}

	// Onboard caregiver tab
	@FindBy(xpath = "//span[text() =' Onboard Caregiver ']")
	private WebElement onboardCaregiverTab;

	// Facility or Family field
	@FindBy(xpath = "//input[@id='AdminPatientName']")
	private WebElement facilityOrFamilyField;
	// Add new Button
	@FindBy(xpath = "//span[.='Add New']")
	private WebElement addNewButton;
	// Care giver grid
	@FindBy(xpath = "//div[@class='formio-data-grid ng-scope']")
	private WebElement caregiverGrid;
	// First name text field
	@FindBy(xpath = "//input[@id='FirstName']")
	private WebElement enterFirstName;
	// Last name text field
	@FindBy(xpath = "//input[@id='LastName']")
	private WebElement enterLastName;
	// Last name error message
	@FindBy(xpath = "//p[text()='Last Name : is required.']")
	private WebElement errorLastName;

	// Email id text field
	@FindBy(xpath = "//input[@id='Email']")
	private WebElement emailID;
	// phone number text field
	@FindBy(xpath = "//input[@id='PhoneNumber']")
	private WebElement phoneNumber;
	// relationship drop down
	@FindBy(xpath = "//i[@ng-click='$select.toggle($event)']")
	private WebElement relationShipDropDown;
	// Submit button
	@FindBy(id = "Submit")
	private WebElement submitButton;
	// Cancel button
	@FindBy(xpath = "//button[@id='CaregiverForm.Cancel']")
	private WebElement cancelButton;
	//Page logo(Go To Eva)
	@FindBy(xpath = "//a[@title='Go to Eva']")
	private WebElement clickOnLogo;
	//Edit button in Caregiver page
	@FindBy(xpath = "//div[@id='form-group-CaregiverList.edit-0-11']/button[@name='CaregiverList.edit-0-11']")
	private WebElement clickOnCaregiver_edit;
	//Inactive radio button
	@FindBy(xpath="//label[@class='control-label ng-binding']/input[@id='Status-5']")
	private WebElement clickOnCaregiver_inactive;
	//Lastname field
	@FindBy(xpath="//div[@class='leftLayout']/label[contains(text(),'Last Name : ')]")
	private WebElement lastname_mandatory;
	//Email field
	@FindBy(xpath="//div[@class='leftLayout']/label[contains(text(),'Email : ')]")
	private WebElement email_mandatory;
	//Relationship field
	@FindBy(xpath="//div[@class='ui-select-match ng-scope']/span[@class='btn btn-default form-control ui-select-toggle']")
	private WebElement relationshipfield;
	//Relationship dropdown	
	@FindBy(xpath="//span[contains(text(),'Aunt')]")
	private WebElement reldropdown;
		@FindBy(xpath = "//span[@class='ng-binding ng-scope'][text()='Son']")
	private WebElement option;

	@FindBy(xpath = "//div[@class='preloader-container']")
	private WebElement loader;

	@FindBy(xpath = "//button[@type='button'][text()='Ok']")
	private WebElement okButton;

	@FindBy(xpath = "//p[text()='Please Enter Email']")
	private WebElement emailErrorMsg;

	@FindBy(xpath = "//p[.='Phone Number : is required.']")
	private WebElement enterPhoneNoMessage;
	
	@FindBy(xpath="//div[@class='leftLayout']/label[contains(text(),'Last Name : ')]")
	private WebElement firsttname_mandatory;
	
	@FindBy(xpath = "//span[contains(text(),' Onboard Caregiver ')]")
	private WebElement onboardCareGiver;

	@FindBy(xpath = "//ul[@class='ui-select-choices ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu ng-scope']/descendant::span[@class='ui-select-choices-row-inner']/descendant::span[text()='Wife']")
	private WebElement scroll;

	@FindBy(xpath = "//ul[@class='ui-select-choices ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu ng-scope']/descendant::span[text()='Aunt']")
	private WebElement relationShipDropDownSelect;
	// DropDown
	@FindBy(xpath = "//ul[@class='ui-select-choices ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu ng-scope']")
	private WebElement dropDownList;
	
	@FindBy(xpath= "//div[@id='form-group-CaregiverList.edit-1-11']/descendant::span[@class='buttonText ng-binding']")
	private WebElement selectEditCaregiver;
	
	@FindBy(xpath= "//td[@class='formio-data-grid-row ng-scope']/descendant::div[@id='form-group-CaregiverList.FirstName-1-4']")
	private WebElement verifyModification;

	private Object[] alloptions;

	private int count = 0;

	/*
	 * Verify Care Center Admin can enter characters/Special characters/Numbers in
	 * Email field
	 */

	public void onboard_Caregiver_020() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCareGiver, "Clicking on Onboard Caregiver");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on the Addnew Button");
		WebActionUtil.typeText(emailID, "Test123$%&@a.com", "Clicking on the Email");
		WebActionUtil.sleep(5);
		WebActionUtil.verifySpecialCharacter("Test123$%&@a.com");

	}
	/*
	 * Verify the Phone Number is mandatory field or not
	 * 
	 */

	public void eva_Onboard_Caregiver23() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCareGiver, "Clciking on the Onboard CareGiver");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on the add new button");
		String phone_Number = phoneNumber.getAttribute("ng-class");

		WebActionUtil.sleep(3);
		if (phone_Number.contains("{'field-required': isRequired(component)}")) {
			WebActionUtil.logger.info("mandatory symbol (*) is present");
		}

		else

		{
			WebActionUtil.logger.info("mandatory symbol (*) is not present");
		}

	}

	/*
	 * Verify the Relationship Field
	 */

	public void eva_Onboard_Caregiver27() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCareGiver, "Clciking on the Onboard Caregiver");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on the add new button");
		WebActionUtil.clickOnElement(relationShipDropDown, "Clicking on relationship");
		WebActionUtil.scrollToElement(scroll, "Scrolling to the element");
		WebActionUtil.sleep(2);

		String[] a = { "Please Select", "Aunt", "Caregiver", "Daughter", "Friend", "Grandparent",
				"Grandson/Granddaughter", "Husband", "Other family member", "Parent", "Sibling", "Son", "Spouse",
				"Uncle", "Wife", };

		List<WebElement> myElements = driver.findElements(By.xpath(
				"//ul[@class='ui-select-choices ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu ng-scope']/descendant::span[@class='ui-select-choices-row-inner']"));

		for (WebElement e : myElements) {
			if (e.getText().equalsIgnoreCase(a[count])) {
				System.out.println("This value is equal=" + e.getText());
				count++;
			} else {
				System.out.println("This value is not equal =" + e.getText());
			}
		}
	}

	/*
	 * Verify the same phone number with multiple caregiver
	 * 
	 */

	public void eva_Onboard_Caregiver38() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCareGiver, "Clicking on the Onboard CareGiver");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on the add new button");
		WebActionUtil.typeText(enterFirstName, "Sindhu", "Entering on the First name");
		WebActionUtil.typeText(enterLastName, "N", "Entering the LastName");
		WebActionUtil.sleep(2);
		WebActionUtil.typeText(emailID, WebActionUtil.generateEmail("gamil.com", 5), "Entering the email");
		WebActionUtil.sleep(2);
		WebActionUtil.typeText(phoneNumber, "7406976942", "Entering the PhoneNumber");
		WebActionUtil.clickOnElementUsingJS(relationShipDropDown, "Clciking on the Relationship");
		WebActionUtil.clickOnElementUsingJS(relationShipDropDownSelect, "Selecting the aunt");
		WebActionUtil.clickOnElementUsingJS(submitButton, "clciking on the submit button");
		
		WebActionUtil.sleep(7);
		driver.findElement(By.xpath("//button[text()='Ok']")).click();
		
		WebActionUtil.sleep(9);
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on the add new button");
		WebActionUtil.typeText(enterFirstName, "Tulsi", "Entering on the First name");
		WebActionUtil.typeText(enterLastName, "Kumar", "Entering the LastName");
		WebActionUtil.sleep(2);
		WebActionUtil.typeText(emailID, WebActionUtil.generateEmail("gamil.com", 5), "Entering the email");
		WebActionUtil.sleep(2);
		WebActionUtil.typeText(phoneNumber, "7406976942", "Entering the PhoneNumber");
		WebActionUtil.clickOnElementUsingJS(relationShipDropDown, "Clciking on the Relationship");
		WebActionUtil.clickOnElementUsingJS(relationShipDropDownSelect, "Selecting the aunt");
		WebActionUtil.clickOnElementUsingJS(submitButton, "clciking on the submit button");
		WebActionUtil.isElementEnabled(submitButton);

	}
	
	public void eva_Onboard_Caregiver41() throws Throwable {
		String name = RandomStringUtils.randomAlphabetic(8);
		WebActionUtil.clickOnElementUsingJS(onboardCareGiver, "Clicking on the Onboard CareGiver");
		WebActionUtil.clickOnElementUsingJS(selectEditCaregiver, "Clickling on the edit Button of CareGiver");
		WebActionUtil.clearAndTypeText(enterFirstName, name, "Editing the firstname");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(4);
		WebActionUtil.clickOnElementUsingJS(submitButton, "clciking on the submit button");
		WebActionUtil.sleep(20);
//		WebActionUtil.isElementEnabled(submitButton);
	
		try {

			WebActionUtil.getText(verifyModification, "Getting the text of the modified name");
			WebActionUtil.logger.info("modification  is done");
		}
		catch(AssertionError e)
		{
			WebActionUtil.logger.info("Modification is not done");
		}
	}
	
	/*
	 *Checking Lastname field is mandatory(*) or not
	 */
public void OnboardCaregiver_014() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "clicking on onboard caregiver");
	WebActionUtil.clickOnElementUsingJS(addNewButton, "clicking on addnew button");
	String firstname_mandatory1=firsttname_mandatory.getAttribute("ng-class");
		
	if(firstname_mandatory1.contains("isRequired"))
	{
		WebActionUtil.logger.info("LastName field have mandatory symbol *");
	}
	else {
		WebActionUtil.logger.info("Lstname field don't have mandatory symbol *");
	}		
}
/* 
 * verify email field have mandatory * is there or  not
 */
public void OnboardCaregiver_018() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "clicking on onboard caregiver");
	WebActionUtil.clickOnElementUsingJS(addNewButton, "clicking on addnew button");
	String email_mandatory1=email_mandatory.getAttribute("ng-class");
		
	if(email_mandatory1.contains("isRequired"))
	{
		WebActionUtil.logger.info("Email field have mandatory symbol *");
	}
	else {
		WebActionUtil.logger.info("EMail field don't have mandatory symbol *");
	}
		
}
/*
 * verifying relation ship is accepting invalid data or not
 */
public void OnboardCaregiver_028(String firstname,String lastname,String email, String contact, String relation) throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "clicking on onboard caregiver");
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(addNewButton, "clicking on addnew button");
	WebActionUtil.sleep(1);
	WebActionUtil.typeText(enterFirstName, firstname, "entering first name");
	WebActionUtil.typeText(enterLastName, lastname, "entering lastname");
	WebActionUtil.typeText(emailID, email, "entering eamil id");
	WebActionUtil.typeText(phoneNumber, contact, "contact name");
	WebActionUtil.typeText(relationshipfield, relation, "entring relationship");
	
	boolean submit1=submitButton.isEnabled();
	
	if(submit1==false) {
		WebActionUtil.logger.info("submit button is not enable hence relation ship field is not accepting invalid data");
	}else {
		WebActionUtil.logger.info("submit button is  enable hence relation ship field is  accepting invalid data");	
	}
	WebActionUtil.clickOnElementUsingJS(relationShipDropDown, "click on relation ship button");
	WebActionUtil.clickOnElementUsingJS(reldropdown, "selecting dropdown");
	boolean submit2=submitButton.isEnabled();
	
	
	if(submit2==true) {
		WebActionUtil.logger.info("submit button is  enable hence user can able to select dropdown data");
	}else {
		WebActionUtil.logger.info("submit button is not enable hence user not able to select dropdown data");	
	}
	
}
   /*
    *Verifying whether User is navigating to OnboardCaregiver Page
    */
	public void verifyUserNavigatingToOnboardCaregiverPage() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.verifyTheTitle("Dashboard - Eva");
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
			WebActionUtil.verifyTheTitle("Onboard Caregiver - Eva");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/*
	 *Verifying Caregiver page fields
	 */
	public void verifyCaregiverPageFields() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
			WebActionUtil.verifyTheTitle("Onboard Caregiver - Eva");
			WebActionUtil.isElementDisplayed(facilityOrFamilyField, "Family", 1);
			WebActionUtil.isElementDisplayed(addNewButton, "Family", 1);
			WebActionUtil.isElementDisplayed(caregiverGrid, "Family", 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
		/*
		 *Verifying whether AdminName field is mandatory
		 */
		public void verifytheAdminNamefieldisReadonly() {
			try {
				WebActionUtil.waitTillPageLoad(driver, 10);
				WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
				WebActionUtil.isElementDisplayed(facilityOrFamilyField, "Family", 1);
				WebActionUtil.verifyAttributeValue(facilityOrFamilyField, "ng-disabled", "readOnly");
				
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

	/*
	 *Verfiying whether user is navigating to Caregiver page after clicking addNew button
	 */
	public void verifyUserNavigatingToaddNewPage() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10); 
			WebActionUtil.verifyTheTitle("Dashboard - Eva");
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
			WebActionUtil.verifyTheTitle("Onboard Caregiver - Eva");
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on addNewButton");
			WebActionUtil.isElementDisplayed(enterFirstName, "Firstname", 1);
			WebActionUtil.isElementDisplayed(enterLastName, "Lastname", 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/*
	 * Verfiying all the fields after clicking on addNew button
	 */
	public void verifyFiledsOFaddNewPage() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on addNewButton");
			WebActionUtil.isElementDisplayed(enterFirstName, "Firstname", 1);
			WebActionUtil.isElementDisplayed(enterLastName, "Lastname", 1);
			WebActionUtil.isElementDisplayed(emailID, "Email", 1);
			WebActionUtil.isElementDisplayed(phoneNumber, "PhoneNo", 1);
			WebActionUtil.isElementDisplayed(relationShipDropDown, "Relationship dropdown", 1);
			WebActionUtil.isElementDisplayed(submitButton, "Submit", 1);
			WebActionUtil.isElementDisplayed(cancelButton, "Cancel", 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
		/*
		 *Verifying submit button is enabled with out giving firstName
		 */
		public void verifySubmitButtonIsEnablewithoutFirstName(String lastname, String email, String contact, String relation) {
			try {
				Thread.sleep(3000);
				WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
				WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on addNewButton");
				WebActionUtil.typeText(enterLastName, lastname, "Firstname");
				WebActionUtil.typeText(emailID, email, "Email");
				WebActionUtil.clickOnElementUsingJS(relationShipDropDown, "Dropdown");
				WebActionUtil.sleep(1);
				WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + relation + "']")).click();
				WebActionUtil.typeText(phoneNumber, contact, "Phone number");
				phoneNumber.sendKeys(Keys.TAB);
				WebActionUtil.isElementNotEnabled(submitButton);

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	
	   /*
	    *Verifying lastname field with valid data
	    */	
		public void VerifytheLastNamefieldwithvaliddata(String validLastname, String numericLastname,
				String specCharLastName) {
			try {
				Thread.sleep(3000);
				WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
				WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on addNewButton");
				WebActionUtil.typeText(enterLastName, validLastname, "Firstname");
				enterLastName.sendKeys(Keys.TAB);
				if (errorLastName.isDisplayed()) {
					System.out.println(
							WebActionUtil.getText(errorLastName, "Last name text field will not accept alphabets"));
				} else {
					System.out.println("Last name text field will accept alphabets");
				}
				WebActionUtil.clearAndTypeText(enterLastName, numericLastname, "Firstname");
				enterLastName.sendKeys(Keys.TAB);
				if (errorLastName.isDisplayed()) {
					System.out.println(
							WebActionUtil.getText(errorLastName, "Last name text field will not accept number"));
				} else {
					System.out.println("Last name text field will accept number");
				}
				WebActionUtil.clearAndTypeText(enterLastName, specCharLastName, "Firstname");
				enterLastName.sendKeys(Keys.TAB);
				if (errorLastName.isDisplayed()) {
					System.out.println(
							WebActionUtil.getText(errorLastName, "Last name text field will not accept special charecter"));
				} else {
					System.out.println("Last name text field will accept special charecter");
				}

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	
        /*
		 *Verifying submit button is enabled with out giving LastName
		 */	
		
	public void verifySubmitButtonIsEnablewithoutLastName(String firstname, String email, String contact, String relation) {
		try {
			Thread.sleep(3000);
			WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "Clicking on onboard caregiver tab");
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Clicking on addNewButton");
			WebActionUtil.typeText(enterFirstName, firstname, "Firstname");
			WebActionUtil.typeText(emailID, email, "Email");
			WebActionUtil.clickOnElementUsingJS(relationShipDropDown, "Dropdown");
			WebActionUtil.sleep(1);
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + relation + "']")).click();
			WebActionUtil.typeText(phoneNumber, contact, "Phone number");
			phoneNumber.sendKeys(Keys.TAB);
			WebActionUtil.isElementNotEnabled(submitButton);

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

/*
 * verifying whether click on inactive radio button in caregiver edit page	
 */
public void OnboardCaregiver_042() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "clicking on onboard caregiver");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElementUsingJS(clickOnCaregiver_edit, "clicking on edit page");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElementUsingJS(clickOnCaregiver_inactive, "clicking on inactive radio button");
	boolean inactive=WebActionUtil.isSelected(clickOnCaregiver_inactive, "iactive button is selected");
	Assert.assertEquals(inactive, true);
	WebActionUtil.logger.info("able to click on inactive radio button");
}

public void verifyInvalidEmailFormatMessage(String fName, String lName, String emailId, String phNum,
			String relation) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(enterFirstName, fName, "Firstname");
		WebActionUtil.typeText(enterLastName, lName, "LastName");
		WebActionUtil.typeText(emailID, emailId, "ValidEmailID");
		WebActionUtil.typeText(phoneNumber, phNum, "phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(relationShipDropDown, "relationShipDropDown");
		WebActionUtil.sleep(1);
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + relation + "']"))
				.click();
		WebActionUtil.clickOnElement(submitButton, "submitButton");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(okButton, "okButton");

	}

//Verify the  Email field by not filling any data

	public void verifySubmitButtonIsDisabled(String fName, String lName, String phNum, String relation)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(enterFirstName, fName, "Firstname");
		WebActionUtil.typeText(enterLastName, lName, "LastName");
		WebActionUtil.typeText(phoneNumber, phNum, "phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(relationShipDropDown, "relationShipDropDown");
		WebActionUtil.sleep(1);
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + relation + "']"))
				.click();
		WebActionUtil.isElementNotEnabled(submitButton);

	}

//Verify Care Center Admin can enter characters/Special characters/Numbers  in Email field

	public void verifyValidEmailID(String invalidEmailId, String validEmailId, String validSpechEmailId)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(emailID, invalidEmailId, "ValidEmailID");
		emailID.sendKeys(Keys.TAB);
		if (emailErrorMsg.isDisplayed()) {
			System.out.println(WebActionUtil.getText(emailErrorMsg, "Error Message"));
		} else {
			System.out.println("Error message is not displaying");
		}

		WebActionUtil.clearAndTypeText(emailID, validEmailId, "ValidEmailID");
		emailID.sendKeys(Keys.TAB);
		if (emailErrorMsg.isDisplayed()) {
			System.out.println(WebActionUtil.getText(emailErrorMsg, "Error Message"));
		} else {
			System.out.println("Error message is not displaying");
		}

		WebActionUtil.clearAndTypeText(emailID, validSpechEmailId, "ValidEmailID");
		emailID.sendKeys(Keys.TAB);
		if (emailErrorMsg.isDisplayed()) {
			System.out.println(WebActionUtil.getText(emailErrorMsg, "Error Message"));
		} else {
			System.out.println("Error message is not displaying");
		}
	}

//Verify the Email  field with invalid data

	public void Eva_CCA_OnboardCaregiver_021(String invalidEmailId) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(clickOnLogo, "Go To Eva");
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(emailID, invalidEmailId, "ValidEmailID");
		emailID.sendKeys(Keys.TAB);
		if (emailErrorMsg.isDisplayed()) {
			System.out.println(WebActionUtil.getText(emailErrorMsg, "Error Message"));
		} else {
			System.out.println("Error message is not displaying");
		}
	}

//Verify the  Phone Number field with valid Phone Number

	public void verifyValidPhoneNumber(String fName, String lName, String emailId, String phNum, String relation)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(enterFirstName, fName, "Firstname");
		WebActionUtil.typeText(enterLastName, lName, "LastName");
		WebActionUtil.typeText(emailID, emailId, "ValidEmailID");
		WebActionUtil.typeText(phoneNumber, phNum, "phonenumber");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(relationShipDropDown, "relationShipDropDown");
		WebActionUtil.sleep(1);
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + relation + "']"))
				.click();
		WebActionUtil.clickOnElement(submitButton, "submitButton");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(okButton, "okButton");
	}

//Verify the Phone Number is mandatory field or not	
	public void verifyPhoneNoMandatoryMessage(String phNum) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(phoneNumber, phNum, "phonenumber");
		phoneNumber.sendKeys(Keys.TAB);
		WebActionUtil.clear(phoneNumber, "phNumber");
		phoneNumber.sendKeys(Keys.TAB);
		if (enterPhoneNoMessage.isDisplayed()) {
			System.out.println(WebActionUtil.getText(enterPhoneNoMessage, "Error Message"));
		} else {
			System.out.println("Error message is not displaying");
		}

	}

//Verify the Phone Number field by not filling any data

	public void verifySubmitButtonIsDisabledWithoutPhoneNo(String fName, String lName, String emailId, String relation)
			throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(enterFirstName, fName, "Firstname");
		WebActionUtil.typeText(enterLastName, lName, "LastName");
		WebActionUtil.typeText(emailID, emailId, "ValidEmailID");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(relationShipDropDown, "relationShipDropDown");
		WebActionUtil.sleep(1);
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + relation + "']"))
				.click();
		WebActionUtil.isElementEnabled(submitButton);

	}

	//Verify user can enter characters/Special characters in  Phone Number field
	public void Eva_CCA_OnboardCaregiver_025(String validPhNum, String invalidPhNum) throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardCaregiverTab, "caregiverTab");
		WebActionUtil.clickOnElementUsingJS(addNewButton, "Addnew");
		WebActionUtil.typeText(phoneNumber, validPhNum, "phNum");
		phoneNumber.sendKeys(Keys.TAB);
		WebActionUtil.clearAndTypeText(phoneNumber, invalidPhNum, "phNum");
		phoneNumber.sendKeys(Keys.TAB);
		if (enterPhoneNoMessage.isDisplayed()) {
			System.out.println(WebActionUtil.getText(enterPhoneNoMessage, "Error Message"));
		} else {
			System.out.println("Error message is not displaying");
		}

	}

}
