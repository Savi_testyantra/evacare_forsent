package com.forsenteva.web.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_Login_NewPasswordPage extends BasePage {

	public Eva_CCA_Login_NewPasswordPage(WebDriver driver) {
		super(driver);
		
	}
	
	/*
	 * 31 testcase web elements
	 */
	@FindBy(xpath = "//div[@class='control-group']/label[contains(text(),' Password ')]")
	private  WebElement newpass_password;
	@FindBy(xpath = "//div[@class='control-group']/label[contains(text(),' Enter Again ')]")
	private  WebElement newpass_enteragain;
	@FindBy(xpath = "//div[@class='button-holder ']/button[@class='btn btn-primary']")
	private  WebElement newpass_save;
	
	/*
	 * 34 testcase webelements
	 */
	
	@FindBy(xpath = "//div[@class='alert alert-error']")
	private  WebElement newpass_alert;
	
	/*
	 *Login_031 test case
	 * verify the new password page contains password,enter again,submit button are displays are not
	 */
	
	public void Login_031() throws Throwable
	{
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.logintestcase, "Sheet1", "TEST_CASE_NO");

		String username = ExcelDataProvider.readcolData(sdata1[1], "email", GenericLib.logintestcase, "Sheet1");
		String password = ExcelDataProvider.readcolData(sdata1[1], "pass", GenericLib.logintestcase, "Sheet1");

		Eva_CCA_Login_Loginpage.LoginApplication(username, password);
		WebActionUtil.sleep(1);

		boolean newpass_pass = WebActionUtil.isElementDisplayedOrNot(newpass_password);

		WebActionUtil.sleep(1);
		boolean newpass_enteragain1 = WebActionUtil.isElementDisplayedOrNot(newpass_enteragain);

		WebActionUtil.sleep(1);
		boolean newpass_submit = WebActionUtil.isElementDisplayedOrNot(newpass_save);

		WebActionUtil.sleep(1);
		if (newpass_pass == true && newpass_enteragain1 == true && newpass_submit == true) {
			WebActionUtil.logger.info("password,enteragain and submit buttons are didplayed in new password page");
		}
	}
	
	/*
	 *Login_034 test case
	 * verify the new password pagehave validation message is there or not.
	 */
public void Login_034() throws Throwable
{
	String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.logintestcase, "Sheet1", "TEST_CASE_NO");

	String username = ExcelDataProvider.readcolData(sdata1[1], "email", GenericLib.logintestcase, "Sheet1");
	String password = ExcelDataProvider.readcolData(sdata1[1], "pass", GenericLib.logintestcase, "Sheet1");

	Eva_CCA_Login_Loginpage.LoginApplication(username, password);
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(newpass_save, "click on save button");
	String alertdata=WebActionUtil.getText(newpass_alert, "alertvalidation");
	Assert.assertEquals(alertdata, "That password is invalid. Please enter in a different password.");
	WebActionUtil.logger.info("new password page have validation message notifying the user as \"That password is invalid. Please enter in a different password.\"");
}
}
