package com.forsenteva.web.pages;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_Login_Loginpage extends BasePage {

	public Eva_CCA_Login_Loginpage(WebDriver driver) {
		super(driver);

	}

	/*
	 * testcase 50 web elements
	 */
	@FindBy(xpath = "//input[@type='text']")
	private static WebElement emailTxtBx;

	@FindBy(xpath = "//input[@type='password']")
	private static WebElement passWordTxtBx;

	@FindBy(xpath = "//button[@class='btn btn-primary']")
	private static WebElement loginBtn;
	
	/*
	 * 51 testcae webelement
	 */
	@FindBy(xpath = "//div[contains(text(),' Authentication failed, please try again ')]")
	private  WebElement validationmsg;
	
	/*
	 * 52 testcae webelement
	 */
	@FindBy(xpath = "//div[@class='required']")
	private  WebElement fieldrequired;
	@FindBy(xpath = "//div[@class='nbt-site-logo']/descendant::img[@class='site-logo']")
	private WebElement verify_SignPage;

	@FindBy(xpath = "//div[@class='control-group']/descendant::label[.='Username']")
	private WebElement user_Name;

	@FindBy(xpath = "//div[@class='control-group']/descendant::label[.=' Password ']")
	private WebElement user_Password;

	@FindBy(xpath = "//a[@id='_58_rjus_column1_0']/descendant::span[.='Register Care Centre']")
	private WebElement register_Care_Centre;
	
	@FindBy(xpath = "//label[text()='Care Centre Name ']/descendant::span[text()='*']")
	private WebElement mendatory_Care_Centre;
	
	@FindBy(xpath = "//label[text()='Admin Name ']/descendant::span[text()='*']")
	private WebElement mendatory_AdminName;
	
	
	
	

	@FindBy(xpath = "//div[@class='navigation']/descendant::span[.='Forgot Password']")
	private WebElement forget_Password;

	@FindBy(xpath = "//div[@class='button-holder ']/descendant::button[.='Submit']")
	private WebElement submit_Button;

	@FindBy(xpath = "//div[@class='control-group form-inline']/descendant::label[@class='checkbox']")
	private WebElement remember_Me;

	@FindBy(xpath = "//div[@id='nbt-sign-in-content']/descendant::header[@class='portlet-topper']/h1/span[text()='Create Account']")
	private WebElement verify_CreateAccountPage;

	@FindBy(xpath = "//div[@class='control-group']/descendant::label[text()='Care Centre Name ']")
	private WebElement care_Center_Name;

	@FindBy(xpath = "//div[@class='control-group']/descendant::label[text()=' Account for ']")
	private WebElement account_For;

	@FindBy(xpath = "//div[@class='control-group']/descendant::label[text()='Admin Name ']")
	private WebElement admin_Name;

	@FindBy(xpath = "//div[@class='row-fluid']/descendant::label[text()='Email Address ']")
	private WebElement email_Adress;

	@FindBy(xpath = "//div[@class='control-group']/descendant::label[text()='Text Verification ']")
	private WebElement text_Verification;

	@FindBy(xpath = "//div[@class='navigation']/descendant::span[text()='Sign In']")
	private WebElement sign_Button;

	@FindBy(xpath = "//div[@class='navigation']/descendant::span[text()='Forgot Password']")
	private WebElement forgetPassword_Button;
	
	@FindBy(xpath = "//input[@id='_58_firstName']")
	private WebElement enter_CareCenterName;
	
	@FindBy(xpath = "//input[@id='_58_lastName']")
	private WebElement enter_AdminName;
	
	@FindBy(xpath = "//input[@id='_58_emailAddress']")
	private WebElement enter_EmailAdress;
	
	
	
	@FindBy(xpath = "//select[@id='_58_accountFor']")
	private WebElement click_Account;
	
	@FindBy(xpath = "//select[@id='_58_accountFor']")
	private List<WebElement> verify_Account;

	/*
	 * Verify Sign In page of Eva on entering Eva application URL
	 */
	public void eva_Login_001() throws Throwable {
		try {
			WebActionUtil.verifyTheTitle("Login - Eva");
			WebActionUtil.logger.info("Entered text is  displayed");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Entered text is not displayed");
		}
	}

	/*
	 * Verify the fields in Sign In page
	 */
	public void eva_Login_002() throws Throwable {
		WebActionUtil.isElementDisplayed(user_Name, "Username is Displayed", 3);
		WebActionUtil.isElementDisplayed(user_Password, "UserPassword is Displayed", 3);
		WebActionUtil.isElementDisplayed(register_Care_Centre, "Register care center is Displayed", 3);
		WebActionUtil.sleep(2);
		WebActionUtil.isElementDisplayed(forget_Password, "ForgetPassword is Displayed", 4);
		WebActionUtil.isElementDisplayed(remember_Me, "RememberMe is Displayed", 3);
		WebActionUtil.isElementDisplayed(submit_Button, "SubmitButton is Displayed", 3);
	}

	/*
	 * Verify when user will clicks the Register Care Center hyperlink, it is
	 * redirecting to Create Account page
	 */
	public void eva_Login_003() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(register_Care_Centre, "Clicking on Register Care Centre");
		try {
			WebActionUtil.verifyElementText(verify_CreateAccountPage, "CREATE ACCOUNT");
			WebActionUtil.logger.info("Title is same ");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Title is not same");
		}

	}

	/*
	 * Verify Create Account page fields
	 */
	public void eva_Login_004() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(register_Care_Centre, "Clicking on Register Care Centre");
		WebActionUtil.isElementDisplayed(care_Center_Name, "Care Center name is Displayed", 3);
		WebActionUtil.isElementDisplayed(account_For, "Account_For is Displayed", 3);
		WebActionUtil.isElementDisplayed(admin_Name, "Admin Name is Displayed", 3);
		WebActionUtil.sleep(2);
		WebActionUtil.isElementDisplayed(email_Adress, "Email Adress is Displayed", 3);
		WebActionUtil.isElementDisplayed(text_Verification, "Text Verification is Displayed", 3);
		WebActionUtil.isElementDisplayed(submit_Button, "Submit Button is Displayed", 3);
		WebActionUtil.isElementDisplayed(sign_Button, "Sign Button is Displayed", 3);
		WebActionUtil.isElementDisplayed(forgetPassword_Button, "Forget Password Button is Displayed", 3);
	}

	/*
	 * Verify the Care Centre Name field is mandatory field or not
	 */
	public void eva_Login_006() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(register_Care_Centre, "Clicking on Register Care Centre");
		try {
			WebActionUtil.verifyElementText(mendatory_Care_Centre, "*");
			WebActionUtil.logger.info("Mandatory field is present");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Mandatory field is not present");
		}

	}
		
	public void eva_Login_014() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(register_Care_Centre, "Clicking on Register Care Centre");
		
		try {
			WebActionUtil.verifyElementText(mendatory_AdminName, "*");
			WebActionUtil.logger.info("Mandatory Adminfield is present");
		} catch (AssertionError e) {
			WebActionUtil.logger.info("Mandatory Adminfield is not present");
		}

	
		
		
	}
	public void eva_Login_009() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(register_Care_Centre, "Clicking on Register Care Centre");
		for (WebElement e : verify_Account)
	      {
	          System.out.println(e.getText());
	      }
		
	}


/*
 * method for login to the application
 */
	public static void LoginApplication(String user, String pass) throws Throwable {

		WebActionUtil.clearAndTypeText(emailTxtBx, user, "passing username");
		
		WebActionUtil.clearAndTypeText(passWordTxtBx, pass, "passing password");
		WebActionUtil.clickOnElementUsingJS(loginBtn, "clicking on submitbutton");
	}

	/*
	 * verify can able to login with valid credentials
	 */
	public void Login_050() throws Throwable {

		int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
		int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

		String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
		String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
		LoginApplication(sdatauser[indexOfUserName], sdatapass[indexOfpass]);
		WebActionUtil.sleep(2);
		String loginsuccess = driver.getTitle();
		WebActionUtil.verifyTheTitle(loginsuccess);
		WebActionUtil.logger.info("Application login is successfull with valid details");

	}
/*
 * verifying can able to login with invalid email id
 */
public void Login_051() throws Throwable
{
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	LoginApplication(RandomStringUtils.randomAlphabetic(5), sdatapass[indexOfpass]);
	WebActionUtil.sleep(2);
	String authenticationmsg=WebActionUtil.getText(validationmsg, "validationmessage");
	
	Assert.assertEquals(authenticationmsg, "Authentication failed, please try again");
	
	WebActionUtil.logger.info("Application have a validation message  as Authentication failed, please try again");
	
	
}
/*
 * verifying can able to login without  email id
 */
public void Login_052() throws Throwable
{
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	LoginApplication("", sdatapass[indexOfpass]);
	WebActionUtil.sleep(2);
	String username_authenticationmsg=WebActionUtil.getText(fieldrequired, "validationmessage");
	
	Assert.assertEquals(username_authenticationmsg, "This field is required.");
	
	WebActionUtil.logger.info("Application have a validation message  as This field is required.");
}
/*
 * verifying entered password is displaying in encrypted or not
 */
public void Login_053() throws Throwable
{
	int indexOfUserName = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "User");
	int indexOfpass = ExcelDataProvider.getColumnIndex(GenericLib.testDataPath1, "Sheet1", "pass");

	String[] sdatauser = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	String[] sdatapass = ExcelDataProvider.toReadExcelData(GenericLib.testDataPath1, "Sheet1", "TC_001");
	WebActionUtil.typeText(emailTxtBx, sdatauser[indexOfUserName], "emailpassing");
	WebActionUtil.typeText(passWordTxtBx, sdatapass[indexOfpass], "passwordpassing");
	String pass_encrypted=passWordTxtBx.getAttribute("type");
	Assert.assertEquals(pass_encrypted, "password");
	WebActionUtil.logger.info("entered password is showing encrypted type");
	
}
}
