package com.forsenteva.web.pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_PersonalizedMusicAndMoviesPage extends BasePage {

	public Eva_CCA_OnboardPatient_PersonalizedMusicAndMoviesPage(WebDriver driver) {
		super(driver);
		
	}
	
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	/*
	 * 73 testcase webelements
	 */
	
	
	@FindBy(xpath = "//a[contains(text(),'Personalized Media')]")
	private WebElement musicpage;
	
	/*
	 * 74 Testcases webelements
	 */
	@FindBy(xpath = "//span[contains(text(),'Tags')]")
	private WebElement tags;
	@FindBy(xpath = "//span[contains(text(),'Music')]")
	private WebElement music;
	@FindBy(xpath = "//span[contains(text(),'Movie')]")
	private WebElement movie;
	@FindBy(xpath = "//div[@class='input-group']/descendant::span[contains(text(),'Artist')]")
	private WebElement artist;
	
	/*
	 * 75 testcases
	 */
	@FindBy(xpath = "//input[@id='MediaTags']")
	private WebElement tagsfield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaTags.Add']")
	private WebElement tagsplus;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMusic.add']")
	private WebElement musicplusfield;
	
	@FindBy(xpath = "//input[@id='MediaMovie']")
	private WebElement Moviefield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMovies.add']")
	private WebElement movieplus;
	
	@FindBy(xpath = "//div[@id='form-group-MediaArtist']/div/div//descendant::input[@id='MediaArtist']")
	private WebElement artistfield;
	
	@FindBy(xpath = "//div[@id='form-group-PersonalizedMediaArtist.add']/descendant::button[@id='PersonalizedMediaArtist.add']")
	private WebElement artistplus;	
	
	/*
	 * 76 testcase webelements
	 */
	
	
	@FindBy(xpath = "//button[@id='PatientsList.PersonalizedMusicandMovies-0-7']")
	private WebElement musicplus;
	
	@FindBy(xpath = "//input[@id='MediaTags']")
	private WebElement tagfield;
	
	@FindBy(xpath = "//button[@name='PersonalizedMediaTags.Add']")
	private WebElement tagadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText-0-1']")
	private WebElement tagdata;
	
	
	@FindBy(xpath = "//input[@id='MediaMusic']")
	private WebElement musicfield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMusic.add']")
	private WebElement musicadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText2-0-1']")
	private WebElement musicdata;
	
	
	@FindBy(xpath = "//input[@id='MediaMovie']")
	private WebElement moviefield;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaMovies.add']")
	private WebElement movieadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText3-0-1']")
	private WebElement moviedata;
	
	@FindBy(xpath = "//button[@id='PersonalizedMediaArtist.add']")
	private WebElement artistadd;
	
	@FindBy(xpath="//div[@id='form-group-PersonalizedMediaText4-0-1']")
	private WebElement artistdata;

	/*
	 * 78 th test case web elements
	 * 
	 */
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete-0-2']")
	private WebElement tagdataremoval;
	
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete2-0-2']")
	private WebElement musicdataremoval;
	
	
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete3-0-2']")
	private WebElement moviedataremoval;
	
	@FindBy(xpath="//button[@name='PersonalizedMediaDelete4-0-2']")
	private WebElement artistdataremoval;
	
	@FindBy(xpath="//button[@class='btn btn-primary pull-right ng-binding']")
	private WebElement alertbox;
	
	@FindBy(xpath = "//div[@id='form-group-MediaTags']//input[@id='MediaTags']")
	private WebElement tagsTextbox;

	@FindBy(xpath = "//div[@id='form-group-PersonalizedMediaTags.Add']//button[@id='PersonalizedMediaTags.Add']")
	private WebElement tagsAddButton;

	public void Eva_CCA_Onboard_Patient_082(String tagData) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToMusicAndMovies();
		
		WebActionUtil.typeText(tagsTextbox, tagData, "Tags");
		WebActionUtil.clickOnElementUsingJS(tagsAddButton, "tag Add");
	}
	
	/*
	 * user can navigating music page
	 * 73 th test case
	 */
	public void OnboardPatient_073() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
		WebActionUtil.sleep(3);
		//String musicpagedata=WebActionUtil.getText(musicpage, "validationmusic");
		
		
		try {
			WebActionUtil.verifyElementText(musicpage, "Personalized Media");
			WebActionUtil.logger.info("user is in navigated to Music and Movies page ");
		}catch(AssertionError e){
			WebActionUtil.logger.info("user is not in navigated to Music and Movies page ");
		}
	}
	
	/*
	 * verifying music page fields
	 * 74 th test case
	 */
	
	public void OnboardPatient_074() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
		WebActionUtil.sleep(3);
		
		boolean tags1=WebActionUtil.isElementDisplayedOrNot(tags);
		WebActionUtil.sleep(3);
		boolean music1=WebActionUtil.isElementDisplayedOrNot(music);
		WebActionUtil.sleep(3);
		boolean movie1=WebActionUtil.isElementDisplayedOrNot(movie);
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(artist, "artist");
		boolean artist1=WebActionUtil.isElementDisplayedOrNot(artist);
		
		
		/*
		 * validating the result
		 */
		
		if(tags1==true && music1==true && movie1==true && artist1==true)
		{
			
			WebActionUtil.logger.info("tags,music,movie,artist fields are displaying");
			
		}
		else
		{
			WebActionUtil.logger.info("tags,music,movie,artist fields are not displaying");
		}
		
	}
	/*
	 * verifying music page
	 * 75 th test script
	 */
	
	public void OnboardPatient_075() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
		WebActionUtil.sleep(3);
		
		boolean tagsfield1=WebActionUtil.isElementDisplayedOrNot(tagsfield);
		WebActionUtil.sleep(3);
		boolean tagsplu1s=WebActionUtil.isElementDisplayedOrNot(tagsplus);
		WebActionUtil.sleep(3);
		boolean musicfield1=WebActionUtil.isElementDisplayedOrNot(musicfield);
		WebActionUtil.sleep(3);
		boolean movieplus1=WebActionUtil.isElementDisplayedOrNot(movieplus);
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(artist, "artist");
		WebActionUtil.sleep(3);
		boolean artistfield1=WebActionUtil.isElementDisplayedOrNot(artistfield);
		WebActionUtil.sleep(3);
		boolean artistplus1=WebActionUtil.isElementDisplayedOrNot(artistplus);
	
		
		/*
		 * validating the result
		 */
		
		if(tagsfield1==true && tagsplu1s==true && musicfield1==true 
				&& movieplus1==true && artistfield1==true && artistplus1==true)
		{
			
			WebActionUtil.logger.info("tags,music,movie,artist fields and add button  are displaying");
			
		}
		else
		{
			WebActionUtil.logger.info("tags,music,movie,artist fields and add button are not displaying");
		}
	}

	/*
	 * verifying user can add data
	 * 76 th test case
	 */
	public void OnboardPatient_076() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(tagfield, RandomStringUtils.randomAlphabetic(5), "adding data on tagfield");
		WebActionUtil.sleep(4);
		WebActionUtil.clickOnElement(tagadd, "click on add");
		/*String data=tagdata.getAttribute("title");
		WebActionUtil.sleep(3);
		
		System.out.println(data);*/
		WebActionUtil.sleep(3);
		boolean data=WebActionUtil.isElementDisplayedOrNot(tagdata);
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(musicfield,RandomStringUtils.randomAlphabetic(5), "adding data on music");
		WebActionUtil.sleep(3);
		
		WebActionUtil.clickOnElement(musicadd, "click on add");
		
		
		WebActionUtil.sleep(2);
		
		/*String data1=musicdata.getAttribute("title");
		
		System.out.println(data1);*/
		
		boolean data1=WebActionUtil.isElementDisplayedOrNot(musicdata);
		
		
		WebActionUtil.typeText(moviefield, RandomStringUtils.randomAlphabetic(5), "adding data on Movie");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(movieadd, "click on add");
		
		WebActionUtil.sleep(3);
		/*String data3=moviedata.getAttribute("title");
		
		System.out.println(data3);*/
		
		boolean data2=WebActionUtil.isElementDisplayedOrNot(moviedata);
		
		
		WebActionUtil.scrollToElement(artist, "artist");
		WebActionUtil.typeText(artistfield, RandomStringUtils.randomAlphabetic(5), "adding data on Artist");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(artistadd, "click on add");
		
		WebActionUtil.sleep(3);
		/*String data4=artistdata.getAttribute("title");
		
		System.out.println(data4);*/
		
		boolean data3=WebActionUtil.isElementDisplayedOrNot(artistdata);
		
		
		if(data==true && data1==true && data2==true && data3==true )
		{
			WebActionUtil.logger.info("User should be allowed to add data's into  Tags/Music/Movie/Artist Portlets");
		}else
		{
			WebActionUtil.logger.info("User not allowed to add data's into  Tags/Music/Movie/Artist Portlets");
		}
	}
	/*
	 * verify the user can edit the entered data
	 * 77 th testcase
	 */
	public void OnboardPatient_077() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
		WebActionUtil.sleep(3);
		
		WebActionUtil.typeText(tagfield, RandomStringUtils.randomAlphabetic(5), "adding data on tagfield");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(tagadd, "click on add");
		WebActionUtil.sleep(3);
		String data=tagdata.getAttribute("read-only");
		
		WebActionUtil.sleep(3);
		
		WebActionUtil.typeText(musicfield, RandomStringUtils.randomAlphabetic(5), "adding data on music");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(musicadd, "click on add");
		WebActionUtil.sleep(2);
		String data1=musicdata.getAttribute("read-only");
		
		WebActionUtil.sleep(3);
		
		WebActionUtil.typeText(moviefield, RandomStringUtils.randomAlphabetic(5), "adding data on Movie");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(movieadd, "click on add");
		WebActionUtil.sleep(3);
		String data2=moviedata.getAttribute("read-only");
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(artist, "artist");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(artistfield, RandomStringUtils.randomAlphabetic(5), "adding data on Artist");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(artistadd, "click on add");
		WebActionUtil.sleep(3);
		String data3=artistdata.getAttribute("read-only");
		WebActionUtil.sleep(3);
		if(data.contains("isDisabled(col, row)") && data1.contains("isDisabled(col, row)")
				&& data2.contains("isDisabled(col, row)") && data3.contains("isDisabled(col, row)") )
		{
			WebActionUtil.logger.info("user can't edit the data and he can add the data & delete the data");
		}
		else
		{
			WebActionUtil.logger.info("user can edit the data and he can add the data & delete the data");
		}
	}
	/*
	 * removing add data checking
	 * 78 th testcase
	 */
	
	public void OnboardPatient_078() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(musicplus, "click music");
		WebActionUtil.sleep(1);
		WebActionUtil.typeText(tagfield, RandomStringUtils.randomAlphabetic(5), "adding data on tagfield");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElement(tagadd, "click on add");
		boolean data=WebActionUtil.isElementDisplayedOrNot(tagdata);
		
	
		
		WebActionUtil.typeText(musicfield,RandomStringUtils.randomAlphabetic(5), "adding data on music");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(musicadd, "click on add");
		WebActionUtil.sleep(2);
		boolean data1=WebActionUtil.isElementDisplayedOrNot(musicdata);
		
		WebActionUtil.typeText(moviefield, RandomStringUtils.randomAlphabetic(5), "adding data on Movie");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(movieadd, "click on add");
		WebActionUtil.sleep(3);
		boolean data2=WebActionUtil.isElementDisplayedOrNot(moviedata);
		WebActionUtil.sleep(3);
		WebActionUtil.scrollToElement(artist, "artist");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(artistfield,RandomStringUtils.randomAlphabetic(5), "adding data on Artist");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(artistadd, "click on add");
		WebActionUtil.sleep(3);
		
		boolean data3=WebActionUtil.isElementDisplayedOrNot(artistdata);
		
		if(data==true && data1==true && data2==true && data3==true )
		{
			WebActionUtil.logger.info("User should be allowed to add data's into  Tags/Music/Movie/Artist Portlets");
		}else
		{
			WebActionUtil.logger.info("User not allowed to add data's into  Tags/Music/Movie/Artist Portlets");
		}
			WebActionUtil.scrollToElement(tags, "tag");
			WebActionUtil.clickOnElement(tagdataremoval, "click on add");
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElement(alertbox, "click on add");
			WebActionUtil.sleep(3);
		try {
			boolean datar11=WebActionUtil.isElementDisplayedOrNot(tagdata);
			WebActionUtil.logger.info("user can not delete the tag data");
		}catch(Exception e) 
		{
			WebActionUtil.logger.info("user can delete the tag data");
			
		}
	
			WebActionUtil.clickOnElement(musicdataremoval, "click on add");
			WebActionUtil.sleep(3);
			
			WebActionUtil.clickOnElement(alertbox, "click on add");
		try {
				boolean data1r=WebActionUtil.isElementDisplayedOrNot(musicdata);
		
				WebActionUtil.logger.info("user can not delete the musicdata");
		}catch(Exception e) 
		{
			WebActionUtil.logger.info("user can  delete the musicdata");
		}
		
			WebActionUtil.clickOnElement(moviedataremoval, "click on add");
			WebActionUtil.sleep(3);
			
			WebActionUtil.clickOnElement(alertbox, "click on add");
		try {
				boolean data2r=WebActionUtil.isElementDisplayedOrNot(moviedata);
				WebActionUtil.logger.info("user can not delete the moviedata");
			}catch(Exception e)
			{
				WebActionUtil.logger.info("user can  delete the moviedata");
			}
		WebActionUtil.scrollToElement(artist, "artist");
			WebActionUtil.clickOnElement(artistdataremoval, "click on add");
			WebActionUtil.sleep(3);
			
			WebActionUtil.clickOnElement(alertbox, "click on add");
		
		try {
			boolean data3r=WebActionUtil.isElementDisplayedOrNot(artistdata);
			WebActionUtil.logger.info("user can not delete the artistdata");
		}catch(Exception e) {
			WebActionUtil.logger.info("user can  delete the artistdata");
		}
		
	}
}
