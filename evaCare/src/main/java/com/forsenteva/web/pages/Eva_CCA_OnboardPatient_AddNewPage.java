package com.forsenteva.web.pages;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_AddNewPage extends BasePage{
	public Eva_CCA_OnboardPatient_AddNewPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//div[@id='form-group-PhoneNumber']//input[@id='PhoneNumber']")
	private WebElement phoneNumberTextBox;

	@FindBy(xpath = "//div[@id='form-group-FirstName']//input[@id='FirstName']")
	private WebElement firstNameTextBox;

	@FindBy(xpath = "//div[@id='form-group-LastName']//input[@id='LastName']")
	private WebElement lastNameTextBox;

	@FindBy(xpath = "//div[@id='form-group-EmergencyContactName']//input[@id='EmergencyContactName']")
	private WebElement emergencyContactTextBox;

	@FindBy(xpath = "//div[@id='form-group-Email']//input[@id='Email']")
	private WebElement emailtextbox;

	@FindBy(xpath = "//div[@id='form-group-Email']//label[@for='Email']")
	private WebElement emailLabel;

	@FindBy(xpath = "//div[@id='form-group-DateOfBirth']//input[@id='DateOfBirth']")
	private WebElement dobtextbox;

	@FindBy(xpath = "//div[@id='form-group-EmergencyContactName']//p[text()='Please Enter Valid Contact Name.']")
	private WebElement emergencyContactErrorMsg;

	@FindBy(xpath = "//div[@id='form-group-EmergencyContactNo']//label[@for='EmergencyContactNo']")
	private WebElement emergencyContactNumberLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Add New')]")
	private WebElement addNew;

	@FindBy(xpath="//i[@class='caret pull-right']")
	private WebElement conversation_PleaseSelect;
	
	@FindBy(xpath="//span[.='Agitated']")
	private WebElement conversation_PleaseSelect_Mood;
	
	@FindBy(xpath="//div[@id='Subjects']/descendant::input[@type='search']")
	private WebElement conversation_Subject;

	@FindBy(xpath = "//div[@id='form-group-PatientsList.Conversation-0-9']/descendant::button[@id='PatientsList.Conversation-0-9']")
	private WebElement clicking_OnConversation;

	@FindBy(xpath = "//div[@id='form-group-CommunicationForm.Event2-0-29']/descendant::button[@id='CommunicationForm.Event2-0-29']")
	private WebElement clicking_OnDeleteConversation;

	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	
	@FindBy(xpath = "//button[contains(text(),'Ok')]")
	private WebElement delete_Ok;
	
	@FindBy(xpath = "//div[@class='input-group topLayoutControl']/descendant::input[@id='Question-0-4']")
	private WebElement conversation_Text;
	
	@FindBy(xpath = "//button[@id='CommunicationForm.AddNew']")
	private WebElement conversation_AddNew;

	@FindBy(xpath = "//div[@class='input-group leftLayoutControl']/descendant::input[@id='CommunicationForm.QuestionPhrase']")
	private WebElement conversation_Question;
	
	@FindBy(xpath="//button[@id='PatientsList.Conversation-0-9']")
	private WebElement click_Conversation;

	@FindBy(xpath="//button[@id='Submit']")
	private WebElement conversation_SubmitButton;
	
	@FindBy(xpath="//div[@id='form-group-Question-0-4']")
	private WebElement conversation_Attribute;//div[@id='form-group-PatientsList.PersonalInterests-0-6']/descendant::button[@id='PatientsList.PersonalInterests-0-6']

	public void eva_Onboard_Paitent199() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clciking on the Onboard Paitent");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(click_Conversation, "Clicking on the Conversation");
		
		WebActionUtil.sleep(5);
		
		WebActionUtil.clickOnElementUsingJS(clicking_OnConversation, "Clciking on the Conversation Button");
		WebActionUtil.clickOnElementUsingJS(clicking_OnDeleteConversation, "Clciking on the Delete conversation");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(delete_Ok, "Clicking on the Delete button");
		String abc=conversation_Attribute.getAttribute("title");
		try {

			Assert.assertEquals(abc, "");
			WebActionUtil.logger.info("value added");
		}catch(AssertionError e)
		{
			WebActionUtil.logger.info("text is not present");
		}
		
	}



	public void eva_Onboard_Paitent191() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clciking on the Onboard Paitent");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(click_Conversation, "Clicking on the Conversation");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(addNew, "Clicking on the Addnew");
		WebActionUtil.sleep(3);
		String question_Phrase = RandomStringUtils.randomAlphabetic(4);
		
		WebActionUtil.typeText(conversation_Question, question_Phrase, "Typing the Name");
		WebActionUtil.clickOnElementUsingJS(conversation_PleaseSelect, "Clicking on the Mood");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(conversation_PleaseSelect_Mood, "Clicking on the Mood");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElementUsingJS(conversation_Subject, "Entering the Subject");
		WebActionUtil.typeText(conversation_Subject, "Baseball sport", "Entering the subject");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
//		WebActionUtil.scrollByPixel(0, 300);
		WebActionUtil.scrollToElement(conversation_SubmitButton, "Scroll on Submit button");
		WebActionUtil.clickOnElementUsingJS(conversation_SubmitButton, "clicking on Submit button");
		WebActionUtil.sleep(15);
		
		String abc=conversation_Attribute.getAttribute("title");
		
		try {
			
						Assert.assertEquals(abc, question_Phrase);
						WebActionUtil.logger.info("value added");
					}catch(AssertionError e)
					{
						WebActionUtil.logger.info("text is not present");
					}

	}

	public void eva_Onboard_Paitent64() throws Throwable {
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Clciking on the Onboard Paitent");
		WebActionUtil.sleep(5);
	}


	/*
	 * verify the emergency contact field with valid data
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_033(String emergencyContactData) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToOnBoardAddNew();
		WebActionUtil.sleep(4);
		try {
			WebActionUtil.typeText(emergencyContactTextBox, emergencyContactData, "");
			String contact = WebActionUtil.getText(emergencyContactTextBox,"" );
			Assert.assertEquals(contact, emergencyContactData);	
			WebActionUtil.logger.info("Emergency contact field accepts uppercase characters");
		}

		catch(AssertionError e) {
			WebActionUtil.logger.info("Emergency contact field does not accept uppercase characters");
		}
	}

	/*
	 * verify the emergency contact field is mandatory or not
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_034() throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToOnBoardAddNew();
		WebActionUtil.sleep(4);
		try {
			Assert.assertEquals(emailLabel.getAttribute("class").contains("required"), true);
			WebActionUtil.logger.info("Email field is a mandatory field");
		}

		catch(AssertionError e) {
			WebActionUtil.logger.info("Email field is not a mandatory field");
		}
	}

	/*
	 * verify the emergency contact field by not filling any data
	 * 
	 * 
	 */

	public void Eva_CCA_Onboard_Patient_035(String firstName,String lastName,String emailDomain, int emailLength) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToOnBoardAddNew();
		WebActionUtil.sleep(4);
		WebActionUtil.typeText(firstNameTextBox, firstName, "First Name");
		WebActionUtil.typeText(lastNameTextBox, lastName, "Last Name");
		WebActionUtil.generateEmail(emailDomain, emailLength);


	}

	/*
	 * verify the emergency contact field with invalid data
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_036(String emergencyContact,String ExpEmergencyContactErrorMsg) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToOnBoardAddNew();
		WebActionUtil.sleep(4);
		try {
			WebActionUtil.typeText(emergencyContactTextBox, emergencyContact, "Emergency Contact");
			WebActionUtil.pressTab(emergencyContactTextBox, "Emergency Contact");
			String actualErrorMsg = WebActionUtil.getText(emergencyContactErrorMsg, "error Msg of Emergency Contact field");
			Assert.assertEquals(actualErrorMsg, ExpEmergencyContactErrorMsg);
			WebActionUtil.logger.info("Emergency contact field does not accept numeric data");
		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("Emergency contact field accepted numeric data");
		}
	}

	/*
	 * verify the emergency phone number is mandatory or not
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_038() throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToOnBoardAddNew();
		WebActionUtil.sleep(4);
		try {
			Assert.assertEquals(emergencyContactNumberLabel.getAttribute("class").contains("required"), true);
			WebActionUtil.logger.info("Emergency phone number field is a mandatory field");
		}

		catch(AssertionError e) {
			WebActionUtil.logger.info("Emergency phone number field is not a mandatory field");
		}		
	}
}