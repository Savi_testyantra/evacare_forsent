package com.forsenteva.web.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_RegisteredDevicesPage extends BasePage{
	String reg;
	public Eva_CCA_OnboardPatient_RegisteredDevicesPage(WebDriver driver) {
		super(driver);
		
	}
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	/*
	 * Eva_CCA_Onboard  Patient_237 webElements 
	 * 
	 */
	@FindBy(xpath="//button[@id='PatientsList.RegisteredDevices-0-11']")
	private WebElement regdecivebuttn;
	
	@FindBy(xpath="//a[contains(text(),'DeviceRegistration')]")
	private WebElement regdecivetext;
	
	/*
	 * Eva_CCA_Onboard  Patient_238 WebElements

	 */
	@FindBy(xpath = "//div[@class='formio-data-grid ng-scope']/descendant::table/descendant::th[@class='ng-binding ng-scope']")
	private List<WebElement> regpagefields;
	@FindBy(xpath="//input[@id='LDeviceRegistrationCode']")
	private WebElement regderegfield;
	
	/*
	 * Eva_CCA_Onboard  Patient_241 webElements
    */
		@FindBy(xpath="//input[@id='LDeviceRegistrationCode']")
		private WebElement regdevicecodeedit;
	
	/*
	 * Eva_Care_OnboardPatient_237Test case
	 * verifying user is navigated to regesterDevice page or not
	 */
	public void OnboardPatient_237() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboardpatient");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(regdecivebuttn, "clicking reg");
		String regdata=WebActionUtil.getText(regdecivetext, "regtext");
		try {
			Assert.assertEquals(regdata, "DeviceRegistration");
			WebActionUtil.logger.info("user is navigated to the Regdevice page");
		}catch(AssertionError e)
		{
			WebActionUtil.logger.info("user is not navigated to the Regdevice page");
		}
		
	}
	/*
	 * Eva_Care_OnboardPatient_238 Test case
	 * verifying regester device page fields are displaying or not
	 */
		public void OnboardPatient_238() throws Throwable
		{
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "onboardpatient");
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(regdecivebuttn, "clicking reg");
			WebActionUtil.sleep(2);
			boolean regdevicefiled=WebActionUtil.isElementDisplayedOrNot(regderegfield);
			System.out.println(regdevicefiled);
			for(int i=1;i<=5;i++)
			{
				 reg= regpagefields.get(i).getText();
				
			}
			if(regdevicefiled==true && reg.contains(reg))
			{
				
				WebActionUtil.logger.info("all regdevice fields are displayed");
				
			}
			else
			{
				WebActionUtil.logger.info("allregdevice fields are not  displayed");
			}
		}
		
		
		/*
		 * Eva_CCA_Onboard  Patient_239 test case
		 * Verifying device code is read only or not.
		 */
		
		public void OnboardPatient_241() throws Throwable
		{
			WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on board patient");
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(regdecivebuttn, "clicking regdevice");
			WebActionUtil.sleep(2);
			String regdataedi=regdevicecodeedit.getAttribute("ng-disabled");
			System.out.println(regdataedi);
			if(regdataedi.contains("readOnly"))
			{
				WebActionUtil.logger.info("device registaration code is read only");
			}else
			{
				WebActionUtil.logger.info("device registaration code is not read only");
			}
		}
}
