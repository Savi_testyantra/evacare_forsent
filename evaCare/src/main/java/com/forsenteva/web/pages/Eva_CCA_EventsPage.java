package com.forsenteva.web.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;


public class Eva_CCA_EventsPage extends BasePage{

	public Eva_CCA_EventsPage(WebDriver driver) {
		super(driver);
		
	}
	/*
	 * 7 th testcase web Elements
	 */
	@FindBy(xpath = "//span[contains(text(),' Events ')]")
	private  WebElement eventspage;
	@FindBy(xpath = "//div[@class='col-md-3']/h5[@class='text-center ng-binding']")
	private  WebElement currentmonth;
	@FindBy(xpath = "//div[@class='col-md-4 text-center btn-group']/button[contains(text(),'Previous')]")
	private  WebElement previousbtn;
	@FindBy(xpath = "//div[@class='col-md-4 text-center btn-group']/button[contains(text(),'Next')]")
	private  WebElement nextbtn;
	
	/*
	 * 14 testcase webelements
	 */
	@FindBy(xpath = "//div[@id='form-group-AddEvents.columns3']//span[text()='Add Event']")
	private  WebElement addevent;
	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Daily']")
	private  WebElement dailybtn;
	@FindBy(xpath = "//i[@class='glyphicon glyphicon-calendar'][1]")
	private  WebElement Startdatedate;
	@FindBy(xpath = "//i[@class='glyphicon glyphicon-calendar'][2]")
	private  WebElement enddatedate;
	@FindBy(xpath = "//div[@class='componentButtons col-xs-12']/button[contains(text(),'Save')]")
	private  WebElement saveevent;
	
	@FindBy(xpath = "//small[@class='cal-events-num badge badge-important pull-left ng-binding']")
	private  WebElement gettingevents_list;
	
	@FindBy(xpath = "//div[@id='mCSB_1_container']//span[@class='nav-title'][text()=' Events ']")
	private WebElement title;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[text()='Year']")
	private WebElement yearButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[text()='Month']")
	private WebElement monthButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[text()='Week']")
	private WebElement weekButton;

	@FindBy(xpath = "//button[text()='Day']")
	private WebElement dayButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents.columns3']//span[text()='Add Event']")
	private WebElement addEventButton;

	@FindBy(xpath = "//div[@class='addEventDialogcontrols']/input[@type='text']")
	private WebElement eventDialogTitleTextBox;

	@FindBy(xpath = "//div[@class='addEventDialogcontrols']//textarea[@placeholder='Enter the event description']")
	private WebElement eventDialogDescriptionTextBox;

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Once']")
	private WebElement eventOnceButton;

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Daily']")
	private WebElement eventDailyButton;

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Weekly']")
	private WebElement eventWeekButton;	

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Monthly']")
	private WebElement eventMonthlyButton;

	@FindBy(xpath = "//div[contains(@class,'removeHasError')]//input[@is-open='event.onceStartOpen']")
	private WebElement dateTextbox;

	@FindBy(xpath = "//div[contains(@class,'removeHasError')]//input[@is-open='event.dailyStartOpen']")
	private WebElement startDateTextbox;

	@FindBy(xpath = "//div[contains(@class,'removeHasError')]//input[@is-open='event.dailyEndOpen']")
	private WebElement endDateTextbox;
	

/*
 * 7th test case
 * verifying previous year,day,month,week calender is displaying or not
 */
public void Events_007() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(eventspage, "clicking on events page");
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(monthButton, "clicking on clicking on month btn");
	String cur_month=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on previous btn");
	String pre_month=currentmonth.getText();
	
	if(!cur_month.equals(pre_month)) {
		WebActionUtil.logger.info("previous month"+pre_month+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_month+" calender is displaying");
	}
	
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on next btn");
	WebActionUtil.clickOnElementUsingJS(yearButton, "clicking on year btn");
	String cur_year=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on previous btn");
	String pre_year=currentmonth.getText();
	
	
	if(!cur_year.equals(pre_year)) {
		WebActionUtil.logger.info("previous month"+pre_year+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_year+" calender is displaying");
	}
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on next btn");
	
	
	WebActionUtil.clickOnElementUsingJS(weekButton, "clicking on weekbtn btn");
	String cur_week=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on previous btn");
	String pre_week=currentmonth.getText();
	
	
	if(!cur_week.equals(pre_week)) {
		WebActionUtil.logger.info("previous month"+pre_week+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_week+" calender is displaying");
	}
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on next btn");
	
	WebActionUtil.clickOnElementUsingJS(dayButton, "clicking on weekbtn btn");
	String cur_day=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on previous btn");
	String pre_day=currentmonth.getText();
	
	
	if(!cur_day.equals(pre_day)) {
		WebActionUtil.logger.info("previous month"+pre_day+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_day+" calender is displaying");
	}
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on next btn");
}
/*
 * 8th test case
 * verifying next year,day,month,week calender is displaying or not
 */
public void Events_008() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(eventspage, "clicking on events page");
	WebActionUtil.sleep(1);
	WebActionUtil.clickOnElementUsingJS(monthButton, "clicking on clicking on month btn");
	String cur_month=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on previous btn");
	String nxt_month=currentmonth.getText();
	
	if(!cur_month.equals(nxt_month)) {
		WebActionUtil.logger.info("next month"+nxt_month+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_month+" calender is displaying");
	}
	
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on next btn");
	WebActionUtil.clickOnElementUsingJS(yearButton, "clicking on year btn");
	String cur_year=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on previous btn");
	String nxt_year=currentmonth.getText();
	
	
	if(!cur_year.equals(nxt_year)) {
		WebActionUtil.logger.info("next month"+nxt_year+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_year+" calender is displaying");
	}
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on next btn");
	
	
	WebActionUtil.clickOnElementUsingJS(weekButton, "clicking on weekbtn btn");
	String cur_week=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on previous btn");
	String nxt_week=currentmonth.getText();
	
	
	if(!cur_week.equals(nxt_week)) {
		WebActionUtil.logger.info("next month"+nxt_week+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_week+" calender is displaying");
	}
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on next btn");
	
	WebActionUtil.clickOnElementUsingJS(dayButton, "clicking on weekbtn btn");
	String cur_day=currentmonth.getText();
	
	WebActionUtil.clickOnElementUsingJS(nextbtn, "clicking on previous btn");
	String nxt_day=currentmonth.getText();
	
	
	if(!cur_day.equals(nxt_day)) {
		WebActionUtil.logger.info("previous month"+nxt_day+" calender is displaying");
	}else
	{
		WebActionUtil.logger.info("current month"+cur_day+" calender is displaying");
	}
	WebActionUtil.clickOnElementUsingJS(previousbtn, "clicking on next btn");
}	

public void Events_014() throws Throwable
{
WebActionUtil.clickOnElementUsingJS(eventspage, "clicking on events page");
WebActionUtil.sleep(3);
WebActionUtil.scrollToElement(addevent, "scrolling to element");
WebActionUtil.clickOnElementUsingJS(addevent, "clicking on add event");
WebActionUtil.clickOnElementUsingJS(dailybtn, "clicking on daily");
WebActionUtil.clickOnElementUsingJS(Startdatedate, "selecting start date");
WebActionUtil.handlingCalender(Startdatedate, "01", 10, 2019);
WebActionUtil.sleep(1);
WebActionUtil.handlingCalender(Startdatedate, "05", 10, 2019);
WebActionUtil.clickOnElementUsingJS(saveevent, "save event");
WebActionUtil.sleep(2);

WebActionUtil.scrollToElement(addevent, "scrolling to element");
WebActionUtil.clickOnElementUsingJS(addevent, "clicking on add event");
WebActionUtil.clickOnElementUsingJS(dailybtn, "clicking on daily");
WebActionUtil.clickOnElementUsingJS(Startdatedate, "selecting start date");
WebActionUtil.handlingCalender(Startdatedate, "01", 10, 2019);
WebActionUtil.sleep(1);
WebActionUtil.handlingCalender(Startdatedate, "07", 10, 2019);
WebActionUtil.clickOnElementUsingJS(saveevent, "save event");
WebActionUtil.sleep(2);

WebActionUtil.scrollToElement(addevent, "scrolling to element");
WebActionUtil.clickOnElementUsingJS(addevent, "clicking on add event");
WebActionUtil.clickOnElementUsingJS(dailybtn, "clicking on daily");
WebActionUtil.clickOnElementUsingJS(Startdatedate, "selecting start date");
WebActionUtil.handlingCalender(Startdatedate, "01", 10, 2019);
WebActionUtil.sleep(1);
WebActionUtil.handlingCalender(Startdatedate, "04", 10, 2019);
WebActionUtil.clickOnElementUsingJS(saveevent, "save event");
WebActionUtil.sleep(2);

List<WebElement> gridelements=driver.findElements(By.xpath("//small[@class='cal-events-num badge badge-important pull-left ng-binding']"));
for(WebElement griddata:gridelements) {

	 System.out.println(griddata.getText());
	
}
WebActionUtil.logger.info("user can view multiple events");
}

/*
 * 
 * verify page has navigated to events page
 */
public void Eva_CCA_Events_001() throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	Assert.assertEquals(WebActionUtil.isElementDisplayedOrNot(title), true);
}

/*
 * 
 * verify fields in events page
 */
public void Eva_CCA_Events_002() throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	try {
		WebActionUtil.isElementDisplayed(previousbtn, "", 1);
		WebActionUtil.isElementDisplayed(nextbtn, "", 1);
		WebActionUtil.isElementDisplayed(yearButton, "", 1);
		WebActionUtil.isElementDisplayed(monthButton, "", 1);
		WebActionUtil.isElementDisplayed(weekButton, "", 1);
		WebActionUtil.isElementDisplayed(dayButton, "", 1);
		WebActionUtil.isElementDisplayed(addEventButton, "", 1);
		WebActionUtil.logger.info("Successful to verify fields in events page");
	}
	catch(AssertionError a ) {
		WebActionUtil.logger.info("Failed to verify fields in events page");
	}
}

/*
 * 
 * verify user can select year based calander
 */
public void Eva_CCA_Events_003(String currentyear) throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	WebActionUtil.clickOnElementUsingJS(yearButton, "");
	boolean bool = WebActionUtil.driver.findElement(By.xpath("//div[@id='form-group-AddEvents']//h5[text()='" + currentyear + "']")).isDisplayed();
	Assert.assertEquals(bool, true);

	List<WebElement> months= WebActionUtil.driver.findElements(By.xpath("//div[contains(@class,'span3 col-md-3 col-xs-6 cal-cell')]"));
	for(WebElement element : months) {
		WebActionUtil.isElementDisplayed(element, "", 1);
	}
}

/*
 * 
 * verify user can select month based calander
 */
public void Eva_CCA_Events_004(String currentMonth) throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	WebActionUtil.clickOnElementUsingJS(monthButton, "");
	try {
		WebActionUtil.driver.findElement(By.xpath("//div[@id='form-group-AddEvents']//h5[contains(text(),'" + currentMonth +"')]"));
		List<WebElement> days = WebActionUtil.driver.findElements(By.xpath("//div[contains(@class,'cal-day-inmonth')]"));
		for(WebElement element:days) {
			WebActionUtil.isElementDisplayed(element, "", 1);
		}
		WebActionUtil.logger.info("month and all the days in a month are displayed");
	}
	catch(AssertionError a) {
		WebActionUtil.logger.info("Failed to display month and all the days in a month");
	}
}

/*
 * 
 * verify user can select week based calander
 */
public void Eva_CCA_Events_005() throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	WebActionUtil.clickOnElementUsingJS(weekButton, "");
	try {
		List<WebElement> week = WebActionUtil.driver.findElements(By.xpath("//div[@id='form-group-AddEvents']//div[contains(@class,'cal-cell')]"));
		for(WebElement element:week) {
			WebActionUtil.isElementDisplayed(element, "", 1);
			WebActionUtil.logger.info("All days are succesfully displayed for a week");
		}
	}
	catch(AssertionError a ) {
		WebActionUtil.logger.info("Failed to display all days for a week");
	}
}	

/*
 * 
 * verify fields in add event page
 */
public void Eva_CCA_Events_010() throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	WebActionUtil.clickOnElementUsingJS(addEventButton, "");
	WebActionUtil.sleep(6);
	WebActionUtil.isElementDisplayed(eventDialogTitleTextBox, "", 1);
	WebActionUtil.clickOnElementUsingJS(addEventButton, "");
	WebActionUtil.sleep(3);
	WebActionUtil.isElementDisplayed(eventDialogDescriptionTextBox, "", 1);
	WebActionUtil.isElementDisplayed(eventOnceButton, "", 1);
	WebActionUtil.isElementDisplayed(eventDailyButton, "", 1);
	WebActionUtil.isElementDisplayed(eventWeekButton, "", 1);
	WebActionUtil.isElementDisplayed(eventMonthlyButton, "", 1);	
}

/*
 * 
 * verify only one date is displayed is user select once option
 */
public void Eva_CCA_Events_011() throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	WebActionUtil.sleep(6);
	WebActionUtil.clickOnElementUsingJS(addEventButton, "");
	WebActionUtil.sleep(6);
	WebActionUtil.clickOnElementUsingJS(eventOnceButton, "");
	WebActionUtil.isElementDisplayed(dateTextbox, "", 1);
}

/*
 * 
 * verify both start date and end date is displayed for option daily/weekly/monthly
 */
public void Eva_CCA_Events_012() throws Throwable {
	Eva_CCA_OnboardPatientPage.navigationToEvents();
	WebActionUtil.clickOnElementUsingJS(addEventButton, "");
	WebActionUtil.sleep(6);
	WebActionUtil.clickOnElementUsingJS(eventDailyButton, "");
	WebActionUtil.isElementDisplayed(startDateTextbox, "", 1);
	WebActionUtil.isElementDisplayed(endDateTextbox, "", 1);

	WebActionUtil.clickOnElementUsingJS(eventWeekButton, "");
	//WebActionUtil.isElementDisplayed(startDateTextbox, "", 1);
	WebActionUtil.isElementDisplayed(endDateTextbox, "", 1);

	WebActionUtil.clickOnElementUsingJS(eventMonthlyButton, "");
	WebActionUtil.isElementDisplayed(startDateTextbox, "", 1);
	WebActionUtil.isElementDisplayed(endDateTextbox, "", 1);
}
	
	
	
}
