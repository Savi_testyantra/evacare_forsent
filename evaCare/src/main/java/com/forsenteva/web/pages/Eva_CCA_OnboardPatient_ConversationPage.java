package com.forsenteva.web.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.omg.CORBA.PRIVATE_MEMBER;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_ConversationPage extends BasePage {

	public Eva_CCA_OnboardPatient_ConversationPage(WebDriver driver) {
		super(driver);

	}

	// path for add new button in conversation page
	   @FindBy(xpath = "//button[@id='CommunicationForm.AddNew']")
	   private WebElement addNewButton;
	   
	   // path for media type radio button of response type.
	   @FindBy(xpath = "//input[@id='AnswerType-Media']")
	   private WebElement mediaResponsetype;

	@FindBy(xpath = "//div[@id='MediaType']/descendant::i[@class='caret pull-right']")
	private WebElement mediaTypedropDown;

	// path for media Image type dropdowm
	@FindBy(xpath = "//span[contains(text(),'Image')]")
	private WebElement mediaTypeImagedropDown;

	// path for media Audio type dropdowm
	@FindBy(xpath = "//span[contains(text(),'Audio')]")
	private WebElement mediatypeAudiodropdown;

	// path for media Audio type dropdowm
	@FindBy(xpath = "//div[@id='ui-select-choices-row-4-2']/descendant::span[text()='Video']")
	private WebElement mediatypeVideodropdown;

	// path for library radio button of upload type
	@FindBy(xpath = "(//*[@id=\"form-group-UploadTypeVideo\"]/div//input)[2]")
	private WebElement libraryUloadtype;

	// path for pick from media library button
	@FindBy(xpath = "//span[text()='Pick From Media Library']")
	private WebElement pickFromMediaLibraryButton;

	// path for Upload Type
	@FindBy(xpath = "//*[@id='form-group-UploadType']/descendant::input[@id='UploadType-googleAlbum']")
	private WebElement googlePhotto;

	// path of Get Alubum Button
	@FindBy(xpath = "//span[text()='Get Album']")
	private WebElement getAlbumButton;

	// Path for new window popup sign in
	@FindBy(xpath = "//div[@class='Xb9hP']/input")
	private WebElement googleSignInField;

	// Path for next Button
	@FindBy(xpath = "//div[@id='identifierNext']")
	private WebElement nextButton;

	// path for UploadType url radio button
	@FindBy(xpath = "//div[@class='ng-scope radio-inline']/label/input[@id='UploadTypeVideo-url']")
	private WebElement urlRadioButton;

	// path for UploadType url radio button
	@FindBy(xpath = "//input[@id='UploadTypeAudio-file']")
	private WebElement fileRadioButton;

	// path for UploadType Library radio button
	@FindBy(xpath = "//input[@id='UploadTypeAudio-library']")
	private WebElement libraryRadioButton;

	// path for URL field
	@FindBy(xpath = "//input[@id='URLImage']")
	private WebElement urlField;

	// Path for invalid URL message
	@FindBy(xpath = "//p[contains(text(),'Please Give a Valid URL')]")
	private WebElement invalidUrlMessage;

	// Path for File Upload URL field
	@FindBy(xpath = "//div[@id='uploadIMG.file']")
	private WebElement fileuploadUrlField;

	// path for Browsing Mp3 song
	@FindBy(xpath = "//div[@id='CommunicationForm.file']/descendant::b[text()='Browse']")
	private WebElement browseLink;

	// path for Media Library Button
	@FindBy(xpath = "//span[text()='Pick From Media Library']")
	private WebElement mediaLibraryButton;

	// Path for Pic Audio window popup
	@FindBy(xpath = "//div[@class='col-md-12 col-sm-12 col-xs-12 componentleftTitle']/descendant::label[text()='Pick audio ']")
	private WebElement picAudioWindow;
	
	// path for video option in media type dropdown
	@FindBy(xpath = "//li[@id='ui-select-choices-4']/descendant::span[text()='Video']")
	private WebElement video;
	// path for pick video pop up
	@FindBy(xpath = "//div[@class='ngdialog-content url-picker']")
	private WebElement pickVideoPopUp;

	// Alert Info Fields
	// path for alert info check box
	@FindBy(id = "AlertInfo")
	private WebElement AlertInfo;
	// Path for Repeated More Than(Times) field
	@FindBy(xpath = "//input[@id='RepeatedMoreThanTimes']")
	private WebElement repeatedMoreThanTimes;
	// Path for Within field
	@FindBy(xpath = "//div[@id='Within']")
	private WebElement withIn;
	// Path for Trigger ) field
	@FindBy(xpath = "//div[@id='Trigger']")
	private WebElement trigger;
	// Path for Email field
	@FindBy(xpath = "//input[@id='Email']")
	private WebElement email;
	// path of error message for Repeated More Than(Times) field
	@FindBy(xpath = "//div[@id='form-group-AlertInformation']/descendant::p[text()='Repeated More Than(Times) : must be a number.']")
	private WebElement errRepeatedMoreThanTimes;
		@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	@FindBy(xpath = "//button[@id='PatientsList.Conversation-0-9']")
	private WebElement coversation;
	@FindBy(xpath = "//div[@id='formRender']/descendant::button[@id='CommunicationForm.AddNew']")
	private WebElement converse_Addnew;
	@FindBy(xpath = "//label[@class='control-label ng-binding']/input[@id='AnswerType-Media']")
	private WebElement restype_media;
	@FindBy(xpath = "//div[@id='MediaType']//span[@class='ui-select-match-text pull-left']")
	private WebElement mediatype;
	@FindBy(xpath = "//div[@class='ui-select-choices-row ng-scope active']/span[@class='ui-select-choices-row-inner']")
	private WebElement image_dropdown;
	@FindBy(xpath = "//div[@class='input-group leftLayoutControl']/input[@id='URLImage']")
	private WebElement image_url;
	@FindBy(xpath = "//div[@class='ng-scope']//p[contains(text(),'Please Give a Valid URL')]")
	private WebElement urlvalidation;
	@FindBy(xpath = "//div[@class='ng-scope radio-inline']//input[@id='UploadType-file']")
	private WebElement radiobuttonfile;
	@FindBy(xpath = "//a[@class='btn btn-sm btn-default ng-scope']/span[@class='glyphicon glyphicon-remove']")
	private WebElement removeadded_file;
	@FindBy(xpath = "//div[@id='Mood']//span[text()='Please select']")
	private WebElement moodDropdown;
	@FindBy(xpath = "//div[@id='Subjects']//input")
	private WebElement subjectTextbox;
	@FindBy(xpath = "//div[@id='form-group-Subjects']//p[text()='Please Select Subject']")
	private WebElement errorMessage;
	@FindBy(xpath = "//div[@id='form-group-Submit']/descendant::button[@id='Submit']")
	private WebElement submitButton;
	@FindBy(xpath = "//div[@id='form-group-AnswerType']//input[@id='AnswerType-Answer']")
	private WebElement answerRadiobutton;
	@FindBy(xpath = "//div[@id='form-group-Answer']//textarea[@id='Answer']")
	private WebElement answerTextArea;
	@FindBy(xpath = "//div[@id='form-group-AnswerType']//input[@id='AnswerType-Media']")
	private WebElement mediaRadiobutton;
	// path for image upload Browse button
	  @FindBy(xpath = "//div[@id='CommunicationForm.file']/a")
	    private WebElement imageBroseButton;
	// Path for remove option after image upload
	    @FindBy(xpath = "//span[@class='glyphicon glyphicon-remove']")
	    private WebElement removeOption;
	 // Path for Answer typre Response Radio button
	    @FindBy(xpath = "//input[@id='AnswerType-Answer']")
	    private WebElement answerRadioButton;
	    
		@FindBy(xpath = "//div[@id='form-group-CommunicationForm.AddNew']//button[@id='CommunicationForm.AddNew']")
		private WebElement conversationAddNewButton;
	// Pick from personalized Media Library Button with Disabled URL field is
	// displaying

	// Method for Test case ID Eva_CCA_Onboard PatientPage _168_ Verifying the
	// presence of Video in the Media Type drop down and view the Presence of URL
	// field with valid URL
	
		// selecting image in dropdown
		@FindBy(xpath = "//div[@id='ui-select-choices-row-4-0']/span[@class='ui-select-choices-row-inner']")
		private WebElement imagedropdown;

		// Path for Image File radio button at Conversation Page
		@FindBy(xpath = "//input[@id='UploadType-file']")
		private WebElement imageFileUpload;

		// Path for upload file option visibility
		@FindBy(xpath = "//div[@id='form-group-uploadIMG.file']")
		private WebElement uploadFiletext;

		// Path for Image Browse button at Conversation Page
		@FindBy(xpath = "//div[@id='uploadIMG.file']/a")
		private WebElement browseBtn;


	public void verifyVideoTypeAndOptionsURL(String url) {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeVideodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeVideodropdown, "mediaResponsetype");
			WebActionUtil.sleep(3);
			WebActionUtil.scrollToElement(urlRadioButton, "urlRadioButton");
			WebActionUtil.sleep(2);
			WebActionUtil.isElementDisplayedOrNot(urlRadioButton);
			// WebActionUtil.isElementDisplayed(urlRadioButton, "urlRadioButton", 2);
//				WebActionUtil.isElementDisplayed(fileRadioButton, "fileRadioButton", 1);
//				WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(urlRadioButton, "urlRadioButton");
			WebActionUtil.isElementDisplayed(urlField, "urlField", 1);
			WebActionUtil.typeText(urlField, url, "UrlField");

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _117 Verify after upload
    // the file into Upload Image field
    public void verifyRemoveOptionAfterFileUpload() {
        try {
            InitializePages.OnboardPatient.navigateToAddConversation();
            WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
            WebActionUtil.sleep(1);
            WebActionUtil.clickOnElement(imageBroseButton, "imageBroseButton");
            webActionUtil.uploadFileWithRobot("D:\\09062019\\evaCare\\Image\\maruti-swift-Image.jpg");
            WebActionUtil.isElementDisplayed(removeOption, "RemoveOption", 1);
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /*
	 * verify user can enter data into mood field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_101(String mood) throws Throwable {
		InitializePages.OnboardPatient.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "");

		try {
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mood + "']")).click();

			String value= WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + mood + "']")).getText();
			Assert.assertEquals(value, mood);
			WebActionUtil.logger.info("mood value is succesfully selected");
		}
		catch( AssertionError a) {
			WebActionUtil.logger.info("Failed to select mood value");
		}
	}

     // Method for Test case ID Eva_CCA_Onboard PatientPage _112 to Verify the Action
    // Radio button from Response type
    public void Eva_CCA_OnboardPatient_112() {
        try {
            InitializePages.OnboardPatient.navigateToAddConversation();
            WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
            WebActionUtil.clickOnElement(answerRadioButton, "answerRadioButton");
            boolean radioBtn = answerRadioButton.isSelected();
            System.out.println(radioBtn);
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /*
     * Worked on 26-09-2019
     */
     // Method for Test case ID Eva_CCA_Onboard PatientPage _118 to verify different
    // Image Upload
    public void Eva_CCA_OnboardPatient_118() {
        try {
            InitializePages.OnboardPatient.navigateToAddConversation();
            WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
            WebActionUtil.sleep(1);
            WebActionUtil.clickOnElement(imageBroseButton, "imageBroseButton");
            webActionUtil.uploadFileWithRobot("D:\\09062019\\evaCare\\Image\\maruti-swift-Image.jpg");
            WebActionUtil.isElementDisplayed(removeOption, "RemoveOption", 1);
            WebActionUtil.clickOnElement(removeOption, "removeOption");
            WebActionUtil.sleep(1);
            WebActionUtil.clickOnElement(imageBroseButton, "imageBroseButton");
            webActionUtil.uploadFileWithRobot("D:\\09062019\\evaCare\\Image\\PNG File.png");
            WebActionUtil.isElementDisplayed(removeOption, "RemoveOption", 1);
        } catch (Throwable e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
	public void Eva_CCA_Onboard_Patient_169(String mediatype, String attributeName, String attributeValue) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "Media radio button");
			WebActionUtil.clickOnElementUsingJS(mediaTypedropDown, "Media type drop down");
			WebActionUtil.sleep(3);
			WebActionUtil.scrollToElement(video, "video");
			WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mediatype + "']")).click();
			WebActionUtil.clickOnElementUsingJS(libraryUloadtype, "Library upload type");
			WebActionUtil.isElementDisplayed(pickFromMediaLibraryButton, "Pick From Library Button", 1);
			WebActionUtil.verifyAttributeValue(urlField, attributeName, attributeValue);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// verifying the Video pop-up is displayed
	public void Eva_CCA_Onboard_Patient_170(String mediatype) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "Media radio button");
			WebActionUtil.clickOnElementUsingJS(mediaTypedropDown, "Media type drop down");
			WebActionUtil.sleep(3);
			WebActionUtil.scrollToElement(video, "video");
			WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mediatype + "']")).click();
			WebActionUtil.clickOnElementUsingJS(libraryUloadtype, "Library upload type");
			WebActionUtil.isElementDisplayed(pickFromMediaLibraryButton, "Pick From Library Button", 1);
			WebActionUtil.clickOnElementUsingJS(pickFromMediaLibraryButton, "pickFromMediaLibraryButton");
			WebActionUtil.isElementDisplayed(pickVideoPopUp, "pickVideoPopUp", 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Verify the elements on pick video popup
	public void Eva_CCA_Onboard_Patient_171(String mediatype) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "Media radio button");
			WebActionUtil.clickOnElementUsingJS(mediaTypedropDown, "Media type drop down");
			WebActionUtil.sleep(3);
			WebActionUtil.scrollToElement(video, "video");
			WebActionUtil.driver
					.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mediatype + "']")).click();
			WebActionUtil.clickOnElementUsingJS(libraryUloadtype, "Library upload type");
			WebActionUtil.isElementDisplayed(pickFromMediaLibraryButton, "Pick From Library Button", 1);
			WebActionUtil.clickOnElementUsingJS(pickFromMediaLibraryButton, "pickFromMediaLibraryButton");
			WebActionUtil.isElementDisplayed(pickVideoPopUp, "pickVideoPopUp", 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Verify the Alert information field
	public void Eva_CCA_Onboard_Patient_175() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.isElementDisplayed(repeatedMoreThanTimes, "repeatedMoreThanTimes", 1);
			WebActionUtil.isElementDisplayed(withIn, "withIn", 1);
			WebActionUtil.isElementDisplayed(trigger, "trigger", 1);
			WebActionUtil.isElementDisplayed(email, "email", 1);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Verify the Alert information fields are read only when alert info check box is unchecked
	public void Eva_CCA_Onboard_Patient_176() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.verifyAttributeValue(repeatedMoreThanTimes, "disabled", "true");
			WebActionUtil.verifyAttributeValue(withIn, "disabled", "true");
			WebActionUtil.verifyAttributeValue(trigger, "disabled", "true");
			WebActionUtil.verifyAttributeValue(email, "disabled", "true");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Verify the Alert information fields are enabled when alert info check box is
	// checked
	public void Eva_CCA_Onboard_Patient_177() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.clickOnElementUsingJS(AlertInfo, "AlertInfo");
			WebActionUtil.isElementEnabled(repeatedMoreThanTimes);
			WebActionUtil.isElementEnabled(withIn);
			WebActionUtil.isElementEnabled(trigger);
			WebActionUtil.isElementEnabled(email);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Verify Repeated More Than(Times) field is number field
	public void Eva_CCA_Onboard_Patient_178(String input) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.clickOnElementUsingJS(AlertInfo, "AlertInfo");
			WebActionUtil.isElementEnabled(repeatedMoreThanTimes);
			WebActionUtil.isElementEnabled(withIn);
			WebActionUtil.isElementEnabled(trigger);
			WebActionUtil.isElementEnabled(email);
			WebActionUtil.typeText(repeatedMoreThanTimes, input, "RepeatedMoreThanTimes");
			WebActionUtil.sleep(5);
			WebActionUtil.isElementDisplayed(errRepeatedMoreThanTimes, "ErrRepeatedMoreThanTimes", 1);
			System.out.println(WebActionUtil.getText(errRepeatedMoreThanTimes, "ErrRepeatedMoreThanTimes"));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Verify Within field drop down list
	public void Eva_CCA_Onboard_Patient_180() {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.clickOnElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementEnabled(repeatedMoreThanTimes);
			WebActionUtil.isElementEnabled(withIn);
			WebActionUtil.isElementEnabled(trigger);
			WebActionUtil.isElementEnabled(email);
			WebActionUtil.clickOnElement(withIn, "WithIn");
			List<WebElement> withinElements = driver
					.findElements(By.xpath("//span[@class='ui-select-choices-row-inner']"));
			List<String> list = new ArrayList<>();
			list.add("Please select");
			list.add("Hour");
			list.add("Day");
			list.add("Week");
			list.add("Month");
			List<String> lst = new ArrayList<>();
			for (int i = 0; i <= withinElements.size() - 1; i++) {
				String val = withinElements.get(i).getText();
				lst.add(val);
			}
			if (list.equals(lst)) {
				System.out.println("Within' field drop down displaying Please select, Hour, Day, Week and Month");
			} else {
				System.out.println("Within' field drop down is not displaying the expected list of elements");
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	//Validate selected data is displaying or not in within' field
	public void Eva_CCA_Onboard_Patient_182(String withinOption, String expected) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.clickOnElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementEnabled(repeatedMoreThanTimes);
			WebActionUtil.isElementEnabled(withIn);
			WebActionUtil.isElementEnabled(trigger);
			WebActionUtil.isElementEnabled(email);
			WebActionUtil.clickOnElement(withIn, "WithIn");
			WebActionUtil.driver
			.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + withinOption + "']")).click();
			String actual=WebActionUtil.getText(withIn, "Day");
			Assert.assertEquals(actual, expected);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	//Verify within' field should not allow multiple select
	public void Eva_CCA_Onboard_Patient_183(String withinDay, String withinMonth, String expected) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.clickOnElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementEnabled(repeatedMoreThanTimes);
			WebActionUtil.isElementEnabled(withIn);
			WebActionUtil.isElementEnabled(trigger);
			WebActionUtil.isElementEnabled(email);
			WebActionUtil.clickOnElement(withIn, "WithIn");
			WebActionUtil.driver
			.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + withinDay + "']")).click();
			WebActionUtil.clickOnElement(withIn, "WithIn");
			WebActionUtil.driver
			.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + withinMonth + "']")).click();
			String actual=WebActionUtil.getText(withIn, "Month");
			Assert.assertNotEquals(actual, expected);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	//Verify Trigger field is drop down and also drop down list
	public void Eva_CCA_Onboard_Patient_184(String expected) {
		try {
			WebActionUtil.waitTillPageLoad(driver, 10);
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "Add new");
			WebActionUtil.scrollToElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementDisplayed(AlertInfo, "AlertInfo", 1);
			System.out.println("Alert info check box is selected: " + AlertInfo.isSelected());
			WebActionUtil.clickOnElement(AlertInfo, "AlertInfo");
			WebActionUtil.isElementEnabled(repeatedMoreThanTimes);
			WebActionUtil.isElementEnabled(withIn);
			WebActionUtil.isElementEnabled(trigger);
			WebActionUtil.isElementEnabled(email);
			String actual = WebActionUtil.getText(trigger, "Trigger");
			Assert.assertEquals(actual, expected);
			WebActionUtil.clickOnElement(trigger, "Trigger");
			List<WebElement> triggerElements = driver
					.findElements(By.xpath("//span[@class='ui-select-choices-row-inner']"));
			List<String> list = new ArrayList<>();
			list.add("Email");
			list.add("SMS");
			List<String> lst = new ArrayList<>();
			for (int i = 0; i <= triggerElements.size() - 1; i++) {
				String val = triggerElements.get(i).getText();
				lst.add(val);
			}
			if (list.equals(lst)) {
				System.out.println("Trigger field is a drop down and displaying Email and SMS");
			} else {
				System.out.println("Trigger field is not a drop down");
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	// Method for Test case ID Eva_CCA_Onboard PatientPage _149_ For Verifying Get
	// Album Button

	public void verifyGetAlbumButton() {
		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElementUsingJS(mediaTypeImagedropDown, "mediaResponsetype");
			WebActionUtil.clickOnElementUsingJS(googlePhotto, "googlePhotto");
			WebActionUtil.scrollToElement(getAlbumButton, "getAlbumButton");
			WebActionUtil.sleep(3);
			boolean getbtn = webActionUtil.isElementDisplayedOrNot(getAlbumButton);
			System.out.println(getbtn);

		} catch (Throwable e) {

			e.printStackTrace();
		}

	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _150_ For Verifying
	// Google sign in Popup after clicking on Get Album button

	public void verifyGoogleSigninpage() {
		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.sleep(3);
			WebActionUtil.clickOnElementUsingJS(mediaTypeImagedropDown, "mediaResponsetype");
			WebActionUtil.clickOnElementUsingJS(googlePhotto, "googlePhotto");
			WebActionUtil.scrollToElement(getAlbumButton, "getAlbumButton");
			WebActionUtil.sleep(3);
			boolean getbtn = webActionUtil.isElementDisplayedOrNot(getAlbumButton);
			System.out.println(getbtn);
			String winHandleBefore = driver.getWindowHandle();

			getAlbumButton.click();
			WebActionUtil.sleep(4);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				System.out.println("Title of the new window: " + driver.getTitle());
				boolean browser = driver.getTitle().equals("Sign in – Google accounts");
				if (browser) {
					System.out.println("As Bhavana said I need to write Correctly");
				}
			}

		} catch (Throwable e) {

			e.printStackTrace();
		}

	}
	// Method for Test case ID Eva_CCA_Onboard PatientPage _151_ For Verifying
	// Google sign in for photo upload after clicking on get Album button

	public void verifyGoogleSigninForGooglePhoto(String email) {
		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.sleep(1);
			WebActionUtil.clickOnElementUsingJS(mediaTypeImagedropDown, "mediaResponsetype");
			WebActionUtil.clickOnElementUsingJS(googlePhotto, "googlePhotto");
			WebActionUtil.scrollToElement(getAlbumButton, "getAlbumButton");
			WebActionUtil.sleep(3);
			boolean getbtn = webActionUtil.isElementDisplayedOrNot(getAlbumButton);
			System.out.println(getbtn);
			String winHandleBefore = driver.getWindowHandle();

			getAlbumButton.click();
			WebActionUtil.sleep(4);

			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);
				System.out.println("Title of the new window: " + driver.getTitle());
				boolean browser = driver.getTitle().equals("Sign in – Google accounts");
				if (browser) {
					System.out.println("Sign in Google account opended with new window");
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					WebActionUtil.sleep(5);
					WebActionUtil.typeText(googleSignInField, email, "elementName");
					WebActionUtil.sleep(2);
					WebActionUtil.clickOnElementUsingJS(nextButton, "nextButton");

				}
			}

		} catch (Throwable e) {

			e.printStackTrace();
		}

	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _154_ For Verifying
	// Audio type and other Radio button options

	public void verifyAudioTypeAndOptions() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");

			WebActionUtil.isElementDisplayed(urlRadioButton, "urlRadioButton", 1);
			WebActionUtil.isElementDisplayed(fileRadioButton, "fileRadioButton", 1);
			WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _155_ For Verifying
	// Audio type and URL Field

	public void verifyUrlField() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");

			WebActionUtil.isElementDisplayed(urlRadioButton, "urlRadioButton", 1);
//					WebActionUtil.isElementDisplayed(fileRadioButton, "fileRadioButton", 1);
//					WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
			WebActionUtil.clickOnElement(urlRadioButton, "urlRadioButton");
			WebActionUtil.isElementDisplayed(urlField, "urlField", 1);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _156_ For Verifying
	// Audio type and verify with invalid url format should through error message

	public void verifyInvalidUrlMessage(String url) {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");

			WebActionUtil.isElementDisplayed(urlRadioButton, "urlRadioButton", 1);
//					WebActionUtil.isElementDisplayed(fileRadioButton, "fileRadioButton", 1);
//					WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
			WebActionUtil.clickOnElement(urlRadioButton, "urlRadioButton");
			WebActionUtil.isElementDisplayed(urlField, "urlField", 1);
			WebActionUtil.typeText(urlField, url, "invalidUrl");
			urlField.sendKeys(Keys.TAB);
			WebActionUtil.sleep(1);
			// WebActionUtil.isElementDisplayed(invalidUrlMessage, url, 1);
			if (invalidUrlMessage.isDisplayed()) {
				System.out.println("Invalid URL Format" + " " + invalidUrlMessage.getText());
			} else {
				System.out.println("Valid URL Format");
			}

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _157_ For Verifying
	// Audio type and verify with Valid url format should not through error message

	public void verifyValidUrl(String url) {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");

			WebActionUtil.isElementDisplayed(urlRadioButton, "urlRadioButton", 1);
			WebActionUtil.clickOnElement(urlRadioButton, "urlRadioButton");
			WebActionUtil.isElementDisplayed(urlField, "urlField", 1);
			WebActionUtil.typeText(urlField, url, "invalidUrl");
			urlField.sendKeys(Keys.TAB);
			WebActionUtil.sleep(1);
			if (invalidUrlMessage.isDisplayed()) {
				System.out.println("Invalid URL Format" + " " + invalidUrlMessage.getText());
			} else {
				System.out.println("Valid URL Format");
			}

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// Method for Test case ID Eva_CCA_Onboard PatientPage _158_ For Verifying
	// Audio type and File Upload URL Field

	public void verifyFileUploadUrlField() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");
			WebActionUtil.isElementDisplayed(fileRadioButton, "fileRadioButton", 1);
			WebActionUtil.clickOnElement(fileRadioButton, "fileRadioButton");
			WebActionUtil.isElementDisplayed(fileuploadUrlField, "fileuploadUrlField", 1);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _159_ For Verifying
	// Audio type and MP3 File Upload URL Field

	public void verifyMp3FileUpload() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");
			WebActionUtil.isElementDisplayed(fileRadioButton, "fileRadioButton", 1);
			WebActionUtil.clickOnElement(fileRadioButton, "fileRadioButton");
			WebActionUtil.isElementDisplayed(fileuploadUrlField, "fileuploadUrlField", 1);
			WebActionUtil.clickOnElement(browseLink, "browseLink");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _160_ For Verifying
	// Audio type and verify Media Library Button

	public void verifyMediaLibraryButton() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");
			WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
			WebActionUtil.clickOnElement(libraryRadioButton, "libraryRadioButton");
			WebActionUtil.isElementDisplayed(mediaLibraryButton, "mediaLibraryButton", 1);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _161_ For Verifying
	// Audio type and verify Media Library Popup

	public void verifyPicAudioMediaLibraryPopup() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");
			WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
			WebActionUtil.clickOnElement(libraryRadioButton, "libraryRadioButton");
			WebActionUtil.isElementDisplayed(mediaLibraryButton, "mediaLibraryButton", 1);
			WebActionUtil.clickOnElement(mediaLibraryButton, "mediaLibraryButton");
			WebActionUtil.sleep(2);
			WebActionUtil.isElementDisplayed(picAudioWindow, "picAudioWindow", 1);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _162_ For Verifying
	// Audio type and verify Media Library Popup

	public void verifyAudioMediaLibraryPopupOptions() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeAudiodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeAudiodropdown, "mediaResponsetype");
			WebActionUtil.isElementDisplayed(libraryRadioButton, "libraryRadioButton", 1);
			WebActionUtil.clickOnElement(libraryRadioButton, "libraryRadioButton");
			WebActionUtil.isElementDisplayed(mediaLibraryButton, "mediaLibraryButton", 1);
			WebActionUtil.clickOnElement(mediaLibraryButton, "mediaLibraryButton");
			WebActionUtil.sleep(2);
			WebActionUtil.isElementDisplayed(picAudioWindow, "picAudioWindow", 1);

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Method for Test case ID Eva_CCA_Onboard PatientPage _165_ Verifying the
	// presence of
	// Video in the Media Type drop down and view the Presence of URL field

	public void verifyVideoTypeAndOptions() {

		try {
			InitializePages.OnboardPatient.navigateToAddConversation();
			WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
			WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
			WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
			WebActionUtil.clickOnElement(mediaTypedropDown, "MediaDropDown");
			WebActionUtil.sleep(2);
			WebActionUtil.scrollToElement(mediatypeVideodropdown, "audioDropDown");
			WebActionUtil.clickOnElementUsingJS(mediatypeVideodropdown, "mediaResponsetype");
			WebActionUtil.sleep(3);
			WebActionUtil.scrollToElement(urlRadioButton, "urlRadioButton");
			WebActionUtil.sleep(2);
			WebActionUtil.isElementDisplayedOrNot(urlRadioButton);
			WebActionUtil.sleep(2);
			WebActionUtil.clickOnElementUsingJS(urlRadioButton, "urlRadioButton");
			WebActionUtil.isElementDisplayed(urlField, "urlField", 1);

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	 * verifying the url having validation message is there or not in conversation page level  
	 */
	
	public void OnboardPatient_144() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onboard patient");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(coversation, "clicking on conversation");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(converse_Addnew,"clicking on addnew button in conversation");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(restype_media, "click on restotype media");
		WebActionUtil.sleep(1);
		WebActionUtil.scrollToElement(mediatype, "scroling to media type");
		WebActionUtil.clickOnElementUsingJS(mediatype, "clicking on media type");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(image_dropdown, "selecting image");
		WebActionUtil.sleep(1);
		WebActionUtil.typeText(image_url, "google", "typing url");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(1);
		String urlvalidation1=WebActionUtil.getText(urlvalidation,"getting validation message");
		Assert.assertEquals(urlvalidation1, "Please Give a Valid URL");
		WebActionUtil.logger.info("conversation page image url have proper validation as Please Give a Valid URL");
	
	}
	/*
	 * testcase 147
	 * verifying user can able to upload a image file or not
	 */
	public void OnboardPatient_147() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onboard patient");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(coversation, "clicking on conversation");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(converse_Addnew,"clicking on addnew button in conversation");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(restype_media, "click on restotype media");
		WebActionUtil.sleep(1);
		WebActionUtil.scrollToElement(mediatype, "scroling to media type");
		WebActionUtil.clickOnElementUsingJS(mediatype, "clicking on media type");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(image_dropdown, "selecting image");
		WebActionUtil.sleep(1);
		
		WebActionUtil.clickOnElementUsingJS(radiobuttonfile, "click on file button");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElement(browseLink, "Browse link");
		
		StringSelection stringSelection = new StringSelection("C:\\Users\\Tulasi\\Desktop\\rose.png");
	       Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	       clipboard.setContents(stringSelection, null);
	       Robot robot = null;
	       try {
	           robot = new Robot();
	       } catch (AWTException e) {
	           e.printStackTrace();
	       }
	       robot.delay(250);
	       robot.keyPress(KeyEvent.VK_ENTER);
	       robot.keyRelease(KeyEvent.VK_ENTER);
	       robot.keyPress(KeyEvent.VK_CONTROL);
	       robot.keyPress(KeyEvent.VK_V);
	       robot.keyRelease(KeyEvent.VK_V);
	       robot.keyRelease(KeyEvent.VK_CONTROL);
	       robot.keyPress(KeyEvent.VK_ENTER);
	       robot.delay(150);
	       robot.keyRelease(KeyEvent.VK_ENTER);
	       WebActionUtil.sleep(4);
		
		boolean uploadedfile=WebActionUtil.isElementDisplayedOrNot(removeadded_file);
		if(uploadedfile==true) {
			WebActionUtil.logger.info("user can able upload a image file");
			
		}
		
	}
	/*
	 * verify  mood field in add conversation page
	 * 
	 * 
	 */
	public void Eva_CCA_OnboardPatient_100() throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
		WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope']"));
	}

	/*
	 * verify user can enter data into mood field
	 * 
	 * 
	 */
	public void Eva_CCA_OnboardPatient_101(String mood) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "");

		try {
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mood + "']")).click();

			String value= WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + mood + "']")).getText();
			Assert.assertEquals(value, mood);
			WebActionUtil.logger.info("mood value is succesfully selected");
		}
		catch( AssertionError a) {
			WebActionUtil.logger.info("Failed to select mood value");
		}
	}

	/*
	 * validate selected data is displaying or not
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_102(String moodValue1,String moodValue2) throws Throwable{
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
		try {
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + moodValue1 + "']")).click();

			String actualValue = WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + moodValue1 + "']")).getText();
			Assert.assertEquals(actualValue, moodValue1);

			WebActionUtil.clickOnElementUsingJS(moodDropdown, "");
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + moodValue1 + "']")).click();

			String actualValue2 = WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + moodValue2 + "']")).getText();
			Assert.assertEquals(actualValue2.contains(actualValue), false);
			WebActionUtil.logger.info("mood value is single value select");
		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("mood value is not single value select");

		}
	}

	/*
	 * validate multiple selection in the mood field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_103() throws Throwable{
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");

		WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
		List<WebElement> subjects = WebActionUtil.driver.findElements(By.xpath("//span[@class='ui-select-choices-row-inner']"));

		try {
			for(WebElement element:subjects) {
				WebActionUtil.isElementDisplayed(element, "", 1);
				WebActionUtil.logger.info("subject dropdown is multi value select");
			}
		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("subject dropdown is not multi value select");
		}
	}

	/*
	 * verify entering data into subject field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_105(String value1, String value2) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");

		WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
		WebActionUtil.clearAndTypeText(subjectTextbox, value1, "");
		try {
			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + value1 + "']")).click();
			WebElement subjectElement = WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + value1 + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);

			WebActionUtil.typeText(subjectTextbox, value2, "");
			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + value2 + "']")).click();


			WebElement subject2=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + value2 + "']"));
			Assert.assertEquals(subject2.isDisplayed(), true);


			WebActionUtil.logger.info("subject dropdown is validated");

		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("Could not able to validate subject dropdown");
		}
	}

	/*
	 * validate multiple selection in subject field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_106(String subject) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");

		WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
		WebActionUtil.clearAndTypeText(subjectTextbox, subject, "");

		try {
			WebActionUtil.logger.info("checking whether selected element is displayed");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject + "']")).click();
			WebElement subjectElement=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + subject + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);
			WebActionUtil.logger.info("Could not able to validate subject dropdown");

		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("Could not able to validate subject dropdown");

		}
	}

	/*
	 * validate selected data is displaying or not
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_107(String subject1,String subject2, String subject3)  throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");

		WebActionUtil.logger.info("checking whether selected elements is displayed");
		try {
			// first entry
			WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
			WebActionUtil.clearAndTypeText(subjectTextbox, subject1, "");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject1 + "']")).click();
			WebElement subjectElement=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + subject1 + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);


			// second entry
			WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
			WebActionUtil.clearAndTypeText(subjectTextbox, subject2, "");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject2 + "']")).click();
			WebElement subjectElement1=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + subject2 + "']"));
			Assert.assertEquals(subjectElement1.isDisplayed(), true);

			// third entry
			WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
			WebActionUtil.clearAndTypeText(subjectTextbox, subject3, "");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject3 + "']")).click();
			WebElement subjectElement2=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + subject3 + "']"));
			Assert.assertEquals(subjectElement2.isDisplayed(), true);
			WebActionUtil.logger.info("selected elements is displayed succesfully");

			List<WebElement> removeElements = WebActionUtil.driver.findElements(By.xpath("//div[@id='Subjects']//span[contains(@class,' ui-select-match-close')]"));
			for(WebElement element : removeElements) {
				Assert.assertEquals(WebActionUtil.isElementDisplayedOrNot(element), true);
			}

			WebActionUtil.logger.info("remove elements for added data is available");
		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("remove elements for added data is not available");
		}
	}

	/*
	 * validate remove data from subject field
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_108(String subject1,String subject2, String subject3)  throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");

		WebActionUtil.logger.info("checking whether selected elements is displayed");
		try {
			// first entry
			WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
			WebActionUtil.clearAndTypeText(subjectTextbox, subject1, "");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject1 + "']")).click();
			WebElement subjectElement=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + subject1 + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);


			// second entry
			WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
			WebActionUtil.clearAndTypeText(subjectTextbox, subject2, "");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject2 + "']")).click();
			WebElement subjectElement1=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + subject2 + "']"));
			Assert.assertEquals(subjectElement1.isDisplayed(), true);

			// third entry
			WebActionUtil.clickOnElementUsingJS(subjectTextbox, "");
			WebActionUtil.clearAndTypeText(subjectTextbox, subject3, "");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + subject3 + "']")).click();
			WebElement subjectElement2=WebActionUtil.driver.findElement(By.xpath("//div[@id='Subjects']//span[text()='" + subject3 + "']"));
			Assert.assertEquals(subjectElement2.isDisplayed(), true);
			WebActionUtil.logger.info("selected elements is displayed succesfully");
			List<WebElement> removeElements = WebActionUtil.driver.findElements(By.xpath("//div[@id='Subjects']//span[contains(@class,' ui-select-match-close')]"));

			for(WebElement element : removeElements) {
				WebActionUtil.clickOnElementUsingJS(element, "remove element");
			}

			Assert.assertEquals(WebActionUtil.isElementDisplayedOrNot(errorMessage), true);
			WebActionUtil.isElementNotEnabled(submitButton);
			WebActionUtil.logger.info("Succesful in displaying message when user remove all selected data");
		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("failed in displaying message when user remove all selected data");
		}
	}

	/*
	 * verify user can remove all data from subject field or not
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_109() throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");
		WebActionUtil.logger.info("checking both elements of response field are radio button");
		try {
			String typeAtt = answerRadiobutton.getAttribute("type");
			Assert.assertEquals(typeAtt, "radio");

			String mediaAtt = mediaRadiobutton.getAttribute("type");
			Assert.assertEquals(mediaAtt, "radio");
			Assert.assertEquals(answerRadiobutton.isSelected(), true);
			WebActionUtil.logger.info("successful in verifying response field");

		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("Failed in verifying response field");
		}
	}

	/*
	 * verify the response type field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_110() throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");
		String attribtValue = answerRadiobutton.getAttribute("type");
		System.out.println(attribtValue);
		try {
			Assert.assertEquals(attribtValue, "radio");
			Assert.assertEquals(answerRadiobutton.isSelected(), true);
			WebActionUtil.logger.info("Succesfully response type radio button does not allow user to enter data into it");
		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("Response type field failed to behave as a radio button because it accepted data sent to it");
		}
	}

	/*
	 * verify after selecting answer option from Response type field
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_112(String data) throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(converse_Addnew, "click on add new button");
		String tagName = answerTextArea.getTagName();
		try {
			Assert.assertEquals(tagName, "textarea");
			WebActionUtil.clearAndTypeText(answerTextArea, data, "enter data into answer text field");
			data.replace("\n", Keys.chord(Keys.SHIFT, Keys.ENTER));
			
			WebActionUtil.logger.info("succesful in entering multiple lines of data into answer text area");
		}
		catch(AssertionError e) {
			WebActionUtil.logger.info("Failed to enter multiple lines of code in answer text area");
		}
	}
	
	/*
	 * Method for Test case ID Eva_CCA_Onboard PatientPage _145 to verify URL field on Conversation Page
	 */

	public void Eva_CCA_OnboardPatient_145(String url) throws Throwable {

		InitializePages.OnboardPatient.navigateToAddConversation();
		WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
		WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
		WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(mediaTypeImagedropDown, "mediaResponsetype");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(imagedropdown, "mediaResponsetype");

		// WebActionUtil.clickOnElementUsingJS(urlRadioButton, "urlRadioButton");
		WebActionUtil.sleep(1);
		WebActionUtil.isElementDisplayed(urlField, "urlField", 1);
		WebActionUtil.typeText(urlField, url, "UrlField");

	}

	/*
	 *  Method for Test case ID Eva_CCA_Onboard PatientPage _146 to verify URL field options on Conversation Page
	 */

	public void Eva_CCA_OnboardPatient_146() throws Throwable {

		InitializePages.OnboardPatient.navigateToAddConversation();
		WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
		WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
		WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
		WebActionUtil.clickOnElementUsingJS(mediaTypeImagedropDown, "mediaResponsetype");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(imagedropdown, "mediaResponsetype");
		WebActionUtil.clickOnElement(imageFileUpload, "imageFileUpload");
		WebActionUtil.isElementDisplayed(uploadFiletext, "Upload_Field", 1);

	}

	/*
	 * Method for Test case ID Eva_CCA_Onboard PatientPage _148 to verify File Upload Functionality for Audio file Upload
	 */
	

	public void Eva_CCA_OnboardPatient_148() throws Throwable {

		InitializePages.OnboardPatient.navigateToAddConversation();
		WebActionUtil.clickOnElementUsingJS(addNewButton, "addNewButton");
		WebActionUtil.clickOnElementUsingJS(mediaResponsetype, "mediaResponsetype");
		WebActionUtil.scrollToElement(mediaTypedropDown, "Media");
		WebActionUtil.clickOnElementUsingJS(mediaTypeImagedropDown, "mediaResponsetype");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElementUsingJS(imagedropdown, "mediaResponsetype");
		WebActionUtil.clickOnElement(imageFileUpload, "imageFileUpload");
		WebActionUtil.sleep(1);
		WebActionUtil.clickOnElement(browseBtn, "BrowseButton");
		WebActionUtil.uploadFileWithRobot("D:\\09062019\\evaCare\\Image\\maruti-swift-Image.jpg");
		WebActionUtil.isElementDisplayed(removeOption, "RemoveOption", 1);

	}

}
