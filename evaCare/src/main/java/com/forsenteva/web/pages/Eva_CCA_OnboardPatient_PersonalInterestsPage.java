package com.forsenteva.web.pages;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_PersonalInterestsPage extends BasePage {

	public Eva_CCA_OnboardPatient_PersonalInterestsPage(WebDriver driver) {
		super(driver);
		
	}
	
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	//clicking on personal interest in patient grid under onboard patient
    @FindBy(xpath = "//div[@id='form-group-PatientsList.PersonalInterests-0-6']/button[@id='PatientsList.PersonalInterests-0-6']")
    private WebElement pinterest;
	
	@FindBy(xpath = "//input[@id='AddActivities']")
	private WebElement activitytextfield;
	
	@FindBy(xpath = "//p[contains(text(),'Activities name should be less than 45.')]")
	private WebElement activitytext;
	
	/*
	 * Eva_CCA_Onboard  Patient_072 WebElements
	 */
		
		@FindBy(xpath="//input[@id='AddHobbies']")
		private WebElement addhobies;
		
		@FindBy(xpath="//button[@id='AddHobbiesEvent']")
		private WebElement hobbiesaddclick;
		
		@FindBy(xpath="//div[@id='form-group-ActivitesList2-0-1']")
		private WebElement hobbiesdata;
		
		@FindBy(xpath="//div[@class='formio-data-grid ng-scope']/descendant::table[@class='table datagrid-table']/descendant::tr[@class='ng-scope']")
		List<WebElement> activitydata;
		
		
		@FindBy(xpath="//button[@id='AddActivitiesEvent']")
		private WebElement activityclick;
		
		@FindBy(xpath="//input[@id='AddInterests']")
		private WebElement addinterest;
		@FindBy(xpath="//button[@id='AddInterestsEvent']")
		private WebElement addinteresrclick;
		@FindBy(xpath="//div[@id='form-group-ActivitesList3-0-1']")
		private WebElement addinterestdata;
		
		/*
		 * 62 testcase webelements
		 */
		@FindBy(xpath="//a[contains(text(),'Personal Interests')]")
		private WebElement personalnavogate;
		

		/*
		 * 63 testcases WebElements
		 */
		@FindBy(xpath="//span[contains(text(),'Activities')]")
		private WebElement activity;
		@FindBy(xpath="//span[contains(text(),'Hobbies')]")
		private WebElement hobies;
		@FindBy(xpath="//span[contains(text(),'Interests')]")
		private WebElement interest;
		@FindBy(xpath="//input[@name='Filter']")
		private WebElement serchfield;
		@FindBy(xpath="//button[@id='Search']")
		private WebElement searchbtn;
		@FindBy(xpath="//label[@class='control-label ng-binding']/input[@id='PresentPast-Present']")
		private WebElement present;
		@FindBy(xpath="//label[@class='control-label ng-binding']/input[@id='PresentPast-Past']")
		private WebElement past;
	
	/*
	 * 71 testcase
	 * Testfield can accept max 45 characters
	 */
	public void OnboardPatient_071() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "Addnewbutton");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(pinterest, "clicking on personal interest");
		WebActionUtil.sleep(2);
		WebActionUtil.typeText(activitytextfield, "caregivercaregivercaregivercaregivercaregivercaregiver", "testdatamax45characters");
		WebActionUtil.sleep(2);
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		//String data=WebActionUtil.getText(activitytext, "getting text");
		//System.out.println(data);
		try {
				WebActionUtil.verifyElementText(activitytext, "Activities name should be less than 45.");
				WebActionUtil.logger.info("Personalise text filed only accept max45 characters");
			}catch(AssertionError e)
			{
				WebActionUtil.logger.info("Personalise text filed any lenght of characters");
			}
	}
	/*
	 * Eva_Care_OnboardPatient_072Test case
	 * 
	 */
	public void OnboardPatient_072() throws Throwable
	{	
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "click onboardpt");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(pinterest, "personalinteresr");
		WebActionUtil.sleep(2);
		for(int i=0;i<=15;i++)
		{
			WebActionUtil.typeText(activitytextfield,RandomStringUtils.randomAlphabetic(5) , "adding text");
			WebActionUtil.clickOnElement(activityclick, "clicking on element");
			WebActionUtil.sleep(2);
			
		}
		WebActionUtil.logger.info("user can able to add 10 activities");
		
		for(int i=0;i<=15;i++)
		{
			WebActionUtil.typeText(addhobies, RandomStringUtils.randomAlphabetic(5), "adding hobies");
			WebActionUtil.clickOnElement(hobbiesaddclick, "adding hobies");
			WebActionUtil.sleep(2);
			
		}
		WebActionUtil.logger.info("user can able to add 10 hobies");	
		for(int i=0;i<=15;i++)
		{
			WebActionUtil.typeText(addinterest, RandomStringUtils.randomAlphabetic(5), "adding  interest");
			WebActionUtil.clickOnElement(addinteresrclick, "adding click");
			WebActionUtil.sleep(2);
		}
		WebActionUtil.logger.info("user can able to add 10 interest");
		if(activitydata.size()==40);
		{
			WebActionUtil.logger.info("user can able to add 10 activities in all fields");
		}
	}
	
	/*
	 * 62 testcase
	 *verifying aplication is navigated to Personal interest or not
	 */	
	public void OnboardPatient_062() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "click onboardpt");
		WebActionUtil.sleep(2);
		WebActionUtil.clickOnElementUsingJS(pinterest, "personalinteresr");
		WebActionUtil.sleep(2);
		boolean piconversion=WebActionUtil.isElementDisplayedOrNot(personalnavogate);
		
		try {
			Assert.assertEquals(piconversion, true);
			WebActionUtil.logger.info("application is navigated to the personal interest");
		}catch(AssertionError e)
		{
			WebActionUtil.logger.info("application is not navigated to the personal interest");
		}
	}	
	/*
	 * 63 testcase
	 *1. verifying all the fields are displayed in the personal interest page
	 *2. checking search the perticular patient and clicking the personal interest page is coming or not
	 */	
	public void OnboardPatient_063() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "click onboardpt");
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(pinterest, "personalinteresr");
		WebActionUtil.sleep(3);
		
		boolean activity1=WebActionUtil.isElementDisplayedOrNot(activity);
		System.out.println(activity1);
		boolean hobbie1=WebActionUtil.isElementDisplayedOrNot(hobies);
		System.out.println(hobbie1);
		boolean interest1=WebActionUtil.isElementDisplayedOrNot(interest); 
		System.out.println(interest1);
		boolean present1=WebActionUtil.isElementDisplayedOrNot(present);
		System.out.println(present1);
		boolean past1=WebActionUtil.isElementDisplayedOrNot(past);
		System.out.println(past1);
		if(activity1==true &&hobbie1==true&&interest1==true&&present1==true&&past1==true) {
			WebActionUtil.logger.info("in Personal interest page present ,past,activity,hobbies,interest fields are displayed");
		}
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElementUsingJS(onboardPatient, "click onboardpt");
		WebActionUtil.sleep(3);
		WebActionUtil.typeText(serchfield, "a233", "search name field");
		WebActionUtil.sleep(5);
		WebActionUtil.clickOnElement(searchbtn, "clicking on search btn");
		WebActionUtil.sleep(6);
		WebActionUtil.clickOnElementUsingJS(pinterest, "personalinteresr");
	WebActionUtil.sleep(3);
	boolean piconversion=WebActionUtil.isElementDisplayedOrNot(personalnavogate);
			try {
			Assert.assertEquals(piconversion, true);
			WebActionUtil.logger.info("application is navigated to the personal interest for perticular patient");
	}catch(AssertionError e)
		{
			WebActionUtil.logger.info("application is not navigated to the personal interest for perticular patient");
		}
	}
}

