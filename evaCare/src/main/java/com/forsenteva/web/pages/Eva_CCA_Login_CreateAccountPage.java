package com.forsenteva.web.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.forsenteva.web.generic.ExcelDataProvider;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.GenericLib;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_Login_CreateAccountPage extends BasePage{

	public Eva_CCA_Login_CreateAccountPage(WebDriver driver) {
		super(driver);
	
	}
	/*
	 * 20 testcase web elements
	 */
	@FindBy(xpath = "//span[contains(text(),'Register Care Centre')]")
	private  WebElement register;
	@FindBy(xpath = "//div[@class='control-group error']/input[@class='field lfr-input-text error-field']")
	private  WebElement emailaddress;
	@FindBy(xpath = "//div[@class='email']")
	private  WebElement eMailtext;
	/*
	 *8 th testcase web elements 
	 */
	
	@FindBy(xpath = "//div[@class='required']")
	private  WebElement carecenter_validationmessage;
	@FindBy(xpath = "//input[@id='_58_firstName']")
	private  WebElement carecentername;
	
	/*
	 * 12 th testcase webelements
	 */
	@FindBy(xpath = "//select[@id='_58_accountFor']")
	private  WebElement carecenter_select;
	/*
	 * 14 th test case webelements
	 */
		@FindBy(xpath = "//div[@class='control-group']/label[@class='control-label']/span[@class='label-required']")
		private  WebElement adminname;
		
	/*
	 * 8 testcase
	 * verifying caregiver name filed having validation message as "This field is required."
	 */
	public void Login_008() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(register, "clicking on regester patient");
		WebActionUtil.sleep(1);
		carecentername.sendKeys(Keys.SPACE);
		
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);
		String carecenter_validationmessage1=carecenter_validationmessage.getText();
		
		Assert.assertEquals(carecenter_validationmessage1, "This field is required.");
		WebActionUtil.logger.info("caregiver name filed having validation message as This field is required.");
	}
	
	/*
	 * 12 testcase
	 * Verifying Account for field is allowing multiple select data
	 */
public void Login_012() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(register, "clicking on regester patient");
	
	Select select=new Select(carecenter_select);
	WebElement data=select.getFirstSelectedOption();
	String defaultItem = data.getText();
	
	select.selectByIndex(1);
	
	WebElement data1=select.getFirstSelectedOption();
	String defaultItem1 = data1.getText();
	try {
			Assert.assertEquals(defaultItem, defaultItem1);
			WebActionUtil.logger.info("User can select multiple select data");
		}catch(AssertionError e)
		{
			WebActionUtil.logger.info("User can't select multiple select data");
		}
}
/*
 * Login_014 testcase
 * verifiing Admin name field contains * or not
 */
public void Login_014() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(register, "clicking on regester patient");
	boolean adminname1=WebActionUtil.isElementDisplayedOrNot(adminname);
	if(adminname1==true)
	{
		WebActionUtil.capture(driver);
		WebActionUtil.logger.info("Admin name field having * symbol");
		
	}else {
		WebActionUtil.logger.info("Admin name field not having * symbol");
	}
}

	
	/*
	 * 20 test case 
	 * verifying email address is accepting a to z,A to Z ,0 to 9 is accepting or not
	 * verifying email address is accepting stating . and ending .
	 */
	public void Login_020() throws Throwable
	{
		WebActionUtil.clickOnElementUsingJS(register, "clicking on regester patient");
		WebActionUtil.sleep(1);
		String[] sdata1 = ExcelDataProvider.getExcelData(GenericLib.emaildatapath, "Sheet1", "TEST_CASE_NO");
		int i=1; 
	while(i<5)
	{
		String sdata2 = ExcelDataProvider.readcolData(sdata1[i], "Email", GenericLib.emaildatapath, "Sheet1");
		
	
		WebActionUtil.clearAndTypeText(emailaddress, sdata2, "entering email");
		driver.switchTo().activeElement().sendKeys(Keys.TAB);
		WebActionUtil.sleep(2);
		try {
				boolean data=WebActionUtil.isElementDisplayedOrNot(eMailtext);
		
			if(data==true)
			{  
				
				i++;	
			}
			else {
				WebActionUtil.logger.info("Email successfully verified all conditions");
			}
			
		}catch(AssertionError e)
		{
			WebActionUtil.logger.info("Email not verified all conditions");
		}
	}
	}
}
