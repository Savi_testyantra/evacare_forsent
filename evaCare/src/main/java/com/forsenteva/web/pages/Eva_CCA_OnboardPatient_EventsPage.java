package com.forsenteva.web.pages;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.util.WebActionUtil;

public class Eva_CCA_OnboardPatient_EventsPage  extends BasePage{

	public Eva_CCA_OnboardPatient_EventsPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(xpath = "//div[@id='mCSB_1_container']//span[@class='nav-title'][text()=' Events ']")
	private WebElement title;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[contains(text(),'Previous')]")
	private WebElement previousButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[contains(text(),'Next')]")
	private WebElement nextButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[text()='Year']")
	private WebElement yearButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[text()='Month']")
	private WebElement monthButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents']//button[text()='Week']")
	private WebElement weekButton;

	@FindBy(xpath = "//button[text()='Day']")
	private WebElement dayButton;

	@FindBy(xpath = "//div[@id='form-group-AddEvents.columns3']//span[text()='Add Event']")
	private WebElement addEventButton;

	@FindBy(xpath = "//div[@class='addEventDialogcontrols']/input[@type='text']")
	private WebElement eventDialogTitleTextBox;

	@FindBy(xpath = "//div[@class='addEventDialogcontrols']//textarea[@placeholder='Enter the event description']")
	private WebElement eventDialogDescriptionTextBox;

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Once']")
	private WebElement eventOnceButton;

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Daily']")
	private WebElement eventDailyButton;

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Weekly']")
	private WebElement eventWeekButton;	

	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Monthly']")
	private WebElement eventMonthlyButton;

	@FindBy(xpath = "//div[contains(@class,'removeHasError')]//input[@is-open='event.onceStartOpen']")
	private WebElement dateTextbox;

	@FindBy(xpath = "//div[contains(@class,'removeHasError')]//input[@is-open='event.dailyStartOpen']")
	private WebElement startDateTextbox;

	@FindBy(xpath = "//div[contains(@class,'removeHasError')]//input[@is-open='event.dailyEndOpen']")
	private WebElement endDateTextbox;
	
	/*
	 * 7 th testcase web Elements
	 */
	@FindBy(xpath = "//span[contains(text(),' Events ')]")
	private  WebElement eventspage;
	@FindBy(xpath = "//div[@class='col-md-3']/h5[@class='text-center ng-binding']")
	private  WebElement currentmonth;
	@FindBy(xpath = "//div[@class='col-md-4 text-center btn-group']/button[contains(text(),'Previous')]")
	private  WebElement previousbtn;
	@FindBy(xpath = "//div[@class='col-md-4 text-center btn-group']/button[contains(text(),'Next')]")
	private  WebElement nextbtn;
	@FindBy(xpath = "//div[@class='btn-group pull-right']/button[contains(text(),'Year')]")
	private  WebElement yearbtn;
	@FindBy(xpath = "//div[@class='btn-group pull-right']/button[contains(text(),'Month')]")
	private  WebElement monthbtn;
	@FindBy(xpath = "//div[@class='btn-group pull-right']/button[contains(text(),'Week')]")
	private  WebElement weekbtn;
	@FindBy(xpath = "//div[@class='btn-group pull-right']/button[contains(text(),'Day')]")
	private  WebElement daybtn;
	//@com.paulhammant.ngwebdriver.ByAngularModel.FindBy(model="")
	/*
	 * 14 testcase webelements
	 */
	@FindBy(xpath = "//div[@active='event.activeTab']//a[text()='Daily']")
	private  WebElement dailybtn;
	@FindBy(xpath = "//i[@class='glyphicon glyphicon-calendar'][1]")
	private  WebElement Startdatedate;
	@FindBy(xpath = "//i[@class='glyphicon glyphicon-calendar'][2]")
	private  WebElement enddatedate;
	@FindBy(xpath = "//div[@class='componentButtons col-xs-12']/button[contains(text(),'Save')]")
	private  WebElement saveevent;
	
	@FindBy(xpath = "//small[@class='cal-events-num badge badge-important pull-left ng-binding']")
	private  WebElement gettingevents_list;
	
	@FindBy(xpath = "//span[contains(text(),' Onboard Patient ')]")
	private WebElement onboardPatient;
	@FindBy(xpath="//button[@id='PatientProfile.Submit']")
	private WebElement submit;
	
	/*
	 * Eva_CCA_Onboard  Patient_230 testcase web elements
	 */
	@FindBy(xpath="//button[@name='PatientsList.Events-0-10']")
	private WebElement eventbuttonclick;
	@FindBy(xpath="//small[@class='cal-events-num badge badge-important pull-left ng-binding']")
	private WebElement eventshowlick;
	@FindBy(xpath="//div[@class='cal-slide-box ng-scope in collapse']/descendant::ul/descendant::a/span[contains(text(),'New Event')]/../following-sibling::a/i[@class='glyphicon glyphicon-edit']")
	private WebElement eventedit;
	
	@FindBy(xpath="//input[@class='form-control ng-pristine ng-valid ng-not-empty ng-touched']")
	private WebElement eventitle;
	@FindBy(xpath="//button[@class='componentDialogButton pull-right btn btn-success theme-color ng-binding']")
	private WebElement savebtn;
	
	@FindBy(xpath="//span[@class='ng-binding']")
	private WebElement spantext;
	
	
	/*
	 * 202 testcase webelements
	 */
	
	@FindBy(xpath = "//button[contains(text(),'Previous')]")
	private WebElement previous;
	@FindBy(xpath = "//button[contains(text(),'Next')]")
	private WebElement next;
	@FindBy(xpath = "//button[contains(text(),'Year')]")
	private WebElement year;
	@FindBy(xpath = "//button[contains(text(),'Month')]")
	private WebElement month;
	@FindBy(xpath = "//button[contains(text(),'Week')]")
	private WebElement week;
	@FindBy(xpath = "//button[contains(text(),'Day')]")
	private WebElement day;
	@FindBy(xpath = "//button[@class='btn btn-sm btn-primary pull-right theme-color']/span[contains(text(),'Schedule Media')]")
	private WebElement shedulemedia;
	@FindBy(xpath = "//button[@class='btn btn-sm btn-primary pull-right theme-color']/span[contains(text(),'Add Event')]")
	private WebElement addevent;
	@FindBy(xpath = "//button[@class='btn btn-primary ng-binding']")
	private WebElement export;
	@FindBy(xpath = "//div[@class='col-md-12 text-center']/div[@class='col-md-3']/h5[@class='text-center ng-binding']")
	private WebElement monthdata;
	
	// Path for add events button
		@FindBy(xpath = "//div[@class='col-md-12 pull-right text-center']/descendant::span[text()='Add Event']")
		private WebElement addEventsBtn;
		// Path for scheduled media button
		@FindBy(xpath = "//div[@class='col-md-12 pull-right text-center']/descendant::span[text()='Schedule Media']")
		private WebElement scheduledMediaBtn;

		// Add events popup elements
		// Path for title text field
		@FindBy(xpath = "//input[@class='form-control ng-pristine ng-valid ng-not-empty ng-touched']")
		private WebElement eventTitle;
		// Path for description field
		@FindBy(xpath = "//textarea[@class='form-control ng-pristine ng-valid ng-empty ng-touched']")
		private WebElement description;
		// Path for calendar
		@FindBy(xpath = "//button[@class='btn btn-default']")
		private WebElement calender;
		// Path for month
		@FindBy(xpath = "//strong[@class='ng-binding']")
		private WebElement monthclick;
		// Path for start time hour field
		@FindBy(xpath = "//table[@class='uib-timepicker']//td[@class='uib-timepicker']")
		private WebElement startTimeHour;
		// Path for start time hour field
		@FindBy(xpath = "//input[@class='form-control text-center ng-pristine ng-valid ng-valid-minutes ng-not-empty ng-valid-maxlength ng-touched']")
		private WebElement startTimeMins;
		// Path for start time hour field
		@FindBy(xpath = "(//input[@class='form-control text-center ng-pristine ng-valid ng-valid-hours ng-not-empty ng-valid-maxlength ng-touched'])[2]")
		private WebElement endTimeHour;
		// Path for start time hour field
		@FindBy(xpath = "//input[@class='form-control text-center ng-pristine ng-valid ng-valid-hours ng-not-empty ng-valid-maxlength ng-touched']")
		private WebElement endTimeMin;

		// Scheduled media pop up elements
		// Path for title text field
		@FindBy(xpath = "//div[@class='addEventDialogText']/descendant::label[text()='Title:']/../../descendant::input")
		private WebElement schTitleText;
		// Path for description text field
		@FindBy(xpath = "//div[@class='addEventDialogText']/descendant::label[text()='Description:']/../../descendant::textarea")
		private WebElement schDescriptionText;
		// Path for media selection field
		@FindBy(xpath = "//div[@class='addEventDialogText']/descendant::label[text()='Media:']/../../../descendant::input")
		private WebElement schMediaSelect;
		// Path for Once tab in events
		@FindBy(xpath = "//ul[@class='nav nav-tabs']/descendant::a[text()='Once']")
		private WebElement schOnceEventsTab;
		// Path for Once tab in events
		@FindBy(xpath = "//ul[@class='nav nav-tabs']/descendant::a[text()='Daily']")
		private WebElement schDailyEventsTab;
		// Path for Once tab in events
		@FindBy(xpath = "//ul[@class='nav nav-tabs']/descendant::a[text()='Weekly']")
		private WebElement schWeeklyEventsTab;
		// Path for Once tab in events
		@FindBy(xpath = "//ul[@class='nav nav-tabs']/descendant::a[text()='Monthly']")
		private WebElement schMonthlyEventsTab;
		// Path for date field when events "Once" is selected
		@FindBy(xpath = "//label[text()='Date:']")
		private WebElement schDateField;
		// Path for date field when events "Once" is selected
		@FindBy(xpath = "(//label[text()='Start Time:'])[1]")
		private WebElement schStartTimeLable;
		// Path for save button
		@FindBy(xpath = "//div[@class='componentButtons col-xs-12']/descendant::button[text()='Save']")
		private WebElement schSaveButton;
		// Path for date field when events "Once" is selected
		@FindBy(xpath = "//div[@class='componentButtons col-xs-12']/descendant::button[text()='Cancel']")
		private WebElement schCancelButton;

		public void Eva_CCA_Onboard_Patient_224() throws Throwable {
			InitializePages.OnboardPatient.navigateToAddEvents();
			WebActionUtil.clickOnElement(scheduledMediaBtn, "ScheduledMediaBtn");
			WebActionUtil.clickOnElement(schOnceEventsTab, "Once Events Tab");
			WebActionUtil.isElementDisplayed(schDateField, "Only one date field", 1);

		}

		public void Eva_CCA_OnboardPatient_223() throws Throwable {
			InitializePages.OnboardPatient.navigateToAddEvents();
			WebActionUtil.clickOnElement(scheduledMediaBtn, "ScheduledMediaBtn");
			WebActionUtil.sleep(3);
			WebActionUtil.isElementDisplayed(schTitleText, "Title text field", 1);
			WebActionUtil.isElementDisplayed(schDescriptionText, "Description text field", 1);
			WebActionUtil.isElementDisplayed(schMediaSelect, "Media select field", 1);
			WebActionUtil.isElementDisplayed(schOnceEventsTab, "Once tab", 1);
			WebActionUtil.isElementDisplayed(schDailyEventsTab, "Daily tab", 1);
			WebActionUtil.isElementDisplayed(schWeeklyEventsTab, "Weekly tab", 1);
			WebActionUtil.isElementDisplayed(schMonthlyEventsTab, "Monthly tab", 1);
			WebActionUtil.isElementDisplayed(schDateField, "Date field", 1);
			WebActionUtil.isElementDisplayed(schStartTimeLable, "Start time field", 1);
			WebActionUtil.isElementDisplayed(schSaveButton, "Save button", 1);
			WebActionUtil.isElementDisplayed(schCancelButton, "Cancel button", 1);

		}
	
	/*
	 * Eva_Care_OnboardPatient_230Test case
	 * user can edit or update schedule event---
	 * 
	 */
public void OnboardPatient_230() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElementUsingJS(eventbuttonclick, "clicking on event");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElement(eventshowlick, "event clicking");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElement(eventedit, "edit the event");
	WebActionUtil.sleep(2);
	WebActionUtil.clearAndTypeText(eventitle, "New Event 5", "editing the event");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElement(submit, "submit the button");
	WebActionUtil.sleep(2);
	String eventdata=WebActionUtil.getText(spantext, "getting text");
	System.out.println(eventdata);
}
public void OnboardPatient_202() throws Throwable
{
	WebActionUtil.clickOnElementUsingJS(onboardPatient, "clicking on onbord patient");
	WebActionUtil.sleep(2);
	WebActionUtil.clickOnElementUsingJS(eventbuttonclick, "clicking on event");
	WebActionUtil.sleep(2);
	
	
	String[] monthName = {"January", "February",
            "March", "April", "May", "June", "July",
            "August", "September", "October", "November",
            "December"};

    Calendar cal = Calendar.getInstance();
    String month2 = monthName[cal.get(Calendar.MONTH)];
    String monthdata1= monthdata.getText();
	
    System.out.println(monthdata1);
	
	
	
	boolean previous1=WebActionUtil.isElementDisplayedOrNot(previous);
	
	boolean next1=WebActionUtil.isElementDisplayedOrNot(next);
	
	boolean year1=WebActionUtil.isElementDisplayedOrNot(year);
	
	boolean month1=WebActionUtil.isElementDisplayedOrNot(month);
	
	boolean week1=WebActionUtil.isElementDisplayedOrNot(week);
	
	boolean day1=WebActionUtil.isElementDisplayedOrNot(day);

	boolean shedulemedia1=WebActionUtil.isElementDisplayedOrNot(shedulemedia);
	
	boolean addevent1=WebActionUtil.isElementDisplayedOrNot(addevent);
	
	boolean export1=WebActionUtil.isElementDisplayedOrNot(export);
	
	
	
	if(previous1==true &&next1==true&&year1==true&&month1==true&&week1==true&&
			day1==true&&shedulemedia1==true&&addevent1==true&&export1==true && monthdata1.contains(month2))
	{
		WebActionUtil.logger.info("previous,next,year,month,week,day.shedulemedia,addevent,export buttons are displayed");
	}
	else {
		WebActionUtil.logger.info("previous,next,year,month,week,day.shedulemedia,addevent,export buttons are not displayed");
	}
	

	
}
	

	
	public void Eva_CCA_Events_225() throws Throwable {
		Eva_CCA_OnboardPatientPage.navigationToEvents();
		
	}
}